<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH . "third_party/MX/Router.php";

class MY_Router extends MX_Router {

  public function __construct ()
  {
    parent::__construct();
  }
/*
  function show_404 ()
  {
    echo '404';
    //include(APPPATH . 'controllers/' . $this->fetch_directory() . $this->error_controller . EXT);
    //call_user_func(array($this->error_controller, $this->error_method_404));
  }
*/
}