<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @name 				Site_Controller
 * @author 			Júnior Messias <junior@blinks.com.br>
 * @package 		Site
 * @subpackage 	Controllers
 */

class Site_Controller extends CI_Controller
{
	var $site;
	var $empresa;
	var $_pagina;
	var $_acao;
  var $_titulo = array();

	/**
	 * Método Construtor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->boot();
    $this->estrutura();
 	}

	/**
	 * Gera as configurações iniciais do sistema
	 * data/hora local | config do site | empresa
	 * página | ação
   *
	 * @access public
	 * @return void
	 */
	public function boot()
	{
    //print_r($this->router->fetch_class());
    //print_r($this->router->fetch_method());

		date_default_timezone_set($this->config->item('time_reference'));

    $this->load->helper('url');

    // Configurações do site e empresa
		$this->load->model('config/config_m');
		$this->load->model('empresa/empresa_m');
    $this->site = $this->config_m->get_id(1);
		$this->empresa = $this->empresa_m->get_id(1);

    if((!$this->site->online) && ($this->uri->segment($this->uri->total_segments()) != 'manutencao'))
      redirect(site_url('site/manutencao'));

		// Mídia
		$this->load->model('midia/midia_m');
    $this->load->model('menu/menu_m');

    // Municipio / Estado
    $this->load->model('municipio/municipio_m');
    $this->load->model('estado/estado_m');
    $m = $this->municipio_m->get_id($this->empresa->id_municipio);
    $e = $this->estado_m->get_id($m->id_estado);
    $this->empresa->municipio = $m->desc_municipio;
    $this->empresa->estado = $e->desc_estado;
    $this->empresa->uf = $e->uf;

		// Profiler
		$this->output->enable_profiler(FALSE);

		// Variáveis
		$this->_pagina = $this->uri->segment(2);
		$this->_acao = $this->uri->segment(3);

		// Template de Admin
		$this->template->set_template('site');
	}

  public function estrutura()
  {
    $this->load->model('produto/produto_m');

    // CSS
    $this->template->add_css('css/colorbox.css'); // Colorbox
    $this->template->add_css('css/lava.css'); // Lava
    $this->template->add_css('css/orbit.css'); // Orbit
    if($this->agent->is_browser('MSIE')) // Orbit - IE
        $this->template->add_css('.timer { display: none !important; } div.caption { background:transparent; filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000,endColorstr=#99000000);zoom: 1; }', 'embed');
    $this->template->add_css($this->tema_url('css/site.css')); // Site
    // Aplicando Background
    if(file_exists($this->config->item('dir_midia').$this->site->img_fundo))
      $this->template->add_css('body { background: url('.site_url($this->config->item('dir_midia').$this->site->img_fundo).') repeat-x scroll right top; }', 'embed');

    // JS
    $this->template->add_js("\n".'var SITE_URL = "'.site_url().'";'."\n".'var CURRENT_URL = "'.current_url().'";'."\n", 'embed');
    $this->template->add_js('js/jquery.colorbox-min.js'); // Colorbox
    $this->template->add_js('js/jquery.lavalamp.min.js'); // Lavalamp
    $this->template->add_js('js/jquery.easing.min.js'); // Easing - Lavalamp
    $this->template->add_js('js/jquery.orbit.min.js'); // Orbit Slider
    $this->template->add_js('js/jquery.watermarkinput.js'); // Pesquisa
    $this->template->add_js('js/site/menu_lateral.js'); // Menu lateral

    $this->load->model('banner/banner_m');
    $this->load->model('categoria/categoria_m');

    // Menu lateral
    switch($this->site->categoria_menu)
    {
      case 0:
        $sql = 'SELECT id_produto AS id, desc_produto AS descricao, slug AS slug, tipo AS tipo FROM produto WHERE tipo = 2
                UNION
                SELECT id_categoria AS id, desc_categoria AS descricao, slug AS slug, tipo AS tipo FROM categoria WHERE tipo = 1
                ORDER BY descricao ASC';
        $itens = $this->db->query($sql);
      break;
      case 2: // Servico - mostra serviços
        $itens = $this->produto_m->get_all(NULL, NULL, array('tipo' => 2, 'status' => 1));
      break;
      default: // Prdouto - mostra categorias
        $itens = $this->categoria_m->get_all(NULL, NULL, array('tipo' => 1));
    }

    $this->template->write_view('menu_lateral', 'comum/menu_lateral', array('itens' => $itens));
  }

  public function tema_url($string = '')
  {
    (strlen($this->site->tema) == 0) ? $this->site->tema = 'padrao' : $this->site->tema;
		return $this->config->item('dir_tema').$this->site->tema.'/'.$string;
	}

  function envia_email($introducao, $mensagem, $assunto, $de_email, $de, $para)
  {
    $this->load->library('email');

    $conteudo ='
    <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
      <tbody>
        <tr>
          <td align="center">
            <table width="575" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
                <tr>
                  <td height="61" bgcolor="#f5f5f5" align="left" height="60px">
                    <a title="'.$this->empresa->desc_empresa.'" href="'.site_url().'" target="_blank">'.img(array('src' => $this->config->item('dir_midia').$this->empresa->logotipo, 'title' => $this->empresa->desc_empresa, 'alt' => $this->empresa->desc_empresa, 'border' => '0', 'style' => 'margin:3px')).'</a>
                  </td>
                </tr>
                <tr>
                  <td align="center">
                    <table width="550" cellpadding="0" border="0">
                      <tbody>
                        <tr>
                          <td>
                            <br><font size="4" face="Arial, Helvetica, sans-serif" color="#222">'.$introducao.'</font>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <font size="1"><br></font>
                            <font color="#333" size="2" face="Arial, Helvetica, sans-serif">Mensagem: '.br().$mensagem.'</font>
                          </td>
                        </tr>
                        <tr>
                          <td align="right"><font face="Arial, Helvetica, sans-serif" color="#6e6e6e" size="1">Não nos responsabilizamos pelo conteúdo deste texto</font></td>
                        </tr>
                        <tr>
                          <td><br><hr style="border-top:#999 solid 1px;border-right:0;border-left:0;border-bottom:0"></td>
                        </tr>
                        <tr>
                          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.data_hora_extenso(timestamp(), FALSE).'</font></td>
                        </tr>
                        <tr>
                          <td><hr style="border-top:#999 solid 1px;border-right:0;border-left:0;border-bottom:0"></td>
                        </tr>
                        <tr>
                          <td height="60"  bgcolor="#f5f5f5"><font size="4" face="Arial, Helvetica, sans-serif" color="#666666"><b>'.$this->empresa->desc_empresa.'</b></font>
                            '.br().$this->empresa->slogan.br().'
                            <a title="'.$this->empresa->desc_empresa.'" href="'.site_url().'" target="_blank"><font size="2" face="Arial, Helvetica, sans-serif" color="#537880">'.site_url().'</font></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    ';

		//$email['smtp_host'] = 'email.sicoobsmo.com.br';
		//$email['smtp_user'] = 'site@sicoobsmo.com.br';
		//$email['smtp_pass'] = 'site';
		//$email['smtp_port'] = 25;
		//$email['protocol'] = 'smtp';
		$email['mailtype'] = 'html';
		$email['charset'] = 'utf-8';
		$this->email->initialize($email);

    $this->email->from($de_email, $de);
		$this->email->to($para);
		//$this->email->bcc('jrmessias@gmail.com');

		$this->email->subject($assunto);
		$this->email->message($conteudo);

		return $this->email->send();

    //$this->email->print_debugger();
  }
}