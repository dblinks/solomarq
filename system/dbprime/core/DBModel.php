<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * DBModel - Model
 *
 * @package 		DataMapper
 * @subpackage 	Model
 * @category 		DB
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class DBModel extends CI_Model {

	protected $_tabela;
	protected $_id;
	protected $_desc;
	protected $_cri = 'data_cri';
	protected $_alt = 'data_alt';
	protected $_order;

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Métodos mágicos
	 *
	 * @ignore
	 * @param	string $metodo Nome do método
	 * @param	array  $dado 	 Dados para o método
	 * @return	mixed
	 */
	public function __call($metodo, $dado)
	{
		$metodos_magicos = array('get_by_');

		foreach ($metodos_magicos as $metodo_)
		{
			$partes = explode($metodo_, $metodo);
			return $this->{'_' . trim($metodo_, '_')}(str_replace($metodo_, '', $metodo), $dado);
		}

		throw new Exception('Não foi possível encontrar o método '.$metodo.' na classe '.get_class($this));
	}

	/**
	 * Get By
	 *
	 * Pegar objetos por campo e valor específicos
	 *
	 * @ignore
	 * @param	string $campo Campo de seleção
	 * @param	array  $valor Valor de localizar
	 */
	private function _get_by($campo, $valor = array())
	{
		$valor = is_array($valor) ? $valor[0] : $valor;
		$this->db->where($campo, $valor);
		return $this->db->get($this->_tabela);
	}

	/**
		* Insere novo registro na base de dados
		* Retornando o ID inserido
		*
		* @param array $dado Dados a serem inseridos
		* @return integer
		*/
	public function insert($dado, $tabela = NULL)
	{
		array_key_exists($this->_cri, $dado) ? $dado[$this->_cri] = now() : NULL;

		$tabela = (is_null($tabela)) ?  $this->_tabela : $tabela;
		return ($this->db->insert($tabela, $dado))
			? $this->db->insert_id()
			: FALSE;
	}

	/**
		* Altera registro na base de dados
		* Retornando o ID inserido
		*
		* @param array   $dado Dados a serem inseridos
		* @param integer $id 	 ID a ser alterado
		* @return bool
		*/
	public function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		array_key_exists($this->_alt, $dado) ? $dado[$this->_alt] = now() : NULL;

		$tabela = (is_null($tabela)) ?  $this->_tabela : $tabela;
    $id_tabela = (is_null($id_tabela)) ?  $this->_id : $id_tabela;

    (is_array($id)) ? $this->db->where($id) : $this->db->where($id_tabela, $id);

    return $this->db->set($dado)
				->update($tabela);
	}

	/**
		* Exclui registro na base de dados
		*
		* @param integer $id
		* @return bool
		*/
	public function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		$tabela = (is_null($tabela)) ?  $this->_tabela : $tabela;
    $id_tabela = (is_null($id_tabela)) ?  $this->_id : $id_tabela;

		return $this->db->delete($tabela, array($id_tabela => $id))
			? TRUE
			: FALSE;
	}

	/**
		* Verifica se outra tabela possui foreign key
		*
		* @param integer $id
		* @return bool
		*/
	public function check_foreign($tabela, $id, $campo_id = NULL)
	{
		$campo_id = is_null($campo_id) ? $this->_id : $campo_id;
		$query = $this->db->get_where($tabela, array($campo_id => $id));
		return (bool)$query->num_rows();
	}

	/**
	 * Get Field
	 *
	 * Pegar campo de tabela
	 *
	 * @ignore
	 * @param	string $campo Campo de retorno
 	 * @param	string $id		Identificador do campo
 	 * @param	string $campo_id		Campo de identificação
	 * @param	string|array  $where
	 */
	public function get_field($campo = NULL, $id = NULL, $campo_id = NULL, $where = NULL, $tabela = NULL)
	{
		// where
		(!is_null($where)) ? $this->db->where($where) : NULL;

		// campo desc
		$campo = (is_null($campo)) ? $this->_desc : $campo;
		$this->db->select($campo);

		// campo id
		$campo_id = (is_null($campo_id)) ? $this->_id : $campo_id;
		$this->db->where(array($campo_id => $id));

		$tabela = (is_null($tabela)) ?  $this->_tabela : $tabela;

		$query = $this->db->get($tabela);
		if($query->num_rows() > 0)
		{
			foreach ($query->result()as $row){}
			return $row->{$campo};
		}
		return FALSE;
	}

	function get_all($n = NULL, $offset = NULL, $where = NULL, $like = NULL, $order = NULL, $orlike = NULL, $notinc = NULL, $notin = NULL, $inc = NULL, $in = NULL, $tabela = NULL)
	{
		$tabela = (is_null($tabela)) ?  $this->_tabela : $tabela;

		// where
		if(!is_null($where))
		{
			foreach($where as $chave => $valor)
				$this->db->where(array($chave => $valor));
		}

		// like
		(!is_null($like)) ? $this->db->like($like) : NULL;

		// or_like
		(!is_null($orlike)) ? $this->db->or_like($orlike) : NULL;

		// not_in
		(!is_null($notinc) && !is_null($notin)) ? $this->db->where_not_in($notinc, $notin) : NULL;

		// in
		(!is_null($inc) && !is_null($in)) ? $this->db->where_not_in($inc, $in) : NULL;

		// order
		(is_null($order)) ? $this->db->order_by($this->_order) : $this->db->order_by($order);

		$query = (is_null($n) && is_null($offset)) ? $this->db->get($tabela) : $this->db->get($tabela, $n, $offset);

		return $query;
	}

	function get_id($id, $campo_id = NULL, $tabela = NULL)
	{
		$tabela = (is_null($tabela)) ?  $this->_tabela : $tabela;
		$campo_id = is_null($campo_id) ? $this->_id : $campo_id;
		$query = $this->db->get_where($tabela, array($campo_id => $id));
		if($query->num_rows() > 0)
			foreach ($query->result()as $row){}
		else
			$row = NULL;

		return $row;
	}

	function get_array($where = NULL, $not_in = NULL, $id = NULL, $desc = NULL, $tabela = NULL)
	{
		$chave = array();
		$valor = array();

		$tabela = (is_null($tabela)) ?  $this->_tabela : $tabela;

		// where
		(!is_null($where)) ? $this->db->where($where) : NULL;

		// where not in
		(!is_null($not_in)) ? $this->db->where_not_in($not_in) : NULL;

		$id_campo = (is_null($id)) ? $this->_id : $id;
		$desc_campo = (is_null($desc)) ? $this->_desc : $desc;

		$this->db->select($id_campo.' AS id, '.$desc_campo.' AS descricao', FALSE);

		$this->db->order_by($this->_order);

		$query = $this->db->get($tabela);

		// Retorno
		array_push($chave, '');
		array_push($valor, '');

		foreach($query->result_array() as $row)
		{
			array_push($chave , $row['id']);
			array_push($valor , $row['descricao']);
		}

		return array_combine($chave, $valor);
	}

	/*
	function get_in($tabela, $id, $campo)
	{
		$this->db->where_in($campo, $id);
		return $this->db->get($tabela);
	}
	*/
}

?>