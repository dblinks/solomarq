<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @name 				Admin controller
 * @author 			Júnior Messias <junior@blinks.com.br>
 * @package 		Admin
 * @subpackage 	Controllers
 */

class Admin_Controller extends CI_Controller
{
	public $usuario;
	var $modulo;
	var $site;
	var $empresa;
  var $tema = 'admin';
	var $_modulo;
	var $_modulo_principal;
	var $_acao;
	var $_permissao;

	/**
	 * Método Construtor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->boot();
 	}

	/**
	 * Gera as configurações iniciais do sistema
	 * variáveis / sessão / profiler / debug
	 * redirecinamento / usuário / módulo
	 *
	 * @access public
	 * @return void
	 */
	public function boot()
	{
    //print_r($this->router->fetch_class());
    //print_r($this->router->fetch_method());

		date_default_timezone_set($this->config->item('time_reference'));

		// Mídia
		$this->load->model('midia/midia_m');

		// Configurações do site e empresa
		$this->load->model('config/config_m');
		$this->load->model('empresa/empresa_m');
		$this->site = $this->config_m->get_id(1);
		$this->empresa = $this->empresa_m->get_id(1);

		// Profiler
		$this->output->enable_profiler(FALSE);

		// Redirecionamento se não estiver logado
		if ( (($this->uri->segment(2)) && (!$this->logado())) || (!$this->logado()) )
		{
			if ( !in_array($this->uri->segment(2), array('login', 'recuperar_senha', 'logout')) )
				redirect('admin/login');
		}

		// Variáveis
		$this->_modulo = $this->uri->segment(2);
		$this->_acao = $this->uri->segment(3);
    $acao_tpl = explode('_', $this->uri->segment(3)); // Ação

		$this->load->model('usuario/usuario_m');
		$this->load->model('modulo/modulo_m');

		// Dados do usuário da sessão
		if ($this->logado())
		{
			$this->usuario = $this->usuario_m->get_id($this->session->userdata('usuario'));
			$this->_permissao = explode(':', $this->usuario->permissao);
		}

		// Dados do módulo atual
		if (($this->_modulo) && !in_array($this->_modulo, $this->config->item('url_liberada')))
		{
			$m = $this->modulo_m->get_by_url($this->_modulo);

			// Erro módulo não encontrado
			if($m->num_rows() == 0)
			{
				$this->session->set_flashdata('msg', msg('Módulo não encontrado.', 'alerta'));
				redirect($this->agent->referrer());
			}

			// Módulo
			$this->modulo = $this->modulo_m->get_id($this->_modulo, 'url');

			// Erro módulo desativado
			if((!is_null($this->modulo->desc_modulo)) && ($this->modulo->ativo == 0))
			{
				$this->session->set_flashdata('msg', msg('Módulo '.$this->modulo->desc_modulo.' está desativado.', 'alerta'));
				redirect(admin_url());
			}

			// Sem acesso ao módulo
			if (
        (!is_null($this->modulo->desc_modulo)) &&
        ((!in_array($this->modulo->url, $this->_permissao)) ||
          ($this->modulo->nivel < $this->usuario->nivel)) &&
        !in_array($acao_tpl[0], array('ajax', 'xml', 'json'))
      )
			{
				if (($this->modulo->url != 'usuario') && ($this->_acao != 'perfil')){
					$this->session->set_flashdata('msg', msg('Você não possui acesso ao módulo '.$this->modulo->desc_modulo.'.', 'erro'));
					redirect(admin_url());
				}
			}
		}

		// Setando layout
		if(in_array($acao_tpl[0], array('ajax', 'xml', 'json')))
		{
			$this->output->enable_profiler(FALSE);
			$this->template->set_template($acao_tpl[0]);
		}
		else
		{
			// Template de Admin
			$this->template->set_template('admin');

			// Seção do menu
			$this->load->model('secao/secao_m');
			($this->logado()) ? $this->template->write_view('menu_lateral', 'admin/menu_lateral') : NULL;

			// Atalhos - Módulo
			if (!is_null(@$this->modulo->url))
			{
				file_exists(APPPATH.'modules/'.$this->modulo->url.'/views/admin/atalho.php')
					? $this->template->write_view('atalho', $this->modulo->url.'/admin/atalho')
					: $this->template->write('atalho', '');
			}

			// Ajuda - Módulo
			if (($this->_modulo) && (@!is_null($this->modulo->desc_modulo)))
			{
				file_exists(APPPATH.'modules/'.$this->modulo->url.'/views/ajuda.php')
					? $this->template->write('btn_ajuda', anchor(current_url().'#', img('img/icons/question-frame.png')))
					: NULL;
			}

			$this->template->write('titulo', ' / '.$this->site->desc_config);

			// Mensagens
			if(strlen($this->session->flashdata('msg')) > 0)
			{
				$this->template->write('msg', $this->session->flashdata('msg'));
			}
			if(strlen($this->session->flashdata('msgaux')) > 0)
			{
				$this->template->write('msg', $this->session->flashdata('msgaux'));
			}

			$this->template->add_js("\n".'var ADMIN_URL = "'.admin_url().'";'."\n".'var SITE_URL = "'.site_url().'";'."\n".'var CURRENT_URL = "'.current_url().'";'."\n".'var URLL = "/'.str_replace(site_url(), '', current_url()).'";'."\n", 'embed');
		}
	}

	/**
	 * Verifica se usuário está logado
	 *
	 * @access public
	 * @return bool
	 */
	function logado()
	{
		$logado = $this->session->userdata('logado');
		return ($logado==1) ? TRUE : FALSE;
	}

	public function ajuda($slug)
	{
	}
}