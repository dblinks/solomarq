<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array( 'class' => 'form validar'), array('id' => @$id_municipio));

echo li();
echo form_label('Município: ', 'municipio');
echo form_input(array('name' => 'municipio', 'id' => 'municipio', 'value' => set_value('municipio', @$desc_municipio), 'title' => 'Digite o nome do município.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite o município\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Estado: ', 'estado');
echo form_dropdown('estado', $this->estado_m->get_array(), @$id_estado, 'id="estado" title="Selecione o estado do município." class="dica corner {validar:{required:true, messages:{required:\'Selecione um estado\'}}}"').br();
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>