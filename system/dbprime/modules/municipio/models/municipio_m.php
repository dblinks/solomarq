<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Municipio_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Municipio
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link	
 * @version 		1.0 #20110921	
 */
 
class Municipio_m extends DBModel {

		var $_tabela = 'municipio';
		var $_id = 'id_municipio';
		var $_desc = 'desc_municipio';		
		var $_cri = 'data_cri';
		var $_alt = 'data_alt';
		var $_order = 'desc_municipio ASC';

    var $validation = array(
			array(
				'field' => 'desc_municipio',
				'label' => 'Município',
				'rules' => array('required')
			),			
			array(
				'field' => 'slug',
				'label' => 'SLUG',
				'rules' => array('trim')
			)
		);

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */	
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			'id_estado' => $dado['estado'],
			$this->_desc	=> $dado['municipio'],
			'slug'	=> slug($dado['municipio'])
		));
	}
		
	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */	
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::update(array(
			'id_estado' => $dado['estado'],
			$this->_desc => $dado['municipio'],
			'slug'	=> slug($dado['municipio'])
		), $dado['id']);
	}	
		
	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */			
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}
}

/* End of file Municipio_m,.php */
/* Location: ./app/modules/municipio/models/municipio.php */
