<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Município - Controller
 *
 * @package 		Admin
 * @subpackage 	Municipio
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{
	private $validation_rules = array(
		array(
			'field'   => 'municipio',
			'label'   => 'Município',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'estado',
			'label'   => 'Estado',
			'rules'   => 'trim|integer'
		)
	);

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('municipio/municipio_m');
		$this->load->model('estado/estado_m');
	}

	/**
	 * Página principal - index
	 *
	 * @access	public
	 */
	public function index()
	{
		$this->template->write('conteudo', heading('Listando Municípios', 1));
		$this->template->write_view('conteudo', 'admin/listar');

		$this->template->render();
	}

	/**
	 * Adiciona registro com validação do formulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function adicionar()
	{
		$this->template->write('conteudo', heading('Adicionar '.$this->modulo->alias, 1));
		$this->template->write_view('conteudo', 'admin/form');

		// Dados recebidos
		if ($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				$this->municipio_m->insert($this->input->post())
					? $this->session->set_flashdata('msg', msg('Sucesso ao adicionar '.$this->modulo->alias, 'sucesso'))
					: $this->session->set_flashdata('msg', msg('Erro ao adicionar '.$this->modulo->alias, 'erro'));
					redirect($this->agent->referrer());
			}
			else
			{
				$this->session->set_flashdata('msg', msg(validation_errors(), 'erro'));
				redirect($this->agent->referrer());
			}

			redirect(admin_url($this->modulo->url));
		}

		$this->template->render();
	}

	/**
	 * Altera registro com validação de fomulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
 	public function alterar()
	{
		$a = $this->municipio_m->get_id($this->uri->segment(4));
		$this->template->write('conteudo', heading('Alterar '.$this->modulo->alias.' : '.$a->desc_municipio));
		$this->template->write_view('conteudo', 'admin/form', $a);

		// Dados recebidos
		if($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				$this->municipio_m->update($this->input->post())
					? $this->session->set_flashdata('msg', msg('Sucesso ao alterar '.$this->modulo->alias, 'sucesso'))
					: $this->session->set_flashdata('msg', msg('Erro ao alterar '.$this->modulo->alias, 'erro'));
					redirect($this->agent->referrer());
			}
			else
			{
				$this->session->set_flashdata('msg', msg(validation_errors(), 'erro'));
				redirect($this->agent->referrer());
			}

			redirect(admin_url($this->modulo->url));
		}

		$this->template->render();
	}

	/**
	 * Exclui registro
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function excluir()
	{
    if($this->estado_m->check_foreign('municipio', $this->uri->segment(4)))
		{
			$this->session->set_flashdata('msg', msg('Município não pode ser excluído!'.br().'Exclua os Representantes deste Município antes.'.br().'Sua empresa pode estar neste Município.', 'erro'));
		}
    else
    {
      $this->municipio_m->delete($this->uri->segment(4))
        ? $this->session->set_flashdata('msg', msg('Sucesso ao excluir '.$this->modulo->alias, 'sucesso'))
        : $this->session->set_flashdata('msg', msg('Erro ao excluir '.$this->modulo->alias, 'erro'));
    }
		redirect(admin_url($this->modulo->url));
	}

	/**
	 * Codifica registros em Json
	 *
	 * @access	public
	 * @return	string
	 */
	public function json_listar()
	{
		######################## ALTERAR
		//Colunas de retorno
		$colunas = array($this->municipio_m->_id, $this->municipio_m->_desc, 'id_estado', 'opcoes');
		$estado = $this->estado_m->get_array();
		//Campo de ID
		$id = $colunas[0];

		// Paginação
		if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1')
		{
			$n = $_POST['iDisplayLength'];
			$offset = $_POST['iDisplayStart'];
		}
		else
		{
			if (isset($_POST['iDisplayLength']))
			{
				$n = ($_POST['iDisplayLength'] == '-1') ? NULL : $_POST['iDisplayLength'];
				$offset = 0;
			}
			else
			{
				$n = 25;
				$offset = 0;
			}
		}

		// Ordenação
		if(isset($_POST['iSortCol_0']))
		{
			$order = NULL;
			for($i=0;$i<intval($_POST['iSortingCols']);$i++)
			{
				if($_POST['bSortable_'.intval($_POST['iSortCol_'.$i])] == "true")
				{
					if(trim($colunas[intval($_POST['iSortCol_'.$i])]) == 'opcoes') { $order = NULL; break; }
					$order .= $colunas[intval($_POST['iSortCol_'.$i])]." ".$_POST['sSortDir_'.$i].", ";
				}
			}
			$order = rtrim($order, ' ,');
			if((trim($order) == 'ORDER BY') || (strlen($order) == 0)) {	$order = NULL; }
		}
		else
			$order = NULL;

		//Filtro
		$orlike = NULL;
		if(strlen($_POST['sSearch']) > 0)
		{
			$campo = array();
			$valor = array();
			for($i = 0; $i < (count($colunas)-1); $i++)
			{
				$campo[] = $colunas[$i];
				$valor[] = trim($_POST['sSearch']);
			}
			$orlike = array_combine($campo, $valor);
		}

		######################## ALTERAR
		//SQL Query
		$query = $this->municipio_m->get_all($n, $offset, NULL, NULL, $order, $orlike);

		// Resultado do filtro
		$query_filtro = $this->municipio_m->get_all(NULL, NULL, NULL, NULL, $order, $orlike);
		$filtro = $query_filtro->num_rows();

		// Total
		$query_total = $this->municipio_m
		->get_all();
		$total = $query_total->num_rows();

		//Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $total,
			"iTotalDisplayRecords" => $filtro,
			"aaData" => array()
		);

		######################## ALTERAR
		foreach($query->result() as $row)

		{
			$retorno = array();
			for($i=0;$i<count($colunas);$i++)
			{
				switch($colunas[$i])
				{
					case 'opcoes': // Ações
						$links = array();
						$links[] = anchor(admin_url($this->modulo->url.'/alterar/'.$row->{$this->municipio_m->_id}), 'alterar', array('class' => 'alterar'));
						$links[] = anchor(admin_url($this->modulo->url.'/excluir/'.$row->{$this->municipio_m->_id}), 'excluir', array('desc' => $row->{$this->municipio_m->_desc}, 'class' => 'excluir', 'onclick' => 'excluir();'));
						$retorno[] = implode(' | ', $links);
					break;
					case 'id_estado':
						$retorno[] = $estado[$row->id_estado];
					break;
					case 'desc_municipio':
						$capital = ($row->capital) ? img('img/icons/star-small.png') : '';
						$retorno[] = $row->desc_municipio.$capital;
					break;
					default: // Outras colunas
						$retorno[] = $row->{$colunas[$i]};
				}
			}
			$output['aaData'][] = $retorno;
		}
		echo json_encode($output);
	}

	/**
	 * Codifica registros em options de select
	 *
	 * @access	public
	 * @return	string
	 */
	public function ajax_option()
	{
		$return = '<option value=""></option>'."\n";
		$array = $this->municipio_m->get_array(array('id_estado' => $this->uri->segment($this->uri->total_segments())));
		foreach($array as $k => $v)
			$return .= '<option value="'.$k.'">'.$v.'</option>'."\n";
		echo $return;
	}

}

/* End of file municipio.php */
/* Location: ./app/modules/municipio/controllers/municipio.php */
