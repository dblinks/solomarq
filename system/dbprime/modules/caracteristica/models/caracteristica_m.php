<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Caracteristica_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Característica
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Caracteristica_m extends DBModel {

	var $_tabela = 'caracteristica';
	var $_id = 'id_caracteristica';
	var $_desc = 'desc_caracteristica';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_caracteristica ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Insere registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			$this->_desc	=> $dado['caracteristica'],
			'slug'	=> slug($dado['caracteristica'])
		));
	}

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::update(array(
			$this->_desc	=> $dado['caracteristica'],
			'slug'	=> slug($dado['caracteristica'])
		), $dado['id']);
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}

}

/* End of file Grupo_m.php */
/* Location: ./app/modules/grupo/models/Grupo_m.php */
