<div id="atalhos">
  <h6>Atalhos</h6>
  <ul>
    <li><?php echo anchor(admin_url('produto/adicionar'), 'Adicionar '.$this->modulo_m->get_field(NULL, 'produto', 'url'), array('class' => 'adicionar')); ?></li>
    <li><?php echo anchor(admin_url('produto'), 'Listar '.$this->modulo_m->get_field(NULL, 'produto', 'url'), array('class' => 'listar')); ?></li>
    <li><?php echo anchor(admin_url('categoria/adicionar'), 'Adicionar '.$this->modulo_m->get_field(NULL, 'categoria', 'url'), array('class' => 'adicionar')); ?></li>
    <li><?php echo anchor(admin_url('categoria'), 'Listar '.$this->modulo_m->get_field(NULL, 'categoria', 'url'), array('class' => 'listar')); ?></li>
    <li><?php echo anchor(admin_url($this->modulo->url.'/adicionar'), 'Adicionar '.$this->modulo->desc_modulo, array('class' => 'adicionar')); ?></li>
    <li><?php echo anchor(admin_url($this->modulo->url), 'Listar '.$this->modulo->desc_modulo, array('class' => 'listar')); ?></li>
  </ul>
  <br class="clear-both">
</div>