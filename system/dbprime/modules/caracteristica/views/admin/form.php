<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_caracteristica));

echo li();
echo form_label('Característica: ', 'caracteristica');
echo form_input(array('name' => 'caracteristica', 'id' => 'caracteristica', 'value' => set_value('caracteristica', @$desc_caracteristica), 'title' => 'Digite a característica do Produto/Serviço.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite a característica\'}}}', 'maxlength' => '50', 'size' => '25')).br();
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>