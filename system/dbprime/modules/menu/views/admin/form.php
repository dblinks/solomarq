<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_menu));
echo form_hidden('slug', $slug);

echo li();
echo form_label('Título: ', 'menu');
echo form_input(array('name' => 'menu', 'id' => 'menu', 'value' => set_value('menu', @$desc_menu), 'title' => 'Digite o título do item de menu, como irá aparecer no menu superior no site,', 'class' => 'dica titulo {validar:{required:true, messages:{required:\'Digite o título da página\'}}}', 'maxlength' => '200', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Mostrar: ', 'status');
echo form_dropdown('status', $this->config->item('simnao'), @$status, 'class="dica" title="Esta página deve ser mostrada?"');
echo li_close();

if($this->_acao == 'adicionar')
{
  $ordem_max = $this->db->select_max('ordem')->get($this->pagina_m->_tabela);
  foreach($ordem_max->result() as $ordem_m ) { $ordem = $ordem_m->ordem + 1; }
}
echo li();
echo form_label('Ordem: ', 'ordem');
echo form_input(array('name' => 'ordem', 'id' => 'ordem', 'value' => set_value('ordem', $ordem), 'class' => 'input numero dica', 'title' => 'Digite a ordem que deve aparecer no menu, pode ser alterada nas setas da listagem.', 'maxlength' => '2', 'size' => '5')).br();
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>