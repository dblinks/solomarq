<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Página - Controller
 *
 * @package 		Admin
 * @subpackage 	Página
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{

	private $validation_rules = array(
		array(
			'field'   => 'menu',
			'label'   => 'Descrição',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'status',
			'label'   => 'Status',
			'rules'   => 'required'
		)
	);

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu/menu_m');
    $this->load->model('sitemap_m');
	}

	/**
	 * Página principal - index
	 *
	 * @access	public
	 */
	public function index()
	{
		$this->template->write('conteudo', heading('Listando '.$this->modulo->alias, 1));
		$this->template->write_view('conteudo', 'admin/listar');
		$this->template->render();
	}

	/**
	 * Altera registro com validação de fomulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function alterar()
	{
		$a = $this->menu_m->get_id($this->uri->segment(4));
		$this->template->write('conteudo', heading('Alterar '.$this->modulo->alias.' : '.$a->{$this->menu_m->_desc}));
		$this->template->write_view('conteudo', 'admin/form', $a);

		// Dados recebidos
		if ($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				if($this->menu_m->update($this->input->post()))
				{
					$this->session->set_flashdata('msg', msg('Sucesso ao alterar '.$this->modulo->alias, 'sucesso'));
           $this->sitemap_m->gerar();
					redirect(admin_url($this->modulo->url));
				}
				else
				{
					$this->template->write('msg', msg('Erro ao alterar '.$this->modulo->alias, 'erro'));
				}
			}
			else
			{
				$this->template->write('msg', msg(validation_errors(), 'erro'));
			}
		}

		$this->template->render();
	}

	/**
	 * Altera ordem de exibição do item para abaixo
	 *
	 * @access	public
	 * @return	redirect
	 */
	function descer()
	{
		($this->menu_m->descer($this->uri->segment(4)))
			? $this->session->set_flashdata('msg', msg('Sucesso ao alterar a ordem.', 'sucesso'))
			: $this->session->set_flashdata('msg', msg('Não foi possível alterar a ordem.', 'erro'));

		redirect($this->agent->referrer());
	}

	/**
	 * Altera ordem de exibição do item para cima
	 *
	 * @access	public
	 * @return	redirect
	 */
	function subir()
	{
		($this->menu_m->subir($this->uri->segment(4)))
			? $this->session->set_flashdata('msg', msg('Sucesso ao alterar a ordem.', 'sucesso'))
			: $this->session->set_flashdata('msg', msg('Não foi possível alterar a ordem.', 'erro'));

		redirect($this->agent->referrer());
	}

	/**
	 * Exclui registro
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function excluir()
	{
		if($this->menu_m->delete($this->uri->segment(4)))
    {
			$this->session->set_flashdata('msg', msg('Sucesso ao excluir '.$this->modulo->alias, 'sucesso'));
      $this->sitemap_m->gerar();
    }
    else
			$this->session->set_flashdata('msg', msg('Erro ao excluir '.$this->modulo->alias, 'erro'));

		redirect(admin_url($this->modulo->url));
	}

	/**
	 * Codifica registros em Json
	 *
	 * @access	public
	 * @return	string
	 */
	function json_listar()
	{
		######################## ALTERAR
		//Colunas de retorno
		$colunas = array($this->menu_m->_desc, 'ordem', 'status', 'opcoes');
		$principal = $this->menu_m->get_array();
		$id_modulo_midia = $this->modulo_m->get_field('id_modulo', 'midia', 'url');

		//Campo de ID
		$id = $colunas[0];

		// Paginação
		if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1')
		{
			$n = $_POST['iDisplayLength'];
			$offset = $_POST['iDisplayStart'];
		}
		else
		{
			if (isset($_POST['iDisplayLength']))
			{
				$n = ($_POST['iDisplayLength'] == '-1') ? NULL : $_POST['iDisplayLength'];
				$offset = 0;
			}
			else
			{
				$n = 25;
				$offset = 0;
			}
		}

		// Ordenação
		if(isset($_POST['iSortCol_0']))
		{
			$order = $this->menu_m->_order.', ';
			for($i=0;$i<intval($_POST['iSortingCols']);$i++)
			{
				if($_POST['bSortable_'.intval($_POST['iSortCol_'.$i])] == "true")
				{
					if(trim($colunas[intval($_POST['iSortCol_'.$i])]) == 'opcoes') { $order = NULL; break; }
					$order .= $colunas[intval($_POST['iSortCol_'.$i])]." ".$_POST['sSortDir_'.$i].", ";
				}
			}
			$order = rtrim($order, ' ,');
			if((trim($order) == 'ORDER BY') || (strlen($order) == 0)) {	$order = NULL; }
		}
		else
			$order = NULL;

		//Filtro
		$orlike = NULL;
		if(strlen($_POST['sSearch']) > 0)
		{
			$campo = array();
			$valor = array();
			for($i = 0; $i < (count($colunas)-1); $i++)
			{
				$campo[] = $colunas[$i];
				$valor[] = trim($_POST['sSearch']);
			}
			$orlike = array_combine($campo, $valor);
		}

		######################## ALTERAR
		//SQL Query
		$query = $this->menu_m->get_all($n, $offset, NULL, NULL, $order, $orlike);

		// Resultado do filtro
		$query_filtro = $this->menu_m->get_all(NULL, NULL, NULL, NULL, $order, $orlike);
		$filtro = $query_filtro->num_rows();

		// Total
		$query_total = $this->menu_m->get_all();
		$total = $query_total->num_rows();

		//Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $total,
			"iTotalDisplayRecords" => $filtro,
			"aaData" => array()
		);

		######################## ALTERAR
		foreach($query->result() as $row)
		{
			$retorno = array();
			for($i=0;$i<count($colunas);$i++)
			{
				switch($colunas[$i])
				{
					case 'opcoes': // Ações
						$links = array();
						$links[] =  anchor(admin_url($this->modulo->url.'/alterar/'.$row->{$this->menu_m->_id}), 'alterar', array('class' => 'alterar'));
						(in_array($id_modulo_midia, $this->_permissao) && $this->modulo->midia && $row->slug == 'empresa') ? $links[] = anchor(admin_url('midia/adicionar/'.$this->modulo->url.'/'.$row->{$this->pagina_m->_id}), 'mídia', array('class' => 'midia')) : NULL;

            //if(is_null($links[0])) unset($links[0]);
            $retorno[] = implode(' | ', $links);
					break;
					case 'ordem':
						$ordem = '('.$row->ordem.') ';

						if($row->ordem == $total)
						{
							$ordem .=	anchor(admin_url($this->modulo->url.'/subir/'.$row->{$this->menu_m->_id}), img(site_url().'img/icons/arrow-090-medium.png'));
						}
						elseif($row->ordem > 1)
						{
							$ordem .=	anchor(admin_url($this->modulo->url.'/subir/'.$row->{$this->menu_m->_id}), img(site_url().'img/icons/arrow-090-medium.png')).' | ';
							$ordem .=	anchor(admin_url($this->modulo->url.'/descer/'.$row->{$this->menu_m->_id}),  img(site_url().'img/icons/arrow-270-medium.png'));
						}
						else
							$ordem .=	anchor(admin_url($this->modulo->url.'/descer/'.$row->{$this->menu_m->_id}),  img(site_url().'img/icons/arrow-270-medium.png'));

						$retorno[] = $ordem;
					break;
					case 'principal':
						$retorno[] = ($row->principal != 0) ? $principal[$row->principal] : NULL;
					break;
					case 'status':
						$retorno[] = center(status_icon($row->status, 'Sim', 'Não'));
					break;
					default: // Outras colunas
						$retorno[] = $row->{$colunas[$i]};
				}
			}
			$output['aaData'][] = $retorno;
		}
		echo json_encode($output);
	}
}
/* End of file admin.php */
/* Location: ./app/modules/menu/controllers/admin.php */
