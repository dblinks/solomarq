<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Pagina_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Página
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Menu_m extends DBModel {

	var $_tabela = 'menu';
	var $_id = 'id_menu';
	var $_desc = 'desc_menu';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'ordem ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	 /*
	public function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			'principal' => $dado['principal'],
			$this->_desc	=> $dado['pagina'],
			'texto'	=> $dado['texto'],
			'status'	=> $dado['status'],
			'ordem'	=> $dado['ordem']
		));
	}
	*/

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		// Se notícia estiver automática não exibe página de notícias
    if(($this->site->produto_servico == 1) && ($dado['slug'] == 'servico'))
		{
			$dado['status'] = 0;
		}

    if(($this->site->produto_servico == 2) && ($dado['slug'] == 'produto'))
		{
			$dado['status'] = 0;
		}

    unset($dado['slug']);

		return parent::update(array(
			$this->_desc	=> $dado['menu'],
			'status'	=> $dado['status'],
			'ordem'	=> $dado['ordem']
		), $dado['id']);
	}

	/**
	 * Incrementa 1 no campo ordem
	 *
	 * @access	public
	 * @param 	string	$id
	 * @return	boolean
	 */
	function descer($id)
	{
		$this->db->set('ordem', 'ordem + 1', FALSE);
		$this->db->where($this->_id, $id);
		$this->db->update($this->_tabela);

		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	/**
	 * Decrementa 1 no campo ordem
	 *
	 * @access	public
	 * @param 	string	$id
	 * @return	boolean
	 */
	function subir($id)
	{
		$this->db->set('ordem', 'ordem - 1', FALSE);
		$this->db->where($this->_id, $id);
		$this->db->update($this->_tabela);

		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
  /*
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
    $this->midia_m->delete_batch($this->modulo->url, $id);
		return parent::delete($id);
	}
  */
}

/* End of file menu_m.php */
/* Location: ./app/modules/menu/models/menu.php */
