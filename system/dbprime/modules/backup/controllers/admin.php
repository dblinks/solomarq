<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Backup - Controller
 *
 * @package 		Admin
 * @subpackage 	backup
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20120413
 */

class Admin extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('backup/backup_m');
	}


	public function index()
	{
    $this->efetuar();
    /*
		$this->template->write('acao', heading('Listando '.$this->modulo->alias, 2));
		$this->template->write_view('conteudo', 'admin/listar');

		$this->template->render();
     *
     */
	}

	public function efetuar()
	{
    /*
		$this->template->write('acao', heading('Efetuar '.$this->modulo->alias, 2));
		$this->template->write_view('conteudo', 'admin/form');

		// Dados recebidos
		if($this->input->post())
		{
      if($this->backup_m->efetuar($this->input->post()))
      {
        $this->session->set_flashdata('msg', alert('Sucesso ao efetuar '.$this->modulo->alias, 'success'));
        redirect(admin_url($this->modulo->url));
      }
      else
      {
        $this->template->write('msg', alert('Erro ao efetuar '.$this->modulo->alias.' ou nenhum selecionado', 'danger'));
      }
		}

		$this->template->render();
     *
     */
    $this->backup_m->efetuar($backup = array('site' => TRUE, 'bd' => TRUE));
	}

	public function excluir()
	{
    /*
		$this->backup_m->delete($this->uri->segment(4))
			? $this->session->set_flashdata('msg', alert('Sucesso ao excluir '.$this->modulo->alias, 'success'))
			: $this->session->set_flashdata('msg', alert('Erro ao excluir '.$this->modulo->alias, 'danger'));

		redirect(admin_url($this->modulo->url));
     *
     */
	}

  public function download()
	{
    //$this->backup_m->download($this->uri->segment(4));
	}

	function ajax_listar()
	{
    /*
		######################## ALTERAR
		//Colunas de retorno
		$colunas = array($this->backup_m->_id, $this->backup_m->_desc, 'status_site', 'status_bd', 'data', 'opcoes');

		//Campo de ID
		$id = $colunas[0];

		// Paginação
		if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1')
		{
			$n = $_POST['iDisplayLength'];
			$offset = $_POST['iDisplayStart'];
		}
		else
		{
			if (isset($_POST['iDisplayLength']))
			{
				$n = ($_POST['iDisplayLength'] == '-1') ? NULL : $_POST['iDisplayLength'];
				$offset = 0;
			}
			else
			{
				$n = 25;
				$offset = 0;
			}
		}

		// Ordenação
		if(isset($_POST['iSortCol_0']))
		{
			$order = NULL;
			for($i=0;$i<intval($_POST['iSortingCols']);$i++)
			{
				if($_POST['bSortable_'.intval($_POST['iSortCol_'.$i])] == "true")
				{
					if(trim($colunas[intval($_POST['iSortCol_'.$i])]) == 'opcoes') { $order = NULL; break; }
					$order .= $colunas[intval($_POST['iSortCol_'.$i])]." ".$_POST['sSortDir_'.$i].", ";
				}
			}
			$order = rtrim($order, ' ,');
			if((trim($order) == 'ORDER BY') || (strlen($order) == 0)) {	$order = NULL; }
		}
		else
			$order = NULL;

		//Filtro
		$orlike = NULL;
		if(strlen($_POST['sSearch']) > 0)
		{
			$campo = array();
			$valor = array();
			for($i = 0; $i < (count($colunas)-1); $i++)
			{
				$campo[] = $colunas[$i];
				$valor[] = trim($_POST['sSearch']);
			}
			$orlike = array_combine($campo, $valor);
		}

		######################## ALTERAR
		//SQL Query
		$query = $this->backup_m->get_all($n, $offset, NULL, NULL, $order, $orlike);

		// Resultado do filtro
		$query_filtro = $this->backup_m->get_all(NULL, NULL, NULL, NULL, $order, $orlike);
		$filtro = $query_filtro->num_rows();

		// Total
		$query_total = $this->backup_m->get_all();
		$total = $query_total->num_rows();

		//Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $total,
			"iTotalDisplayRecords" => $filtro,
			"aaData" => array()
		);

		######################## ALTERAR
		foreach($query->result() as $row)
		{
			$retorno = array();
			for($i=0;$i<count($colunas);$i++)
			{
				switch($colunas[$i])
				{
					case 'opcoes': // Ações
						$links = array();
						$links[] = (strlen($row->arquivo_site) > 0) ? anchor(admin_url($this->modulo->url.'/download/'.$row->arquivo_site), i('class="icon-download-alt"', nbs()).' Site', array('class' => 'btn btn-info')) : NULL;
            $links[] = (strlen($row->arquivo_bd) > 0) ? anchor(admin_url($this->modulo->url.'/download/'.$row->arquivo_bd), i('class="icon-download-alt"', nbs()).' BD', array('class' => 'btn btn-primary')) : NULL;
            //$links[] = anchor(admin_url($this->modulo->url.'/excluir/'.$row->{$this->backup_m->_id}), i('class="icon-remove"', nbs()).' Excluir', array('class' => 'btn btn-danger excluir', 'data-desc' => $row->{$this->backup_m->_desc}, 'onclick' => 'return false;'));
						$retorno[] = div('class="btn-group"').implode(nbs(), $links).div_close();
					break;
          case 'status_site':
            $retorno[] = status_label($row->status_site, 'Sucesso', 'Erro');
          break;
          case 'status_bd':
            $retorno[] = status_label($row->status_bd, 'Sucesso', 'Erro');
          break;
          case 'data':
            $retorno[] = data_hora_extenso($row->data);
          break;
					default: // Outras colunas
						$retorno[] = $row->{$colunas[$i]};
				}
			}
			$output['aaData'][] = $retorno;
		}
		echo json_encode($output);
     *
     */
	}

}
