<?php

/**
 * Backup_m - Model
 *
 * @package 		DB
 * @subpackage 	Módulo
 * @category 		Backup
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110413
 */

class Backup_m extends CI_Model {

	//var $_tabela = 'backup';
	//var $_id = 'id_backup';
	//var $_desc = 'desc_backup';
	//var $_order = 'desc_backup ASC';
  //var $dado = array();

  public $dir_backup = 'backup/';
	private $arquivo;

	function __construct()
	{
		parent::__construct();
    $this->load->library('zip');
		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->helper('download');
    $this->load->helper('directory');
	}

	function efetuar($dado)
	{
    $status = FALSE;

    $datahora = now();
    $semana_anterior = date('Ymd',mktime(0,0,0,date('m'),date('d')-7,date('Y')));

    $data_exe = date('Ymd', $datahora);

		(!is_dir($this->dir_backup)) ? mkdir($this->dir_backup) : NULL;
    @chmod($this->dir_backup, 0777);

    $bd = $dado['bd'];
    $site = $dado['site'];
    $log = '';

		if($site == TRUE)
		{
      $log .= 'Backup do Site - Início - '.date('H:i:s', now())."\r\n";
      $this->zip->clear_data();

      //$this->dado['desc_backup'] = 'Site';

      $log .= 'Leitura de diretórios - Início - '.date('H:i:s', now())."\r\n";

      // Diretórios
      $this->zip->read_dir('css/');     // CSS
      $this->zip->read_dir('img/');     // Imagens
      $this->zip->read_dir('js/');      // JavaScript
      $this->zip->read_dir('midia/');   // Mídias
      $this->zip->read_dir('system/');  // CodeIgniter
      $this->zip->read_dir('themes/');  // CodeIgniter

      $log .= 'Leitura de diretórios - Fim - '.date('H:i:s', now())."\r\n";

      $log .= 'Leitura de arquivos - Início - '.date('H:i:s', now())."\r\n";

      // Arquivos
      $this->zip->read_file('.htaccess');   // .htaccess
      $this->zip->read_file('dblinks.ico'); // Favicon DBLINKS
      $this->zip->read_file('favicon.ico'); // Favicon Site
      $this->zip->read_file('index.php');   // index.php (CodeIgniter)
      $this->zip->read_file('robots.txt');  // robots.txt (Robots para buscadores)
      $this->zip->read_file('sitemap.xml'); // simtemap.xml (Sitemap para buscadores)

      $log .= 'Leitura de arquivos - Fim - '.date('H:i:s', now())."\r\n";

      $log .= 'Compactando arquivo - Início - '.date('H:i:s', now())."\r\n";

			$this->arquivo = $this->dir_backup.'site_'.$data_exe.'.zip';

      $log .= 'Compactando arquivo - Fim - '.date('H:i:s', now())."\r\n";

      //$this->dado['arquivo_site'] = 'site_'.$data_exe.'.zip';
      $backup = $this->zip->archive($this->arquivo);
      //$this->dado['status_site'] = (bool)$backup;

      $log .= 'Backup do Site - Fim - '.date('H:i:s', now())."\r\n";
		}

    // Banco de dados
    if($bd == TRUE)
		{
      $this->zip->clear_data();

      $log .= 'Backup do Banco de Dados - Início - '.date('H:i:s', now())."\r\n";

      //$this->dado['desc_backup'] = isset($this->dado['desc_backup']) ? $this->dado['desc_backup'].' / Banco de Dados' : 'Banco de Dados';

			$config = array('newline' => "\r\n", 'format' => 'zip');

      $log .= 'SQL - Início - '.date('H:i:s', now())."\r\n";

			$backup_bd =& $this->dbutil->backup($config);

      $log .= 'SQL - Fim - '.date('H:i:s', now())."\r\n";

			$this->arquivo = $this->dir_backup.'bd_'.$data_exe.'.gz';
      $backup = write_file($this->arquivo, $backup_bd);

      $log .= 'Backup do Banco de Dados - Fim - '.date('H:i:s', now())."\r\n";
    }

    $log .= 'Apagando arquivos com data anterior a '.$semana_anterior.' - Início - '.date('H:i:s', now())."\r\n";

    $dir = directory_map($this->dir_backup);
    foreach($dir as $a)
    {
      $data_arquivo = date("YmdHis", filemtime($this->dir_backup.$a));
      if($data_arquivo < $semana_anterior)
      {
        @unlink($a);
        $log .= (unlink($this->dir_backup.$a)) ? 'Sucesso ao apagar arquivo '.$a.' - '.date('H:i:s', now())."\r\n" : 'Erro ao apagar arquivo '.$a.' - '.date('H:i:s', now())."\r\n";
      }
    }

    $log .= 'Apagando arquivos com data anterior a '.$semana_anterior.' - Fim - '.date('H:i:s', now())."\r\n";

    @write_file($this->dir_backup.'backup_'.$data_exe.'.bkp', $log);

    return $log;

		//return TRUE;
	}

  function insert()
  {
    return parent::insert($this->dado);
  }

  function download($arquivo)
  {
    $data = file_get_contents($this->dir_backup.$arquivo);
    force_download($arquivo, $data);
  }
}

/* End of file Backup_m.php */
/* Location: ./app/modules/backup/models/Backup_m.php */
// Adicionar tarefa Cron
// *	23	*	*	* wget http://www.editoramaisquepalavras.com.br/admin/backup/efetuar
