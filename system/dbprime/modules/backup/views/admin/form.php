<?php
echo form_open_multipart(current_url(), array('class' => 'form-striped form-horizontal validar'), array('id' => isset($id_categoria) ? $id_categoria : NULL));
echo form_fieldset('');

echo div('class="control-group"');
echo form_label('Tipo', 'backup', array('class' => 'control-label'));
echo div('class="controls"');
echo form_label(form_checkbox('site', 'site', FALSE, 'id="site"').' Site', 'site', array('class' => 'checkbox inline'));
echo form_label(form_checkbox('bd', 'bd', FALSE, 'id="bd"').' Banco de dados', 'bd', array('class' => 'checkbox inline'));
echo div_close();
echo div_close();

echo div('class="form-actions"');
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'btn btn-success', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => i('class="icon-ok"').' Efetuar'));
echo div_close();

echo form_fieldset_close();
echo form_close();
?>