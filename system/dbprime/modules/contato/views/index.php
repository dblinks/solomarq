<div id="esquerda" class="margem-esquerda">
  <div id="endereco">
  <span class="negrito">Preencha os campos ao lado<br /> e envie sua mensagem para nós.</span>
    <br /><br />
    Os campos marcados com <span class="obrigatorio">*</span><br /> são de preenchimento obrigatório.
    <?php
    $m = $this->municipio_m->get_id($this->empresa->id_municipio);
    $e = $this->estado_m->get_id($m->id_estado);

    echo br(2).heading('Onde estamos', 3, 'class="contato fonte-google"');

    echo '<span class="negrito">';
    echo $this->empresa->endereco.', '.$this->empresa->numero.' | '.$this->empresa->bairro.br();

    echo $m->desc_municipio.' - '.$e->uf.br().'CEP: '.$this->empresa->cep.br();
    echo 'email: '.str_replace('@', '@<span class="hidden">#</span>', $this->empresa->email).br();

    echo 'telefone: '.$this->empresa->telefone;
    echo '</span>';
    ?>
    <br />
    <?php echo anchor(site_url('contato/mapa'), img('img/contato.jpg'), 'class="mapa"'); ?>
    <div class="legenda">Clique no mapa para ampliar.</div>
  </div>
</div>
<div id="direita">
  <div id="formulario">
<?php
echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'));

echo '<table width="610px" border="0" cellspacing="0" cellpadding="0">'."\n";
echo '<tr>'."\n";
echo '<td colspan="4" width="610px">';
echo '<span class="obrigatorio">*</span>'.form_label('Assunto: ', 'assunto').br();
echo form_input(array('name' => 'assunto', 'id' => 'assunto', 'value' => set_value('assunto'), 'class' => 'l100f {validar:{required:true, messages:{required:\'Digite o assunto.\'}}}', 'maxlength' => '200', 'size' => '25'));
echo '</td>'."\n";
echo '</tr>'."\n";
echo '<tr>'."\n";
echo '<td colspan="2" width="305px">';
echo '<span class="obrigatorio">*</span>'.form_label('Nome Completo: ', 'nome').br();
echo form_input(array('name' => 'nome', 'id' => 'nome', 'value' => set_value('nome'), 'class' => 'l100 {validar:{required:true, messages:{required:\'Digite seu nome.\'}}}', 'maxlength' => '200', 'size' => '25'));
echo '</td>'."\n";
echo '<td colspan="2" width="305px">';
echo '<span class="obrigatorio">*</span>'.form_label('Endereço: ', 'endereco').br();
echo form_input(array('name' => 'endereco', 'id' => 'end', 'value' => set_value('endereco'), 'class' => 'l100f {validar:{required:true, messages:{required:\'Digite seu endereço.\'}}}', 'maxlength' => '200', 'size' => '25'));
echo '</td>'."\n";
echo '</tr>'."\n";
echo '<tr>'."\n";
echo '<td colspan="2" width="305px">';
echo '<span class="obrigatorio">*</span>'.form_label('Email: ', 'email').br();
echo form_input(array('name' => 'email', 'id' => 'email', 'value' => set_value('email'), 'class' => 'l100 {validar:{required:true,email:true, messages:{required:\'Digite seu email.\',email:\'Digite um email válido.\'}}}', 'maxlength' => '200', 'size' => '25'));
echo '</td>'."\n";
echo '<td width="152px">';
echo '<span class="obrigatorio">*</span>'.form_label('Telefone fixo: ', 'telefone_fixo').br();
echo form_input(array('name' => 'telefone_fixo', 'id' => 'telefone_fixo', 'value' => set_value('telefone_fixo'), 'class' => 'l100m telefone {validar:{required:true, messages:{required:\'Digite seu telefone fixo.\'}}}', 'maxlength' => '14', 'size' => '14'));
echo '</td>'."\n";
echo '<td>';
echo form_label('Telefone Celular: ', 'telefone_celular').br();
echo form_input(array('name' => 'telefone_celular', 'id' => 'telefone_celular', 'value' => set_value('telefone_celular'), 'class' => 'telefone l100f', 'maxlength' => '14', 'size' => '14'));
echo '</td>'."\n";
echo '</tr>'."\n";
echo '<tr>'."\n";
echo '<td colspan="2" width="305px">';
echo '<span class="obrigatorio">*</span>'.form_label('Cidade: ', 'cidade').br();
echo form_input(array('name' => 'cidade', 'id' => 'cidade', 'value' => set_value('cidade'), 'class' => 'l100 {validar:{required:true, messages:{required:\'Digite sua cidade.\'}}}', 'maxlength' => '200', 'size' => '25'));
echo '</td>'."\n";
echo '<td width="152px">';
echo '<span class="obrigatorio">*</span>'.form_label('Estado: ', 'estado').br();
echo form_dropdown('estado', $this->estado_m->get_array(), NULL,  'class="{validar:{required:true, messages:{required:\'Selecione seu estado.\'}}}"');
echo '</td>'."\n";
echo '<td>';
echo form_label('Bairro: ', 'bairro').br();
echo form_input(array('name' => 'bairro', 'id' => 'bairro', 'value' => set_value('bairro'), 'class' => 'l100f', 'maxlength' => '200', 'size' => '25'));
echo '</td>'."\n";
echo '</tr>'."\n";
echo '<tr>'."\n";
echo '<td colspan="4" width="610px">';
echo '<span class="obrigatorio">*</span>'.form_label('Mensagem: ', 'mensagem').br();
echo form_textarea(array('name' => 'mensagem', 'id' => 'mensagem', 'value' => set_value('mensagem'), 'title' => 'Digite sua mensagem.', 'class' => 'l100f {validar:{required:true, messages:{required:\'Digite sua mensagem.\'}}}', 'cols' => '50', 'rows' => '10')).br();
echo '</td>'."\n";
echo '</tr>'."\n";
echo '<tr>'."\n";
echo '<td colspan="2" width="305px">';
echo form_input(array('type' => 'image', 'src' => site_url('themes/'.$this->site->tema).'/img/enviar.png', 'name' => 'enviar', 'class' => 'left'));
echo '</td>'."\n";
echo '<td colspan="2" width="305px">';
echo '<div class="newsletter">';
echo form_checkbox('newsletter', '1', TRUE).nbs();
echo 'Receber novidades da '.$this->empresa->desc_empresa;
echo '</div>';
echo '</td>'."\n";
echo '</tr>'."\n";
echo '</table>'."\n";

echo form_close();

echo form_fieldset_close();

?>
  </div>
</div>