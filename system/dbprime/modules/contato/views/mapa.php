<?php
$m = $this->municipio_m->get_id($this->empresa->id_municipio);
$e = $this->estado_m->get_id($m->id_estado);

$this->cigmap->addMarkerByCoords($this->site->gm_longitude,$this->site->gm_latitude, $this->empresa->desc_empresa,'<span style="font-family:Arial;font-size:12px;"><b>'.img('image/get_image/'.$this->empresa->logotipo.':16:16').$this->empresa->desc_empresa.' - '.$this->empresa->slogan.'</b>'.br().$this->empresa->endereco.', '.$this->empresa->numero.' - '.$this->empresa->bairro.br().$this->empresa->telefone.br().$this->empresa->email.br().$m->desc_municipio.' - '.$e->uf.', '.$this->empresa->cep.'</span>','Tunápolis');
$this->cigmap->disableOverviewControl();
$this->cigmap->disableMapControls();
$this->cigmap->disableDirections();
$this->cigmap->enableInfoWindow();
$this->cigmap->disableTypeControls();
$this->cigmap->setInfoWindowTrigger('mouseover');
$this->cigmap->setWidth(700);
$this->cigmap->setHeight(500);
$this->cigmap->setZoomLevel(15);
$this->cigmap->setJSAlert('Por favor, habilite o Javascript para que o Google Maps funcione.');
$this->cigmap->setBrowserAlert("Desculpe, o Google Maps não é compatível com este navegador.");
$this->cigmap->setMapType(@$this->site->gm_tipo); // default
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo $this->empresa->desc_empresa; ?></title>
		<link rel="stylesheet" type="text/css" href="css/reset.css" />
    <?php $this->cigmap->printHeaderJS(); ?>
    <?php $this->cigmap->printMapJS(); ?>
    <!-- necessary for google maps polyline drawing in IE -->
    <style type="text/css">
      v\:* {
        behavior:url(#default#VML);
      }
    </style>
  </head>
  <body>
     <?php print $this->cigmap->printMap(); ?>
     <?php print $this->cigmap->printOnLoad(); ?>
  </body>
</html>