<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contato_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Contato
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20111103
 */

class Contato_m extends DBModel {

	var $_tabela = 'contato';
	var $_id = 'id_contato';
	var $_desc = 'desc_contato';
	var $_cri = 'data_cri';
	var $_order = 'desc_contato ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Insere registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			$this->_desc	=> $dado['assunto'],
      'nome'	=> $dado['nome'],
			'endereco'	=> $dado['endereco'],
      'email' => $dado['email'],
      'telefone_fixo' => $dado['telefone_fixo'],
      'telefone_celular' => $dado['telefone_celular'],
      'bairro' => $dado['bairro'],
      'cidade' => $dado['cidade'],
      'estado' => $dado['estado'],
      'mensagem' => $dado['mensagem'],
      'newsletter' => $dado['newsletter']
		));
	}

	/**
	 * Altera registro para lido
	 *
	 * @access	public
	 * @return	boolean
	 */
	function lido($dado = array())
	{
		return parent::update(array(
			'lido'	=> $dado['lido']
		), $dado['id']);
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}

}

/* End of file Grupo_m.php */
/* Location: ./app/modules/grupo/models/Grupo_m.php */
