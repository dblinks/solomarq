<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends Site_Controller {

	function __construct()
	{
		parent::__construct();
    $this->load->model('municipio/municipio_m');
    $this->load->model('estado/estado_m');
    $this->load->model('contato/contato_m');
	}

	public function index()
	{
     //JS
    $this->template->add_js('js/jquery.meio.mask.js');
    $this->template->add_js('js/jquery.metadata.js');
    $this->template->add_js('js/jquery.validate.min.js');
    $this->template->add_js('js/site/form.js');

    //CSS
    $this->template->add_css($this->tema_url('css/contato.css'));
    $this->template->add_css($this->tema_url('css/noticia.css'));

    // Título
    $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');
    $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($p->desc_menu), 'pagina' => $p->desc_menu));

    // Enviando
    if($this->input->post())
    {
      unset($_POST['enviar_x']);
      unset($_POST['enviar_y']);

      $post = $this->input->post();
      $post['estado'] = $this->estado_m->get_field('desc_estado', $post['estado']);

      // Envio do email
      $introducao = 'Novo email de '.$post['nome'];
      $mensagem = $post['mensagem'].br(2).$post['nome'].br().$post['endereco'].' - '.$post['bairro'].br().$post['email'].br().$post['telefone_fixo'].br().$post['telefone_celular'].br().$post['cidade'].' - '.$post['estado'];
      $assunto = $post['assunto'];
      $de_email = $post['email'];
      $de = $post['nome'];
      $para = $this->empresa->email;

      // Cadatro no banco de dados
      if(parent::envia_email($introducao, $mensagem, $assunto, $de_email, $de, $para) && $this->contato_m->insert($post))
      {
        $this->template->write('conteudo', msg('Sua mensagem foi enviada com sucesso.', 'sucesso margem corner'));
      }
      else
      {
        $this->template->write('conteudo', msg('Infelizmente sua mensagem não pode ser enviada, tente novamente mais tarde.', 'alerta margem corner'));
      }
    }

    // Título
    $this->_titulo[] = $p->desc_menu;
    $this->template->write_view('titulo', 'comum/title');

    $this->template->write_view('conteudo', 'index', $p);


		$this->template->render();
	}

  public function mapa()
  {
    $this->load->library('CiGMap');
    $this->template->set_template('no');
    $this->template->write_view('conteudo', 'mapa');
    $this->template->render();
  }

}

/* End of file site.php */
/* Location: ./application/controllers/site. */