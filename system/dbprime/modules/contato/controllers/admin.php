<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contato - Controller
 *
 * @package 		Admin
 * @subpackage 	Contato
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{
	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('contato/contato_m');
	}

	/**
	 * Página principal - index
	 *
	 * @access	public
	 */
	public function index()
	{
		$this->template->write('conteudo', heading('Listando Contatos', 1));
		$this->template->write_view('conteudo', 'admin/listar');
		$this->template->render();
	}

	/**
	 * Exclui registro
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function excluir()
	{
		$this->contato_m->delete($this->uri->segment(4))
			? $this->session->set_flashdata('msg', msg('Mensagem excluída com sucesso!', 'sucesso'))
			: $this->session->set_flashdata('msg', msg('Erro ao excluir MEnsagem!', 'erro'));

		redirect(admin_url($this->modulo->url));
	}

  /**
	 * Visualiza registro
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function visualizar()
	{
    $contato = $this->contato_m->get_id($this->uri->segment(4));
    $this->contato_m->lido(array('lido' => 1, 'id' => $this->uri->segment(4)));
		$this->template->write_view('conteudo', 'admin/visualizar', $contato);
		$this->template->render();
	}

	/**
	 * Codifica registros em Json
	 *
	 * @access	public
	 * @return	string
	 */
	function json_listar()
	{
		$colunas = array($this->contato_m->_id, $this->contato_m->_desc, 'nome', 'datahora', 'lido', 'opcoes');

		//Campo de ID
		$id = $colunas[0];

		// Paginação
		if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1')
		{
			$n = $_POST['iDisplayLength'];
			$offset = $_POST['iDisplayStart'];
		}
		else
		{
			if (isset($_POST['iDisplayLength']))
			{
				$n = ($_POST['iDisplayLength'] == '-1') ? NULL : $_POST['iDisplayLength'];
				$offset = 0;
			}
			else
			{
				$n = 25;
				$offset = 0;
			}
		}

		// Ordenação
		if(isset($_POST['iSortCol_0']))
		{
			$order = NULL;
			for($i=0;$i<intval($_POST['iSortingCols']);$i++)
			{
				if($_POST['bSortable_'.intval($_POST['iSortCol_'.$i])] == "true")
				{
					if(trim($colunas[intval($_POST['iSortCol_'.$i])]) == 'opcoes') { $order = NULL; break; }
					$order .= $colunas[intval($_POST['iSortCol_'.$i])]." ".$_POST['sSortDir_'.$i].", ";
				}
			}
			$order = rtrim($order, ' ,');
			if((trim($order) == 'ORDER BY') || (strlen($order) == 0)) {	$order = NULL; }
		}
		else
			$order = NULL;

		//Filtro
		$orlike = NULL;
		if(strlen($_POST['sSearch']) > 0)
		{
			$campo = array();
			$valor = array();
			for($i = 0; $i < (count($colunas)-1); $i++)
			{
				$campo[] = $colunas[$i];
				$valor[] = trim($_POST['sSearch']);
			}
			$orlike = array_combine($campo, $valor);
		}

		######################## ALTERAR
		//SQL Query
		$query = $this->contato_m->get_all($n, $offset, NULL, NULL, $order, $orlike);

		// Resultado do filtro
		$query_filtro = $this->contato_m->get_all(NULL, NULL, NULL, NULL, $order, $orlike);
		$filtro = $query_filtro->num_rows();

		// Total
		$query_total = $this->contato_m->get_all();
		$total = $query_total->num_rows();

		//Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $total,
			"iTotalDisplayRecords" => $filtro,
			"aaData" => array()
		);

		######################## ALTERAR
		foreach($query->result() as $row)
		{
			$retorno = array();
			for($i=0;$i<count($colunas);$i++)
			{
				switch($colunas[$i])
				{
					case 'opcoes': // Ações
						$links = array();
            $links[] = anchor(admin_url($this->modulo->url.'/visualizar/'.$row->{$this->contato_m->_id}), 'visualizar', array('desc' => $row->{$this->contato_m->_desc}, 'class' => 'visualizar'));
						$links[] = anchor(admin_url($this->modulo->url.'/excluir/'.$row->{$this->contato_m->_id}), 'excluir', array('desc' => $row->{$this->contato_m->_desc}, 'class' => 'excluir', 'onclick' => 'excluir();'));
						$retorno[] = implode(' | ', $links);
					break;
        	case 'lido':
						$retorno[] = center(status_icon($row->lido, 'Sim', 'Não'));
					break;
					case 'datahora':
						$retorno[] = data_hora_extenso($row->datahora);
					break;
					default: // Outras colunas
						$retorno[] = $row->{$colunas[$i]};
				}
			}
			$output['aaData'][] = $retorno;
		}
		echo json_encode($output);
	}
}

/* End of file noticia.php */
/* Location: ./app/modules/noticia/controllers/noticia.php */
