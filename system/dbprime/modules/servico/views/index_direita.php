<div id="direita">
  <?php echo heading($desc_produto, 2, array('class' => 'fonte-google')); ?>
  <div id="texto">
    <?php
// Imagem principal
    $principal = $this->midia_m->get_midia('produto', $id_produto, TRUE);
    if(!is_null($principal))
    {
      echo '<div id="principal">';
      echo anchor($this->config->item('dir_midia').$principal->img_gd, img(array('src' => site_url('image/get_image_c/'.$principal->img_me.':270:185'), 'align' => 'left')), 'title="'.$principal->desc_midia.'" class="galeria" rel="gal"');
      echo '<div id="lupa">';
      echo img('img/lupa.png');
      echo '</div>';
      if(strlen($principal->desc_midia) > 0)
        echo '<div id="legenda">'.$principal->desc_midia.'.</div>';
      echo '</div>';
    }

    // Texto
    echo $texto;

    // Vídeo
    if(strlen($video) > 0)
    {
      echo '<div class="clear-both"></div>';
      echo heading('Vídeo', 1, 'class="imagens"');
      echo '<div id="video"><iframe width="328" height="246" src="http://www.youtube.com/embed/'.$video.'?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>';
    }

    // Imagens
    $imagens = $this->midia_m->get_midia('produto', $id_produto, 0);
    if(!is_null($imagens) && ($imagens->num_rows() > 0))
    {
      echo '<div class="clear-both"></div>';
      echo br(2).heading('Imagens', 1, 'class="imagens"');
      echo '<div class="imagens">';
      foreach($imagens->result() as $img)
      {
        echo anchor($this->config->item('dir_midia').$img->img_gd, img(array('src' => site_url('image/get_image_c/'.$img->img_me.':100:75'), 'align' => 'left')), 'title="'.$img->desc_midia.'" class="galeria" rel="gal"');
      }
      echo '</div>';
    }
    ?>
  </div>
</div>
