<div id="esquerda" class="margem-esquerda">
  <div id="servicos">
    <?php
    $categoria = $this->categoria_m->get_array();
    foreach($servicos->result() as $s)
    {
    ?>
    <div class="servico">
      <div class="categoria"><?php echo $categoria[$s->id_categoria]; ?></div><div class="clear-both"></div>
      <h4><a href="<?php echo site_url('servico/ver/'.$s->id_produto.'/'.slug($s->desc_produto).'/'.$this->uri->slash_segment(5).$this->uri->slash_segment(6))?>"><?php echo bd2br($s->desc_produto); ?></a></h4>
    </div>
    <?php
    }

    $config['base_url'] = site_url($this->uri->slash_segment(1).$this->uri->slash_segment(2).$this->uri->slash_segment(3).$this->uri->slash_segment(4).'pg');
    $config['total_rows'] = $total;
    $config['per_page'] = $this->config->item('servico_por_pagina');
    $config['num_links'] = 3;
    $config['page_query_string'] = FALSE;
    $config['uri_segment'] = 6;

    $config['full_tag_open'] = '<ul id="paginacao">';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = 'Primeira';
    $config['first_tag_open'] = '<li class="primeira">';
    $config['first_tag_close'] = '</li>';
    $config['last_link'] = 'Última';
    $config['last_tag_open'] = '<li class="ultima">';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = 'Próxima';
    $config['next_tag_open'] = '<li class="proxima">';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'Anterior';
    $config['prev_tag_open'] = '<li class="anterior">';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="negrito destaque current">';
    $config['cur_tag_close'] = '</li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);

    echo $this->pagination->create_links();
    ?>
  </div>
</div>
