<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Produto_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Produto
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Produto_m extends DBModel {

	var $_tabela = 'produto';
	var $_id = 'id_produto';
	var $_desc = 'desc_produto';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_produto ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			'id_categoria'	=> $dado['categoria'],
			$this->_desc	=> $dado['produto'],
			'texto'	=> $dado['texto'],
      'video'	=> $dado['video'],
			'destaque' => $dado['destaque'],
			'status'	=> $dado['status'],
			'tipo'	=> $dado['tipo'],
			'slug'	=> slug($dado['produto']),
      'valor' => v2p($dado['valor']),
      'parcela' => $dado['parcela'],
      'parcela_valor' => v2p($dado['parcela_valor'])
		));
	}

  /**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function detalhe_insert(Array $dado, $tabela)
	{
		return parent::insert(array(
			'id_produto'	=> $dado['produto'],
			'desc_produto_detalhe'	=> $dado['detalhe'],
			'texto' => $dado['texto']
		), $tabela);
	}

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::update(array(
			'id_categoria'	=> $dado['categoria'],
			$this->_desc	=> $dado['produto'],
			'texto'	=> $dado['texto'],
      'video'	=> $dado['video'],
			'destaque' => $dado['destaque'],
			'status'	=> $dado['status'],
			'tipo'	=> $dado['tipo'],
			'slug'	=> slug($dado['produto']),
      'valor' => v2p($dado['valor']),
      'parcela' => $dado['parcela'],
      'parcela_valor' => v2p($dado['parcela_valor'])
		), $dado['id']);
	}

   /**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function detalhe_update(Array $dado, $tabela)
	{
		return parent::update(array(
			'desc_produto_detalhe'	=> $dado['detalhe'],
			'texto' => $dado['texto']
		), $dado['id'], $tabela, 'id_produto_detalhe');
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
  //TODO deletar mídia
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
    $qry = $this->produto_m->get_all(NULL, NULL, array($this->produto_m->_id => $id), NULL, 'desc_produto_detalhe ASC', NULL, NULL, NULL, NULL, NULL, 'produto_detalhe');
    if($qry->num_rows() > 0)
    {
      foreach ($qry->result() as $d){
        $this->detalhe_delete($d->id_produto_detalhe, 'produto_detalhe', 'id_produto_detalhe');
      }
    }

    $this->midia_m->delete_batch($this->modulo->url, $id);

    return parent::delete($id);
	}

  /**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function detalhe_delete($id, $tabela, $id_tabela)
	{
		return parent::delete($id, $tabela, $id_tabela);
	}

  /**
	 * Pega detalhes
	 *
	 * @access	public
	 * @return	boolean
	 */
	function get_detalhe($id)
	{
    return parent::get_all(NULL, NULL, array('id_produto' => $id), NULL, 'desc_produto_detalhe ASC', NULL, NULL, NULL, NULL, NULL, 'produto_detalhe');
	}


}

/* End of file Produto_m.php */
/* Location: ./app/modules/produto/models/Produto_m.php */
