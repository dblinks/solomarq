<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servico extends Site_Controller {

	function __construct()
	{
		parent::__construct();
    $this->load->model('produto/produto_m');
    $this->load->model('categoria/categoria_m');
    $this->load->library('pagination');

    $this->js_css();
	}

  function js_css()
  {
    //CSS
    $this->template->add_css($this->tema_url('css/servico.css'));
    $this->template->add_css($this->tema_url('css/noticia.css'));
  }

	public function index()
	{
    // Seleciona último servico para exibir
    $last = $this->produto_m->get_all(1, NULL, array('status' => 1, 'tipo' => 2), NULL, 'data_cri DESC, desc_produto ASC');
    foreach($last->result() as $l){ }
    $s = $this->produto_m->get_id($l->id_produto);

    // Redireciona para exibição da última notícia
    redirect(current_url().'/ver/'.$s->id_produto.'/'.$s->slug);
	}

  public function ver()
  {
    // Notícia a ser exibida
    $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');
    $s = $this->produto_m->get_id($this->uri->segment(3));

    // Título
    $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($p->desc_menu), 'pagina' => $p->desc_menu));

    // Todas notícias
    $todas = $this->produto_m->get_all(NULL, NULL, array('status' => 1, 'tipo' => 2), NULL, 'data_cri DESC, desc_produto ASC');

    // Lista de notícias lateral
    $ate = $this->uri->segment(6) ? ($this->config->item('servico_por_pagina')*($this->uri->segment(6)-1)) : NULL;
    $servicos = $this->produto_m->get_all($this->config->item('servico_por_pagina'), $ate, array('status' => 1, 'tipo' => 2 ), NULL, 'data_cri DESC, desc_produto ASC');

    // Título
    $this->_titulo[] = $p->desc_menu;
    $this->_titulo[] = $s->{$this->produto_m->_desc};
    $this->template->write_view('titulo', 'comum/title');

    $this->template->write_view('conteudo', 'index_esquerda', array('servicos' => $servicos, 'total' => $todas->num_rows()));
    $this->template->write_view('conteudo', 'index_direita', $s);
		$this->template->render();
  }
}

/* End of file site.php */
/* Location: ./application/controllers/site.php */