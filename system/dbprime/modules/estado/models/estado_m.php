<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Estado_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Estado
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link	
 * @version 		1.0 #20110921	
 */

class Estado_m extends DBModel {

	var $_tabela = 'estado';
	var $_id = 'id_estado';
	var $_desc = 'desc_estado';		
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_estado ASC';
	
	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Função executada antes da inserção
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function _before_insert($dado)
	{		
		$a = $this->get_by_uf($dado['uf']);
		if ($a->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('UF já utilizada em outro país', 'erro'));
			return FALSE;
		}
		
		return TRUE;
	}
	
	/**
	 * Função executada antes daalteração
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function _before_update($dado, $id = NULL)
	{
		$this->db->where_not_in($this->_id, $id);
		$a = $this->get_by_uf($dado['uf']);
		if ($a->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('UF já utilizada em outro país', 'erro'));
			return FALSE;
		}

		return TRUE;
	}		
		
	/**
	 * Insere registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		if(!$this->_before_insert($dado)) { return  FALSE; }		
		return parent::insert(array(
			'id_pais' => $dado['pais'],
			$this->_desc	=> $dado['estado'],
			'slug'	=> slug($dado['estado']),
			'uf'	=> $dado['uf']
		));
	}
	
	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */	
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		if(!$this->_before_update($dado, $dado['id'])) { return  FALSE; }		
		return parent::update(array(
			'id_pais' => $dado['pais'],
			$this->_desc => $dado['estado'],
			'slug'	=> slug($dado['estado']),
			'uf'	=> $dado['uf']
		), $dado['id']);
	}					
		
	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */			
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}								
}

/* End of file Estado_m.php */
/* Location: ./app/modules/estado/models/Estado_m.php */
