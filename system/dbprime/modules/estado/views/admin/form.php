<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_estado));

echo li();
echo form_label('Estado: ', 'estado');
echo form_input(array('name' => 'estado', 'id' => 'estado', 'value' => set_value('estado', @$desc_estado), 'title' => 'Digite o nome do estado.', 'class' => 'dica titulo {validar:{required:true, messages:{required:\'Digite o estado\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('URI: ', 'slug');
echo form_input(array('name' => 'slug', 'id' => 'slug', 'value' => set_value('slug', @$slug), 'title' => 'Digite a URL do estado.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite a URI\'}}}', 'maxlength' => '200', 'size' => '25', 'readonly' => 'readonly' )).br();
echo li_close();

echo li();
echo form_label('UF: ', 'uf');
echo form_input(array('name' => 'uf', 'id' => 'uf', 'value' => set_value('uf', @$uf), 'title' => 'Digite a Unidade Federativa (UF) do estado.', 'class' => 'dica corner uf {validar:{minlength:2, messages:{minlength:\'O tamanho é 2 caracteres\'}}}', 'alt' => 'Digite a UF (Unidade Federativa)', 'maxlength' => '2', 'size' => '5')).br();
echo li_close();

echo li();
echo form_label('Pais: ', 'pais');
echo form_dropdown('pais', $this->pais_m->get_array(), set_value(33, @$id_pais), 'id="pais" title="Selecione o país do estado." class="dica corner {validar:{required:true, messages:{required:\'Selecione um país\'}}}"').br();
echo li_close();

echo br(2);

if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>