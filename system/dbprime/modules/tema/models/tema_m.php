<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Config_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Config
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Tema_m extends DBModel {

	var $_tabela = 'config';
	var $_id = 'id_config';
	var $_desc = 'desc_config';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_config ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::update(array(
			'tema'	=> $dado['tema']
		), $dado['id']);
	}
}

/* End of file Config_m.php */
/* Location: ./app/modules/config/models/Config_m.php */
