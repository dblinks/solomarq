<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Tema - Controller
 *
 * @package 		Admin
 * @subpackage 	Tema
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('tema/tema_m');
		$this->load->helper('directory');
	}

	/**
	 * Lista os registros
	 *
	 * @access	public
	 */
	public function index()
	{
		$this->template->write('conteudo', heading('Listando Temas', 1));
		$this->template->write_view('conteudo', 'admin/listar');

		$this->template->render();
	}

  public function aplicar()
  {
    if($this->tema_m->update(array('tema' => $this->uri->segment(4), 'id' => 1)))
      $this->session->set_flashdata('msg', msg('Sucesso ao aplicar o tema.', 'sucesso'));
    else
      $this->template->write('msg', msg('Erro ao aplicar o tema.', 'erro'));

    redirect(admin_url($this->modulo->url));
  }
}

/* End of file tema.php */
/* Location: ./app/modules/tema/controllers/tema.php */