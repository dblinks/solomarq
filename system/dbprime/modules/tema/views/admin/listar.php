<?php
$dir_temas = 'themes/';
$diretorio = directory_map($dir_temas, TRUE, TRUE);
$arr_temas = array();

$this->table->set_heading(array('Thumbnail', 'Título', 'Versão', 'Ativo', 'Opções'));

$this->site->tema = (!isset($this->site->tema)) ? 'padrao' : $this->site->tema;

foreach ($diretorio as $tema)
{
  if($tema == '.DS_Store')
     continue;
  include($dir_temas.$tema.'/config.php');

  $this->table->add_row(array(
		(file_exists($dir_temas.$dir_tema.'/thumb.jpg')) ? img($dir_temas.$dir_tema.'/thumb.jpg') : 'Não disponível',
    $tema,
    $versao,
    center(status_icon($this->site->tema == $dir_tema), 'Sim', 'Não'),
		anchor(admin_url($this->modulo->url.'/aplicar/'.$dir_tema), 'aplicar', array('class' => 'aplicar'))
	));
}

$this->table->set_template($this->config->item('tabela_full'));

echo $this->table->generate().br();
?>
