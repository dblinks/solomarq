<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_secao));

echo li();
echo form_label('Seção: ', 'secao');
echo form_input(array('name' => 'secao', 'id' => 'secao', 'value' => set_value('secao', @$desc_secao), 'title' => 'Digite a seção. Divisão do menu lateral.', 'class' => 'dica titulo {validar:{required:true, messages:{required:\'Digite a seção\'}}}', 'maxlength' => '50', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('URL: ', 'url');
echo form_input(array('name' => 'url', 'id' => 'slug', 'value' => set_value('url', @$url), 'title' => 'Digite a url da seção, se houver.', 'class' => 'dica', 'maxlength' => '50', 'size' => '25')).br();
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>