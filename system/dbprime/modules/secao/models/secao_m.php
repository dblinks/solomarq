<?php

/**
 * Secao_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Secao
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link	
 * @version 		1.0 #20110921	
 */
 
class Secao_m extends DBModel {

	var $_tabela = 'secao';
	var $_id = 'id_secao';
	var $_desc = 'desc_secao';		
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_secao ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
	}
		
	/**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */		
	function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			$this->_desc	=> $dado['secao'],
			'url'	=> $dado['url'],
			'slug'	=> slug($dado['secao'])
		));
	}
	
	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */			
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::update(array(
			$this->_desc	=> $dado['secao'],
			'url'	=> $dado['url'],
			'slug'	=> slug($dado['secao'])
		), $dado['id']);
	}
		
	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */			
	 /*
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}				
	*/
	/**
	 * Incrementa 1 no campo ordem 
	 *
	 * @access	public
	 * @param 	string	$id
	 * @return	boolean
	 */		
	function descer($id)
	{
		$this->db->set('ordem', 'ordem + 1', FALSE);
		$this->db->where($this->_id, $id);
		$this->db->update($this->_tabela);

		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}	
		
	/**
	 * Decrementa 1 no campo ordem 
	 *
	 * @access	public
	 * @param 	string	$id
	 * @return	boolean
	 */		
	function subir($id)
	{
		$this->db->set('ordem', 'ordem - 1', FALSE);
		$this->db->where($this->_id, $id);
		$this->db->update($this->_tabela);

		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
}

/* End of file secao_m.php */
/* Location: ./app/modules/secao/models/secao_m.php */
