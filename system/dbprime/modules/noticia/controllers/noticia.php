<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticia extends Site_Controller {

	function __construct()
	{
		parent::__construct();
    $this->load->model('noticia/noticia_m');
    $this->load->library('pagination');

    $this->js_css();
	}

  function js_css()
  {
    //CSS
    $this->template->add_css($this->tema_url('css/noticia.css'));
  }

	public function index()
	{
    // Seleciona última notícia para exibir
    $last = $this->noticia_m->get_all(1, NULL, array('status' => 1), NULL, 'data DESC, data_cri DESC, desc_noticia ASC');

    if($last->num_rows() > 0)
    {
      foreach($last->result() as $l){ }
      $n = $this->noticia_m->get_id($l->id_noticia);
      // Redireciona para exibição da última notícia
      redirect(current_url().'/ver/'.$n->id_noticia.'/'.$n->slug);
    }
    else
    {
      $this->template->write('conteudo', 'Não há notícias cadastradas.');
      $this->template->render();
    }
	}

  public function ver()
  {
    // Notícia a ser exibida
    $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');
    $n = $this->noticia_m->get_id($this->uri->segment(3));

    // Título
    $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($p->desc_menu, $n->desc_noticia), 'pagina' => $p->desc_menu));

    // Todas notícias
    $todas = $this->noticia_m->get_all(NULL, NULL, array('status' => 1), NULL, 'data DESC, data_cri DESC, desc_noticia ASC');

    // Lista de notícias lateral
    $ate = $this->uri->segment(5) ? ($this->config->item('noticia_por_pagina')*($this->uri->segment(5)-1)) : NULL;
    $noticias = $this->noticia_m->get_all($this->config->item('noticia_por_pagina'), $ate, array('status' => 1), NULL, 'data DESC, data_cri DESC, desc_noticia ASC');

    // Título
    $this->_titulo[] = $p->desc_menu;
    $this->_titulo[] = $n->desc_noticia;
    $this->template->write_view('titulo', 'comum/title');

    $this->template->write_view('conteudo', 'index_esquerda', array('noticias' => $noticias, 'total' => $todas->num_rows()));
    $this->template->write_view('conteudo', 'index_direita', $n);
		$this->template->render();
  }
}

/* End of file site.php */
/* Location: ./application/controllers/site.php */