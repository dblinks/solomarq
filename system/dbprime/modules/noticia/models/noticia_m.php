<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Noticia_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Notícia
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link	
 * @version 		1.0 #20110921	
 */
 
class Noticia_m extends DBModel {

	var $_tabela = 'noticia';
	var $_id = 'id_noticia';
	var $_desc = 'desc_noticia';		
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_noticia ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */	
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			$this->_desc	=> $dado['noticia'],
			'slug'	=> slug($dado['noticia']),
			'texto'	=> $dado['texto'],
			'fonte'	=> $dado['fonte'],
			'fonte_url'	=> $dado['fonte_url'],
			'status'	=> $dado['status'],	
			'data'	=> br2bd($dado['data'])
		));
	}
		
	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */	
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::update(array(
			$this->_desc	=> $dado['noticia'],
			'slug'	=> slug($dado['noticia']),
			'texto'	=> $dado['texto'],
			'fonte'	=> $dado['fonte'],
			'fonte_url'	=> $dado['fonte_url'],
			'status'	=> $dado['status'],	
			'data'	=> br2bd($dado['data'])
		), $dado['id']);
	}				
		
	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */			
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}					
}

/* End of file Noticia_m,.php */
/* Location: ./app/modules/noticia/models/noticia.php */
