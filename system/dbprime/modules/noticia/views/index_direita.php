<div id="direita">
  <?php echo heading($desc_noticia, 2, array('class' => 'fonte-google')); ?>
  <div id="texto">
    <?php
// Imagem principal
    $principal = $this->midia_m->get_midia($this->uri->segment(1), $id_noticia, TRUE);
    if(!is_null($principal))
    {
      echo '<div id="principal">';
      echo anchor($this->config->item('dir_midia').$principal->img_gd, img(array('src' => site_url('image/get_image_c/'.$principal->img_me.':270:185'), 'align' => 'left')), 'title="'.$principal->desc_midia.'" class="galeria" rel="gal"');
      echo '<div id="lupa">';
      echo img('img/lupa.png');
      echo '</div>';
      if(strlen($principal->desc_midia) > 0)
        echo '<div id="legenda">'.$principal->desc_midia.'.</div>';
      echo '</div>';
    }

    // Texto
    echo $texto;

    if(strlen($fonte) > 0)
    {
      echo br().'<div class="fonte_data">';
      echo '<span class="negrito">Fonte: </span>';
      echo (strlen($fonte_url) > 0) ? anchor($fonte_url, $fonte, 'target="_blank"').'.' : $fonte.'.';
      echo br().'<span class="negrito">Publicada em: </span>';
      echo data_hora_extenso($data, FALSE);
      echo '.</div>';
    }

    // Imagens
    $imagens = $this->midia_m->get_midia($this->uri->segment(1), $id_noticia, FALSE);
    if(!is_null($imagens) && ($imagens->num_rows() > 0))
    {
      echo br(2).heading('Imagens', 1, 'class="imagens"');
      echo '<div class="imagens">';
      foreach($imagens->result() as $img)
      {
        echo anchor($this->config->item('dir_midia').$img->img_gd, img(array('src' => site_url('image/get_image_c/'.$img->img_me.':100:75'), 'align' => 'left')), 'title="'.$principal->desc_midia.'" class="galeria" rel="gal"');
      }
      echo '</div>';
    }
    ?>
  </div>
</div>
