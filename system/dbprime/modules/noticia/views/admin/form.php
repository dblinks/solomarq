<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_noticia));

echo li();
echo form_label('Título: ', 'noticia');
echo form_input(array('name' => 'noticia', 'id' => 'noticia', 'value' => set_value('noticia', @$desc_noticia), 'title' => 'Digite o título da notícia.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o título da notícia\'}}}', 'maxlength' => '200', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Texto: ', 'texto');
echo form_textarea(array('name' => 'texto', 'id' => 'texto', 'value' => set_value('texto', @$texto), 'title' => 'Digite o texto da notícia.', 'class' => 'dica editor {validar:{required:true, messages:{required:\'Digite o texto\'}}}', 'cols' => '50', 'rows' => '10')).br();
echo li_close();

echo li();
echo form_label('Fonte: ', 'fonte');
echo form_input(array('name' => 'fonte', 'id' => 'fonte', 'value' => set_value('fonte', @$fonte), 'title' => 'Digite a fonte da notícia.', 'class' => 'dica', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Fonte URL: ', 'fonte_url');
echo form_input(array('name' => 'fonte_url', 'id' => 'fonte_url', 'value' => set_value('fonte_url', @$fonte_url), 'title' => 'Digite o link da fonte da notícia.', 'class' => 'dica {validar:{url:true, messages:{url:\'Digite uma url válida (com http://)\'}}}', 'maxlength' => '255', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Mostrar: ', 'status');
echo form_dropdown('status', $this->config->item('simnao'), (isset($status) && @$status == 0 ) ? 0 : 1, 'class="dica" title="Esta notícia deve ser mostrada?"');
echo li_close();

echo li();
echo form_label('Data: ', 'data');
echo form_input(array('name' => 'data', 'id' => 'datas', 'value' => set_value(bd2br(@$data), date("d/m/Y")), 'title' => 'Digite ou selecione a data da notícia.', 'class' => 'dica data calendario-mes {validar:{datebr:true, messages:{datebr:\'Digite a data no formato dd/mm/yyyy\'}}}', 'maxlength' => '10', 'size' => '25')).br();
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>