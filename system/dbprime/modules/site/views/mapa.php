<?php
// Institucional
echo '<div class="col33">';
echo heading('Institucional', 2);
echo anchor(site_url('empresa'), heading('Empresa', 3));
if(in_array($this->site->produto_servico, array(0, 1)))
{
  $paginas = $this->pagina_m->get_all(NULL, NULL, array('status' => 1));
  if($paginas->num_rows() > 1)
  {
    foreach($paginas->result() as $p)
      echo anchor(site_url('empresa/ver/'.$p->id_pagina.'/'.$p->slug), $p->desc_pagina).br();
  }
}
echo anchor(site_url('representante'), heading('Representantes', 3)).br();
echo '</div>';

echo '<div class="col33">';
$proser = $this->config->item('pro_ser');
echo heading($proser[$this->site->produto_servico], 2);
// Produto
if(in_array($this->site->produto_servico, array(0, 1)))
{
  echo heading('Produtos', 3);
  $categorias = $this->categoria_m->get_all(NULL, NULL, array('tipo' => 1));
  foreach($categorias->result() as $c)
    echo anchor(site_url('produto/categoria/'.$c->id_categoria.'/'.$c->slug), $c->desc_categoria).br();
  echo br();
}

// Serviço
if(in_array($this->site->produto_servico, array(0, 1)))
{
  echo heading('Serviços', 3);
  $servicos = $this->produto_m->get_all(NULL, NULL, array('tipo' => 2));
  foreach($servicos->result() as $s)
    echo anchor(site_url('servico/ver/'.$s->id_produto.'/'.$s->slug), $s->desc_produto).br();
}
echo '</div>';

// Contato
echo '<div class="col33">';
echo heading($this->menu_m->get_field(NULL, 'contato', 'slug'), 2);
echo anchor(site_url('contato'), 'Envie um email').br();
echo 'Telefone: '.$this->empresa->telefone.br();
echo anchor(site_url('contato'), 'Localização').br();
echo '</div>';
?>
