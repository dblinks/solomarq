<div id="box-produto" class="margem">
  <h1 class="fonte-google"><?php
  $arr_per[1] = $this->menu_m->get_field('desc_menu', 'produto', 'slug');
  $arr_per[2] = $this->menu_m->get_field('desc_menu', 'servico', 'slug');
  echo @$arr_per[$this->site->exibir_inicial];
  ?>
  </h1>
  <h2 class="fonte-google">&nbsp;</h2>
  <div class="sublinhado-maior-ma">&nbsp;</div>
  <div class="sublinhado-menor-ma">&nbsp;</div>
  <div id="produtos" class="inicial">
    <?php
    $quebra = 0;
    if(!is_null($produtos))
    {
      foreach($produtos->result() as $produto)
      {
        $img = (!isset($produto->img_me)) ? 'naodisponivel.jpg' : $produto->img_me;
        $quebra ++;
        echo (($quebra % 4) == 0)
          ? '<div class="produto last">'
          : '<div class="produto">';

        $tipo = $this->config->item('pro_ser');
        echo anchor(slug($tipo[$produto->tipo]).'/ver/'.$produto->id_produto.'/'.$produto->slug, img('image/get_image_c/'.$img.':140:100'));
        echo anchor(slug($tipo[$produto->tipo]).'/ver/'.$produto->id_produto.'/'.$produto->slug, heading($produto->desc_produto, 3));
        echo '<div class="detalhe">'.anchor(slug($tipo[$produto->tipo]).'/ver/'.$produto->id_produto.'/'.$produto->slug, '+ detalhes').'</div>';
        echo '</div>';
      }
    }
    ?>
  </div>
  <!-- #produtos -->
</div>
<!-- #box-produtos -->