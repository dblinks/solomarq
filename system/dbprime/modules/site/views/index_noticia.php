<div id="box-noticia" class="margem-direita">
  <h1 class="fonte-google">Notícias</h1>
  <div class="sublinhado-maior-me">&nbsp;</div>
  <div class="sublinhado-menor-me">&nbsp;</div>
  <div id="noticias" class="inicial">
    <?php
    foreach($noticias->result() as $noticia)
    {
    ?>
    <div class="noticia">
      <div class="data"><?php echo substr(bd2br($noticia->data), 0, 5); ?></div>
      <h4><?php echo anchor('noticia/ver/'.$noticia->id_noticia.'/'.$noticia->slug, $noticia->desc_noticia); ?></h4>
    </div>
    <?php
    }
    ?>
    <div class="vejamais"><?php echo anchor(site_url('noticia'), 'Veja mais'); ?></a></div>
  </div>
  <!-- #noticias -->
</div>
<!-- #box-noticias -->