<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends Site_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
    $this->load->model('noticia/noticia_m');
    $this->load->model('produto/produto_m');

    // CSS
    $this->template->add_css($this->tema_url('css/inicial.css'));

    // Produtos/Serviços inicial
    $tipo = ($this->site->exibir_inicial == 0) ? NULL : $this->site->exibir_inicial;
    $selecao = array('produto.status' => 1, 'produto.destaque' => 1, 'produto.tipo' => $tipo);

    $produtos = $this->midia_m->get_midia_join('produto', NULL, 8, NULL, TRUE, $selecao);
    $this->template->write_view('conteudo', 'index_produto', array('produtos' => $produtos));

    // Notícias inicial
    $noticias = $this->noticia_m->get_all(5, NULL, array('status' => 1), NULL, 'data DESC, data_cri DESC, desc_noticia ASC');
    $this->template->write_view('conteudo', 'index_noticia', array('noticias' => $noticias));

		$this->template->render();
	}

  public function mapa()
  {
    $this->template->set_template('basic');

    $this->load->model('pagina/pagina_m');

    // JS - CSS
    $this->template->add_js('js/site/mapa_site.js');
    $this->template->add_css($this->tema_url('css/mapa.css'));

    $this->template->write_view('conteudo', 'mapa');

		$this->template->render();
  }

  public function manutencao()
  {
    if($this->site->online)
      redirect(site_url());

    $this->template->set_template('basic');

    // CSS
    $this->template->add_css('css/manutencao.css');

    $this->template->add_css('#logotipo { background: url('.site_url($this->config->item('dir_midia').$this->empresa->logotipo).') fixed no-repeat 50% 25%; }', 'embed');

    $this->template->write_view('conteudo', 'erro', array('erro' => $this->site->mensagem_offline));

		$this->template->render();
  }

  public function erro404()
  {
    $this->template->set_template('basic');

    // CSS
    $this->template->add_css('css/manutencao.css');

    $this->template->add_css('#logotipo { background: url('.site_url($this->config->item('dir_midia').$this->empresa->logotipo).') fixed no-repeat 50% 25%; }', 'embed');

    $menu = $this->menu_m->get_all(NULL, NULL, array('status' => 1));

    $this->template->write_view('conteudo', 'erro', array('erro' => 'Erro 404 - A página que você tentou acessar não existe.', 'menu' => $menu));

		$this->template->render();
  }
}

/* End of file site.php */
/* Location: ./application/controllers/site.php */