<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Representante extends Site_Controller {

	function __construct()
	{
		parent::__construct();
    $this->load->model('municipio/municipio_m');
    $this->load->model('estado/estado_m');
    $this->load->model('representante/representante_m');

    $this->js_css();
	}

  function js_css(){
    //CSS
    $this->template->add_css($this->tema_url('css/representante.css'));

    //JS
    $this->template->add_js('js/raphael-min.js');
    $this->template->add_js('js/site/mapa.js');
  }

	public function index()
	{
    // Título Local
    $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');
    $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($p->desc_menu), 'pagina' => $p->desc_menu));

    // Título
    $this->_titulo[] = $p->desc_menu;
    $this->template->write_view('titulo', 'comum/title');

    $this->template->write_view('conteudo', 'mapa');
    $this->template->write_view('conteudo', 'index');

		$this->template->render();
	}

	public function ver()
	{
    // Título Local
    $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');

    $estado = $this->estado_m->get_id($this->uri->segment(3), 'uf');
    $representantes = $this->representante_m->get_all(NULL, NULL, array('uf' => $this->uri->segment(3)), NULL, 'parceiro DESC');

    $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($p->desc_menu, $estado->desc_estado), 'pagina' => $p->desc_menu));

    // Todos estados
    $links = $this->estado_m->get_all();

    // Título
    $this->_titulo[] = $p->desc_menu;
    $this->_titulo[] = $estado->desc_estado;
    $this->template->write_view('titulo', 'comum/title');

    $this->template->write_view('conteudo', 'mapa', array('links' => $links, 'estado' => $estado));
    $this->template->write_view('conteudo', 'index', array('representantes' => $representantes, 'estado' => $estado));

		$this->template->render();
	}

}

/* End of file site.php */
/* Location: ./application/controllers/site.php */