<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Representante_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Parceiro
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Representante_m extends DBModel {

	var $_tabela = 'representante';
	var $_id = 'id_representante';
	var $_desc = 'desc_representante';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_representante ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		$dir = $this->config->item('dir_midia');//.$this->_tabela.'/';
		//if(!is_dir($dir)) { @mkdir($dir); @chmod($dir, 0777);  }

		$upload['upload_path'] = $dir;
		$upload['allowed_types'] = 'jpg|jpeg|png|gif';
		$upload['overwrite'] = TRUE;
		$upload['remove_spaces'] = TRUE;
		$this->upload->initialize($upload);

		if(!$this->upload->do_upload('logotipo'))
		{
			$this->session->set_flashdata('msgaux', msg($this->upload->display_errors(), 'erro'));
			$logotipo = NULL;
		}
		else
		{
			$up = $this->upload->data();
			chmod($up['full_path'], 0777);
			$logotipo = '_'.$this->modulo->url.'_'.date('Ymd_His').'_'.time().$up['file_ext'];

			@rename($up['file_name'], $logotipo);
		}

		return parent::insert(array(
			'id_municipio' => $dado['municipio'],
			$this->_desc	=> $dado['representante'],
			'logotipo'	=> $logotipo,
			'slug'	=> slug($dado['representante']),
			'endereco' => $dado['endereco'],
			'numero' => $dado['numero'],
			'logradouro' => $dado['logradouro'],
			'bairro' => $dado['bairro'],
			'cep' => $dado['cep'],
			'telefone' => $dado['telefone'],
			'celular' => $dado['celular'],
			'email' => $dado['email'],
			'site' => prep_url($dado['site']),
			'uf' => $dado['uf'],
			'parceiro' => $dado['parceiro']
		));
	}

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		$dir = $this->config->item('dir_midia');//.$this->_tabela.'/';
		//if(!is_dir($dir)) { @mkdir($dir); @chmod($dir, 0777); }

		$upload['upload_path'] = $dir;
		$upload['allowed_types'] = 'jpg|jpeg|png|gif';
		$upload['overwrite'] = TRUE;
		$upload['remove_spaces'] = TRUE;
		$this->upload->initialize($upload);

		if(!$this->upload->do_upload('logotipo'))
		{
			//$this->session->set_flashdata('msgaux', msg($this->upload->display_errors(), 'erro'));
			$logotipo = $this->representante_m->get_field('logotipo', $dado['id']);
		}
		else
		{
			$up = $this->upload->data();
			chmod($up['full_path'], 0755);
			$logotipo = '_'.$this->modulo->url.'_'.date('Ymd_His').'_'.time().$up['file_ext'];

			$logotipo_atual = $this->representante_m->get_field('logotipo', $dado['id']);
			@unlink($logotipo_atual);

			rename($dir.$up['file_name'], $dir.$logotipo);
		}

		return parent::update(array(
			'id_municipio' => $dado['municipio'],
			$this->_desc	=> $dado['representante'],
			'logotipo'	=> $logotipo,
			'slug'	=> slug($dado['representante']),
			'endereco' => $dado['endereco'],
			'numero' => $dado['numero'],
			'logradouro' => $dado['logradouro'],
			'bairro' => $dado['bairro'],
			'cep' => $dado['cep'],
			'telefone' => $dado['telefone'],
			'celular' => $dado['celular'],
			'email' => $dado['email'],
			'site' => prep_url($dado['site']),
			'uf' => $dado['uf'],
			'parceiro' => $dado['parceiro']
		), $dado['id']);
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		$logotipo_atual = $this->representante_m->get_field('logotipo', $id);
		@unlink($logotipo_atual);
		return parent::delete($id);
	}
}

/* End of file Representante_m,.php */
/* Location: ./app/modules/representante/models/representante.php */
