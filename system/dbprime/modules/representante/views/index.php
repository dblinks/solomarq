<div id="direita" class="margem-direita">
  <div id="representantes">
  <?php
  if(!isset($estado))
  {
    echo 'Selecione um estado para exibir os representantes.';
  }
  else
  {
    echo heading(img('img/icons_estado/'.strtolower($estado->uf).'.png').nbs().$estado->desc_estado.' ('.$estado->uf.')', 1, 'class="fonte-google destaque"');
    if($representantes->num_rows() == 0)
    {
      echo img('img/icons/exclamation.png').' Não foram encontrados representantes no estado selecioando.<br />Favor entre em contato com a matriz no endereço abaixo:'.br(2);
      echo heading($this->empresa->desc_empresa, 2, array('class' => 'fonte-google destaque'));
      echo $this->empresa->slogan.br();
      echo $this->empresa->endereco.', '.$this->empresa->numero.' | '.$this->empresa->bairro.br();
      echo (strlen($this->empresa->complemento) > 0) ? $this->empresa->complemento.br() : NULL;
      echo $this->empresa->municipio.' - '.$this->empresa->uf.br();
      echo 'CEP: '.$this->empresa->cep.br();
      echo 'email: '.$this->empresa->email.br();
      echo 'telefone: '.$this->empresa->telefone;
    }
    else
    {
      $m = $this->municipio_m->get_array();
      $e = $this->estado_m->get_array();

      foreach($representantes->result() as $r)
      {
        echo '<div class="representante">';

        echo '<div class="dado">';
        $rep = ($r->parceiro) ? $r->desc_representante.img('img/icons/star-small.png') : $r->desc_representante;
        echo heading($rep, 2, array('class' => 'fonte-google destaque'));
        echo $r->endereco.', '.$r->numero.' | '.$r->bairro.br();
        echo $m[$r->id_municipio].' | '.$r->uf.' | '.$r->cep.br();
        echo 'telefone: '.$r->telefone.' | celular: '.$r->celular.br();
        echo 'email: '.$r->email.' | site: '.anchor($r->site, $r->site, 'target="_blank"').br();
        echo '</div>';

        echo '<div class="logotipo">';
        echo (strlen(trim($r->logotipo)) > 0) ? img(array('src' => $this->config->item('dir_midia').$r->logotipo, 'align' => 'right')) : nbs();
        echo '</div>';

        echo '<hr class="clear-both">';
        echo '</div>';
      }
      echo '<div class="parceiros">'.img('img/icons/star-small.png').' Parceiros</div>';
    }
  }
  ?>
  </div>
</div>