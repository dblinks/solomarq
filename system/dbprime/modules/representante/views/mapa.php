<div id="esquerda" class="margem-esquerda">
  <div id="mapa_brasil"></div>
  <div id="uf">
  <?php
    echo isset($estado) ? $estado->desc_estado.' - '.$estado->uf : 'UF - Estado';
  ?>
  </div>
  <div id="links">
    <?php
    if(isset($links))
    {
      foreach($links->result() as $l)
        echo anchor(site_url($this->uri->slash_segment(1).'ver/'.$l->uf), $l->desc_estado.nbs().'('.$l->uf.')');
    }
    ?>
  </div>
</div>