<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open_multipart(current_url(), array('class' => 'form validar'), array('id' => @$id_representante));

echo li();
echo form_label('Nome: ', 'representante');
echo form_input(array('name' => 'representante', 'id' => 'representante', 'value' => set_value('parceiro', @$desc_representante), 'title' => 'Digite o nome do representante.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o nome do representante\'}}}', 'maxlength' => '200', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Logotipo: ', 'logotipo');
echo form_upload(array('name' => 'logotipo', 'id' => 'logotipo', 'class' => 'dica', 'title' => 'Selecione o arquivo de logotipo (PNG, JPG, GIF).'));
echo (isset($logotipo) > 0)
	? br().img($this->config->item('dir_midia').$logotipo) : NULL;
echo msg('Envie o logotipo do representante em versão horizontal, com largura máxima de 200px e fundo transparente (PNG).', 'corner nota ajuda', FALSE, FALSE, FALSE);
echo li_close();

echo li();
echo form_label('Endereço: ', 'endereco');
echo form_input(array('name' => 'endereco', 'id' => 'endereco', 'value' => set_value('endereco', @$endereco), 'title' => 'Digite o endereço.', 'class' => 'dica', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Número: ', 'numero');
echo form_input(array('name' => 'numero', 'id' => 'numero', 'value' => set_value('numero', @$numero), 'title' => 'Digite o número.', 'class' => 'dica numero', 'maxlength' => '10', 'size' => '20')).br();
echo li_close();

echo li();
echo form_label('Logradouro: ', 'logradouro');
echo form_input(array('name' => 'logradouro', 'id' => 'logradouro', 'value' => set_value('logradouro', @$logradouro), 'title' => 'Digite o logradouro.', 'class' => 'dica', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Bairro: ', 'bairro');
echo form_input(array('name' => 'bairro', 'id' => 'bairro', 'value' => set_value('bairro', @$bairro), 'title' => 'Digite o bairro.', 'class' => 'dica', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('CEP: ', 'cep');
echo form_input(array('name' => 'cep', 'id' => 'cep', 'value' => set_value('cep', @$cep), 'title' => 'Digite o CEP.', 'class' => 'dica cep', 'maxlength' => '9', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Telefone: ', 'telefone');
echo form_input(array('name' => 'telefone', 'id' => 'telefone', 'value' => set_value('telefone', @$telefone),'title' => 'Digite o telefone.', 'class' => 'dica telefone', 'maxlength' => '10', 'size' => '25')).br();
echo li_close();

echo li();
if($this->_acao == 'alterar')
	$m = $this->municipio_m->get_id($id_municipio);
echo form_label('Estado: ', 'estado');
echo form_dropdown('estado', $this->estado_m->get_array(), @$m->id_estado, 'class="dica carrega {validar:{required:true, messages:{required:\'Selecione o estado\'}}}" destino="municipio" modulo="municipio" acao="option" title="Selecione o estado."');
echo li_close();

echo li();
if($this->_acao == 'alterar')
	$arr_mun = $this->municipio_m->get_array(array('id_estado' => $m->id_estado));
else
	$arr_mun = array();
echo form_label('Município: ', 'municipio');
echo form_dropdown('municipio', $arr_mun, @$id_municipio, 'id="municipio" class="dica {validar:{required:true, messages:{required:\'Selecione o município. (Selecione primeiro o estado)\'}}}" title="Selecione o município. (Selecione primeiro o estado)"');
echo li_close();

echo li();
echo form_label('Celular: ', 'celular');
echo form_input(array('name' => 'celular', 'id' => 'celular', 'value' => set_value('celular', @$celular), 'title' => 'Digite o celular, se houver.', 'class' => 'dica telefone', 'maxlength' => '10', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Email: ', 'email');
echo form_input(array('name' => 'email', 'id' => 'email', 'value' => set_value('email', @$email), 'title' => 'Digite o email.', 'class' => 'dica {validar:{email:true, messages:{email:\'Digite um endereço de email válido\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Site: ', 'site');
echo form_input(array('name' => 'site', 'id' => 'site', 'value' => set_value('site', @$site), 'title' => 'Digite o site.', 'class' => 'dica {validar:{url:true, messages:{url:\'Digite uma url válida (com http://)\'}}}', 'maxlength' => '255', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Parceiro: ', 'parceiro');
echo form_dropdown('parceiro', $this->config->item('simnao'), (bool)@$parceiro, 'class="dica" title="Este representante é um parceiro?"');
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>