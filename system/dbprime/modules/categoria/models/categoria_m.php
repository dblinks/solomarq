<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categoria_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Categoria
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Categoria_m extends DBModel {

	var $_tabela = 'categoria';
	var $_id = 'id_categoria';
	var $_desc = 'desc_categoria';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_categoria ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Salva registro no banco de dados
	 * com validação de uf
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			$this->_desc	=> $dado['categoria'],
      'tipo'	=> $dado['tipo'],
			'slug'	=> slug($dado['categoria'])
		));
	}

	/**
	 * Altera registro no banco de dados
	 * com validação de iso e iso3
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::update(array(
			$this->_desc	=> $dado['categoria'],
      'tipo'	=> $dado['tipo'],
			'slug'	=> slug($dado['categoria'])
		), $dado['id']);
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}
}

/* End of file Categoria_m.php */
/* Location: ./app/modules/categoria/models/Categoria_m.php */
