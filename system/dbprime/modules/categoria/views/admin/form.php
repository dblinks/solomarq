<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_categoria));

echo li();
echo form_label('Categoria: ', 'categoria');
echo form_input(array('name' => 'categoria', 'id' => 'categoria', 'value' => set_value('categoria', @$desc_categoria), 'title' => 'Digite a categoria.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite a categoria\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

switch($this->site->produto_servico)
{
  case 0:
    $pro_ser = $this->config->item('pro_ser');
    unset($pro_ser[0]);
    echo li();
    echo form_label('Tipo: ', 'tipo');
    echo form_dropdown('tipo', $pro_ser, @$tipo, 'title="Selecione o tipo de Categoria." class="dica {validar:{required:true, messages:{required:\'Selecione o tipo\'}}}"');
    echo li_close();
  break;
  default:
    echo form_hidden('tipo', $this->site->produto_servico);
}

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>