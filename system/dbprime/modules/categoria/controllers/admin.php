<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Categoria - Controller
 *
 * @package 		Admin
 * @subpackage 	Categoria
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{

	private $validation_rules = array(
		array(
			'field'   => 'categoria',
			'label'   => 'Categoria',
			'rules'   => 'required|trim'
		)
	);

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('categoria/categoria_m');
    $this->load->model('sitemap_m');
	}

	/**
	 * Lista os registros
	 *
	 * @access	public
	 */
	public function index()
	{
		$this->template->write('conteudo', heading('Listando Categorias', 1));
		$this->template->write_view('conteudo', 'admin/listar');

		$this->template->render();
	}

	/**
	 * Adiciona registro com validação do formulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function adicionar()
	{
		$this->template->write('conteudo', heading('Adicionar '.$this->modulo->alias, 1));
		$this->template->write_view('conteudo', 'admin/form');

		// Dados recebidos
		if ($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				if($this->categoria_m->insert($this->input->post()))
				{
					$this->session->set_flashdata('msg', msg('Sucesso ao adicionar '.$this->modulo->alias, 'sucesso'));
          $this->sitemap_m->gerar();
					redirect(admin_url($this->modulo->url));
				}
				else
				{
					$this->template->write('msg', msg('Erro ao adicionar '.$this->modulo->alias, 'erro'));
				}
			}
			else
			{
				$this->template->write('msg', msg(validation_errors(), 'erro'));
			}
		}

		$this->template->render();
	}

	/**
	 * Altera registro com validação de fomulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function alterar()
	{
		$a = $this->categoria_m->get_id($this->uri->segment(4));
		$this->template->write('conteudo', heading('Alterar '.$this->modulo->alias.' : '.$a->{$this->categoria_m->_desc}));
		$this->template->write_view('conteudo', 'admin/form', $a);

		// Dados recebidos
		if ($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				if($this->categoria_m->update($this->input->post()))
				{
					$this->session->set_flashdata('msg', msg('Sucesso ao alterar '.$this->modulo->alias, 'sucesso'));
          $this->sitemap_m->gerar();
					redirect(admin_url($this->modulo->url));
				}
				else
				{
					$this->template->write('msg', msg('Erro ao alterar '.$this->modulo->alias, 'erro'));
				}
			}
			else
			{
				$this->template->write('msg', msg(validation_errors(), 'erro'));
			}
		}

		$this->template->render();
	}

	/**
	 * Exclui registro
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function excluir()
	{
		if($this->uri->segment(4) == 1)
		{
			$this->session->set_flashdata('msg', msg($this->modulo->alias.' não pode ser excluída. Deve haver ao menos 1 categoria cadastrada.', 'erro'));
			redirect(admin_url($this->modulo->url));
		}

		if($this->categoria_m->check_foreign('produto', $this->uri->segment(4)))
		{
			$this->session->set_flashdata('msg', msg('Categoria não pode ser excluída!'.br().'Exclua os Produtos desta categoria antes.', 'erro'));
		}
		else
		{
			if($this->categoria_m->delete($this->uri->segment(4)))
      {
        $this->session->set_flashdata('msg', msg('Categoria excluída com sucesso!', 'sucesso'));
        $this->sitemap_m->gerar();
      }
      else
				$this->session->set_flashdata('msg', msg('Erro ao excluir Categoria!', 'erro'));
		}
		redirect(admin_url($this->modulo->url));
	}

	/**
	 * Codifica registros em Json
	 *
	 * @access	public
	 * @return	string
	 */
	function json_listar()
	{
		######################## ALTERAR
		//Colunas de retorno
		$colunas = array($this->categoria_m->_id, $this->categoria_m->_desc, 'tipo', 'opcoes');
    $tipo = $this->config->item('pro_ser');

		//Campo de ID
		$id = $colunas[0];

		// Paginação
		if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1')
		{
			$n = $_POST['iDisplayLength'];
			$offset = $_POST['iDisplayStart'];
		}
		else
		{
			if (isset($_POST['iDisplayLength']))
			{
				$n = ($_POST['iDisplayLength'] == '-1') ? NULL : $_POST['iDisplayLength'];
				$offset = 0;
			}
			else
			{
				$n = 25;
				$offset = 0;
			}
		}

		// Ordenação
		if(isset($_POST['iSortCol_0']))
		{
			$order = NULL;
			for($i=0;$i<intval($_POST['iSortingCols']);$i++)
			{
				if($_POST['bSortable_'.intval($_POST['iSortCol_'.$i])] == "true")
				{
					if(trim($colunas[intval($_POST['iSortCol_'.$i])]) == 'opcoes') { $order = NULL; break; }
					$order .= $colunas[intval($_POST['iSortCol_'.$i])]." ".$_POST['sSortDir_'.$i].", ";
				}
			}
			$order = rtrim($order, ' ,');
			if((trim($order) == 'ORDER BY') || (strlen($order) == 0)) {	$order = NULL; }
		}
		else
			$order = NULL;

		//Filtro
		$orlike = NULL;
		if(strlen($_POST['sSearch']) > 0)
		{
			$campo = array();
			$valor = array();
			for($i = 0; $i < (count($colunas)-1); $i++)
			{
				$campo[] = $colunas[$i];
				$valor[] = trim($_POST['sSearch']);
			}
			$orlike = array_combine($campo, $valor);
		}

		######################## ALTERAR
		//SQL Query
		$query = $this->categoria_m->get_all($n, $offset, NULL, NULL, $order, $orlike);

		// Resultado do filtro
		$query_filtro = $this->categoria_m->get_all(NULL, NULL, NULL, NULL, $order, $orlike);
		$filtro = $query_filtro->num_rows();

		// Total
		$query_total = $this->categoria_m->get_all();
		$total = $query_total->num_rows();

		//Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $total,
			"iTotalDisplayRecords" => $filtro,
			"aaData" => array()
		);

		######################## ALTERAR
		foreach($query->result() as $row)
		{
			$retorno = array();
			for($i=0;$i<count($colunas);$i++)
			{
				switch($colunas[$i])
				{
					case 'opcoes': // Ações
						$links = array();
						$links[] = anchor(admin_url($this->modulo->url.'/alterar/'.$row->{$this->categoria_m->_id}), 'alterar', array('class' => 'alterar'));
						($row->id_categoria != 1) ? $links[] = anchor(admin_url($this->modulo->url.'/excluir/'.$row->{$this->categoria_m->_id}), 'excluir', array('desc' => $row->{$this->categoria_m->_desc}, 'class' => 'excluir', 'onclick' => 'excluir();')) : NULL;
						$retorno[] = implode(' | ', $links);
					break;
          case 'tipo';
            $retorno[] = $tipo[(int)$row->tipo];
          break;
					default: // Outras colunas
						$retorno[] = $row->{$colunas[$i]};
				}
			}
			$output['aaData'][] = $retorno;
		}
		echo json_encode($output);
	}

  /**
   * Codifica registros em options de select
   *
   * @access	public
   * @return	string
   */
	public function ajax_option_tipo()
	{
		$return = '<option value=""></option>'."\n";
		$array = $this->categoria_m->get_array(array('tipo' => $this->uri->segment($this->uri->total_segments())));
		foreach($array as $k => $v)
			$return .= '<option value="'.$k.'">'.$v.'</option>'."\n";
		echo $return;
	}
}

/* End of file categoria.php */
/* Location: ./app/modules/categoria/controllers/categoria.php */
