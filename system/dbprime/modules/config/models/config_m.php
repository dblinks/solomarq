<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Config_m - Model
 *
 * @package 		DBModel
 * @subpackage 	Config
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */
class Config_m extends DBModel {

  var $_tabela = 'config';
  var $_id = 'id_config';
  var $_desc = 'desc_config';
  var $_cri = 'data_cri';
  var $_alt = 'data_alt';
  var $_order = 'desc_config ASC';

  /**
   * Inicializa a classe
   *
   * @access	public
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Altera registro no banco de dados
   *
   * @access	public
   * @return	boolean
   */
  function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL) {
    $upload['upload_path'] = $this->config->item('dir_midia');
    $upload['allowed_types'] = 'jpg|jpeg|png|gif';
    $upload['file_name'] = 'fundo';
    $upload['overwrite'] = TRUE;
    $upload['remove_spaces'] = TRUE;
    $this->upload->initialize($upload);

    // Upload Background
    if ($this->upload->do_upload('img_fundo')) {
      $up = $this->upload->data();
      chmod($up['full_path'], 0755);
      $fundo_atual = $this->config_m->get_field('img_fundo', 1);
      ($fundo_atual != $upload['file_name'] . $up['file_ext']) ? @unlink($this->config->item('dir_midia') . $fundo_atual) : NULL;
      $fundo = $upload['file_name'] . $up['file_ext'];
    }
    else
      $fundo = $this->config_m->get_field('img_fundo', 1);

    // Exc Background
    if (isset($dado['exc_background']) && $dado['exc_background']) {
      $fundo = $this->config_m->get_field('img_fundo', 1);
      $fundo = (unlink($this->config->item('dir_midia') . $fundo)) ? NULL : $fundo;
    }

    // Produto e Serviço
    if ($dado['produto_servico'] == 0) {
      $this->db->update('menu', array('status' => 1), array('slug' => 'produto'));
      $this->db->update('menu', array('status' => 1), array('slug' => 'servico'));
    } else {
      // Produto
      if ($dado['produto_servico'] == 1) {
        $dado['categoria_menu'] = 1;
        $dado['exibir_inicial'] = 1;
        $this->db->update('menu', array('status' => 1), array('slug' => 'produto'));
      }
      else
        $this->db->update('menu', array('status' => 0), array('slug' => 'produto'));

      // Serviço
      if ($dado['produto_servico'] == 2) {
        $dado['exibir_inicial'] = 2;
        $dado['categoria_menu'] = 2;
        $this->db->update('menu', array('status' => 1), array('slug' => 'servico'));
      }
      else
        $this->db->update('menu', array('status' => 0), array('slug' => 'servico'));
    }

    // Notícia Automática
    if ($dado['noticia_auto'] == 0) {
      $this->db->update('modulo', array('menu' => 1, 'ativo' => 1), array('url' => 'noticia'));
      $this->db->update('menu', array('status' => 1), array('slug' => 'noticia'));
    } else {
      $this->db->update('modulo', array('menu' => 0, 'ativo' => 0), array('url' => 'noticia'));
      $this->db->update('menu', array('status' => 0), array('slug' => 'noticia'));
    }

    $update = array(
        $this->_desc => $this->empresa->desc_empresa,
        'online' => $dado['online'],
        'mensagem_offline' => nl2br($dado['mensagem_offline']),
        'noticia_auto' => @$dado['noticia_auto'],
        'noticia_link' => @$dado['noticia_link'],
        'produto_servico' => @$dado['produto_servico'],
        'produto_servico' => $dado['produto_servico'],
        'produto_valor' => $dado['produto_valor'],
        'categoria_menu' => $dado['categoria_menu'],
        'exibir_inicial' => $dado['exibir_inicial'],
        'admin_feed' => @$dado['admin_feed']
    );

    if ($this->usuario->nivel == 1) {
      // Google Maps
      $update['gm_latitude'] = @$dado['gm_latitude'];
      $update['gm_longitude'] = @$dado['gm_longitude'];
      $update['gm_tipo'] = @$dado['gm_tipo'];

      // Google Analytics
      if (@$dado['alterar_ga'] == 1) {
        $update['ga_analytics'] = @$dado['ga_analytics'];
        $update['ga_perfil'] = @$dado['ga_perfil'];
        $update['ga_email'] = @$dado['ga_email'];
        $update['ga_senha'] = @$dado['ga_senha'];
      }

      // Twitter
      $update['tw_usuario'] = @$dado['tw_usuario'];

      // Email
      $update['email_protocolo'] = @$dado['email_protocolo'];
      $update['email_servidor'] = @$dado['email_servidor'];
      $update['email_smtp'] = @$dado['email_smtp'];
      $update['email_senha'] = @$dado['email_senha'];
      $update['email_usuario'] = @$dado['email_usuario'];
      $update['email_porta'] = @$dado['email_porta'];
      $update['email_sendmail'] = @$dado['email_sendmail'];

      // Imagens
      if ($this->usuario->nivel == 1) {
        $update['img_th'] = isset($dado['img_th']) ? $dado['img_th'] : 200;
        $update['img_me'] = isset($dado['img_me']) ? $dado['img_me'] : 500;
        $update['img_gd'] = isset($dado['img_gd']) ? $dado['img_gd'] : 800;
      }
      // Background
      $update['img_fundo'] = @$fundo;
    }

    return parent::update($update, $dado['id']);
  }

}

/* End of file Config_m.php */
/* Location: ./app/modules/config/models/Config_m.php */
