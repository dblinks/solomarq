<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Config - Controller
 *
 * @package 		Admin
 * @subpackage 	Config
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{

	private $validation_rules = array(
		array(
			'field'   => 'online',
			'label'   => 'Online',
			'rules'   => 'trim'
		)
	);

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('config/config_m');
	}

	/**
	 * Página principal - index
	 *
	 * @access	public
	 */
	public function index()
	{
    $this->template->add_js('js/admin/admin_config.js');

		$a = $this->config_m->get_id(1);
		$this->template->write('conteudo', heading('Configurações do Site : '.$this->empresa->desc_empresa));
		$this->template->write_view('conteudo', 'admin/form', $a);

		// Dados recebidos
		if($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
        $_POST['ga_senha'] = base64_encode($_POST['ga_senha']);
				$this->config_m->update($this->input->post())
					? $this->session->set_flashdata('msg', msg('Configuração alterada com sucesso', 'sucesso'))
					: $this->session->set_flashdata('msg', msg('Erro ao alterar Configuração', 'erro'));
					redirect($this->agent->referrer());
			}
			else
			{
				$this->session->set_flashdata('msg', msg(validation_errors(), 'erro'));
				redirect($this->agent->referrer());
			}

			redirect(admin_url($this->modulo->url));
		}

		$this->template->render();
	}

  public function ajax_autentica_analytics()
  {
    if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') === FALSE)
      show_error('Você não deveria estar aqui');

    error_reporting(0);

    $this->load->library('CiGAnalytics');
    $ga = new CiGAnalytics();
    $ga->login($this->input->post('email'), $this->input->post('senha'));
    $contas = $ga->getWebsiteProfiles();

    foreach($contas as $conta)
      $return[] = array($conta['profileId'], $conta['accountName'], $conta['webProfileId']);

    echo json_encode($return);
  }
}

/* End of file config.php */
/* Location: ./app/modules/config/controllers/config.php */