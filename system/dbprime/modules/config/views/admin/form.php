<?php

echo form_fieldset('');

echo form_open_multipart(current_url(), array('class' => 'form validar'), array('id' => @$id_config));

echo '<div class="box-tab">';
echo ul();

echo li() . '<a href="#tab_1">Site</a>' . li_close();
echo ($this->usuario->nivel == 1) ? li() . '<a href="#tab_2">Google</a>' . li_close() : NULL;
echo ($this->usuario->nivel == 1) ? li() . '<a href="#tab_3">Email</a>' . li_close() : NULL;
echo ($this->usuario->nivel == 1) ? li() . '<a href="#tab_4">Imagem</a>' . li_close() : NULL;
echo ($this->usuario->nivel == 1) ? li() . '<a href="#tab_5">Twitter</a>' . li_close() : NULL;
echo li() . '<a href="#tab_6">Configurações</a>' . li_close();
echo li() . '<a href="#tab_7">Administração</a>' . li_close();

echo ul_close();

// Tab 01 - Site
echo '<div id="tab_1">';
echo ul(array('class' => 'zebra'));

echo li(array('class' => 'titulo'));
echo heading('Site On-line', 1, array('class' => 'titulo'));
echo li_close();

echo li();
echo form_label('On-line: ', 'online');
echo form_dropdown('online', $this->config->item('simnao'), @$online, 'class="dica" title="Este site está online?"');
echo li_close();

echo li();
echo form_label('Mensagem off-line: ', 'mensagem_offline');
echo form_textarea(array('name' => 'mensagem_offline', 'id' => 'mensagem_offline', 'value' => set_value('mensagem_offline', @str_replace(br(), '', $mensagem_offline)), 'title' => 'Digite a mensagem a ser exibida quando o site estiver em manutenção.', 'class' => 'dica {validar:{required:true, messages:{required:\'Mensagem para quando o site é colocado como off-line/manutenção.\'}}}', 'cols' => '77', 'rows' => '3')) . br();
echo li_close();

if ($this->usuario->nivel == 1) { // Somente SuperUsuário
  echo li(array('class' => 'titulo'));
  echo heading('Background', 1, array('class' => 'titulo'));
  echo li_close();

  echo li();
  echo form_label('Imagem de fundo: ', 'img_fundo');
  echo form_upload(array('name' => 'img_fundo', 'id' => 'img_fundo', 'class' => 'dica', 'title' => 'Selecione o arquivo d eimagem de fundo para o site.'));
  echo ($this->site->img_fundo) ? br() . img(array('src' => $this->config->item('dir_midia') . $this->site->img_fundo, 'width' => '600')) : msg('Envie uma imagem com o tamanho 1920x405px (LxA).', 'corner nota ajuda', FALSE, FALSE, FALSE);
  echo li_close();

  if ($this->site->img_fundo) {
    echo li();
    echo form_label('Excluir Background: ', 'exc_background');
    echo form_checkbox(array('name' => 'exc_background', 'id' => 'exc_background', 'value' => '1', 'checked' => FALSE, 'title' => 'Se deseja excluir a imagem de fundo, marque este campo.', 'class' => 'dica'));
    echo li_close();
  }
}

echo ul_close();
echo '</div>';

if ($this->usuario->nivel == 1) { // Somente SuperUsuário
  // Tab 02 - Google
  echo '<div id="tab_2">';

  echo ul(array('class' => 'zebra'));

  echo form_fieldset('Google Maps');

  $passos = 'Para descobrir a latitude e a longitude do seu local, siga os seguintes passos: ' . br();
  $passos .= '1 - Ache no ' . anchor('http://maps.google.com.br/', 'Google Maps', 'target="_blank"') . ' o ponto desejado.' . br();
  $passos .= '2 - Clique como botão direito e escolha a opção "O que há aqui".' . br();
  $passos .= '3 - No campo de busca aparecerá antes da vírgula a latitude e após a vírgula a longitude. Ex.: -26.726753,-53.515949' . br();

  echo msg($passos, 'ajuda corner');
  echo li();
  echo form_label('Latitude: ', 'gm_latitude');
  echo form_input(array('name' => 'gm_latitude', 'id' => 'gm_latitude', 'value' => set_value('gm_latitude', @$gm_latitude), 'title' => 'Digite a latitude.', 'class' => 'dica', 'maxlength' => '30', 'size' => '25')) . br();
  echo li_close();

  echo li();
  echo form_label('Longitude: ', 'gm_longitude');
  echo form_input(array('name' => 'gm_longitude', 'id' => 'gm_longitude', 'value' => set_value('gm_longitude', @$gm_longitude), 'title' => 'Digite a longitude.', 'class' => 'dica', 'maxlength' => '30', 'size' => '25')) . br();
  echo li_close();

  echo li();
  echo form_label('Tipo de Mapa: ', 'gm_tipo');
  echo form_dropdown('gm_tipo', $this->config->item('gm_tipo'), @$gm_tipo, 'class="dica" title="Escolha o tipo de mapa a ser exibido na página de contato."');
  echo li_close();

  echo form_fieldset_close();

  echo form_fieldset('Google Analytics');

  echo li();
  echo form_label('Alterar Google Analytics: ', 'alterar_ga');
  echo form_checkbox(array('name' => 'alterar_ga', 'id' => 'alterar_ga', 'value' => '1', 'checked' => FALSE, 'class' => 'dica', 'title' => 'Deseja alterar os dados do Google Analytics?'));
  echo li_close();

  echo '<div id="box_alterar_ga" style="display:none;">';

  echo li();
  echo form_label('E-mail do Google Analytics: ', 'ga_email');
  echo form_input(array('name' => 'ga_email', 'id' => 'ga_email', 'value' => set_value('ga_email', @$ga_email), 'title' => 'E-mail utilizado para o Google Analytics, é necessário para mostrar o gráfico na Inicial.', 'class' => 'dica', 'maxlength' => '30', 'size' => '25')) . br();
  echo li_close();

  echo li();
  echo form_label('Senha do Google Analytics: ', 'ga_senha');
  echo form_password(array('name' => 'ga_senha', 'id' => 'ga_senha', 'value' => set_value('ga_senha', @base64_decode($ga_senha)), 'title' => 'Senha do Google Analytics. Também é necessária para mostrar o gráfico na Inicial.', 'class' => 'dica', 'maxlength' => '30', 'size' => '25')) . br();
  echo li_close();

  echo li();
  echo form_label('Perfil do Google Analytics: ', 'ga_perfil');
  echo form_password(array('name' => 'ga_perfil', 'id' => 'ga_perfil', 'value' => set_value('ga_perfil', $ga_perfil), 'title' => 'Perfil do Google Analytics. ', 'class' => 'dica', 'maxlength' => '30', 'size' => '25')) . br();
  echo li_close();
  /*
    echo li();
    echo form_label('Perfil do Google Analytics: ', 'ga_perfil');
    echo form_dropdown('ga_perfil', array(''), $ga_perfil, 'id="ga_perfil" class="dica" prof="'.$ga_perfil.'" title="Escolha o ID do Perfil para este site no Google Analytics."');
    echo li_close();
   */
  echo li();
  echo form_label('Código de acompanhamento: ', 'ga_analytics');
  echo form_input(array('name' => 'ga_analytics', 'id' => 'ga_analytics', 'value' => set_value('ga_analytics', @$ga_analytics), 'title' => 'Digite seu código de acompanhamento do Google Analytics para ativar a captura de dados do Google Analytics. Ex.: UA-19483569-6.', 'class' => 'dica analytics', 'maxlength' => '30', 'size' => '25')) . br();
  echo li_close();

  echo '</div>';

  echo form_fieldset_close();

  echo ul_close();


  echo '</div>';
}

if ($this->usuario->nivel == 1) { // Somente SuperUsuário
  // Tab 03 - Email
  echo '<div id="tab_3">';

  echo ul(array('class' => 'zebra'));

  echo li();
  echo form_label('Protocolo de envio: ', 'email_protocolo');
  echo form_dropdown('email_protocolo', $this->config->item('email_protocolo'), @$email_protocolo, 'class="dica" title="Escolha e o protocolo de email para envio."');
  echo li_close();

  echo li();
  echo form_label('Email de envio: ', 'email_servidor');
  echo form_input(array('name' => 'email_servidor', 'id' => 'email_servidor', 'value' => set_value('email_servidor', @$email_servidor), 'title' => 'Digite o email para ser usado como remetente no envio.', 'class' => 'dica', 'maxlength' => '200', 'size' => '50')) . br();
  echo li_close();

  echo li();
  echo form_label('Host SMTP: ', 'email_smtp');
  echo form_input(array('name' => 'email_smtp', 'id' => 'email_smtp', 'value' => set_value('email_smtp', @$email_smtp), 'title' => 'Digite o servidor SMTP.', 'class' => 'dica', 'maxlength' => '200', 'size' => '50')) . br();
  echo li_close();

  echo li();
  echo form_label('Usuário SMTP: ', 'email_usuario');
  echo form_input(array('name' => 'email_usuario', 'id' => 'email_usuario', 'value' => set_value('email_usuario', @$email_usuario), 'title' => 'Digite o usuário de email para envio.', 'class' => 'dica', 'maxlength' => '200', 'size' => '50')) . br();
  echo li_close();

  echo li();
  echo form_label('Senha SMTP: ', 'email_senha');
  echo form_input(array('name' => 'email_senha', 'id' => 'email_senha', 'value' => set_value('email_senha', @$email_senha), 'title' => 'Digite a senha de email para envio.', 'class' => 'dica', 'maxlength' => '200', 'size' => '50')) . br();
  echo li_close();

  echo li();
  echo form_label('Porta SMTP: ', 'email_porta');
  echo form_input(array('name' => 'email_porta', 'id' => 'email_porta', 'value' => set_value('email_porta', @$email_porta), 'title' => 'Digite a porta de SMTP do servidor para envio.', 'class' => 'dica', 'maxlength' => '5', 'size' => '50')) . br();
  echo li_close();

  echo li();
  echo form_label('Caminho Sendmail: ', 'email_sendmail');
  echo form_input(array('name' => 'email_sendmail', 'id' => 'email_sendmail', 'value' => set_value('email_sendmail', @$email_sendmail), 'title' => 'Digite o caminho do sendmail para envio.', 'class' => 'dica', 'maxlength' => '5', 'size' => '50')) . br();
  echo li_close();

  echo ul_close();

  echo '</div>';
}

if ($this->usuario->nivel == 1) { // Somente SuperUsuário
  // Tab 03 - Imagem
  echo '<div id="tab_4">';

  echo ul(array('class' => 'zebra'));

  echo msg('Quando é cadastrada uma imagem no módulo Mídia, são geradas 3 imagens, aqui são configurados os tamanhos destas imagens.', NULL, 'ajuda corner', FALSE) . br();

  echo li();
  echo form_label('Tamanho máximo da imagem menor: ', 'img_th');
  echo form_input(array('name' => 'img_th', 'id' => 'img_th', 'value' => set_value('img_th', @$img_th), 'title' => 'Digite o tamanho da imagem menor (exibição inicial). Ex.: 150.', 'class' => 'dica numero {validar:{required:true, messages:{required:\'Digite o tamanho thumbnail.\'}}}', 'maxlength' => '10', 'size' => '15')) . br();
  echo li_close();

  echo li();
  echo form_label('Tamanho máximo da imagem média: ', 'img_me');
  echo form_input(array('name' => 'img_me', 'id' => 'img_me', 'value' => set_value('img_me', @$img_me), 'title' => 'Digite o tamanho da imagem média (exibição no produto). Ex.: 300.', 'class' => 'dica numero {validar:{required:true, messages:{required:\'Digite o tamanho médio.\'}}}', 'maxlength' => '10', 'size' => '15')) . br();
  echo li_close();

  echo li();
  echo form_label('Tamanho máximo da imagem grande: ', 'img_gd');
  echo form_input(array('name' => 'img_gd', 'id' => 'img_gd', 'value' => set_value('img_gd', @$img_gd), 'title' => 'Digite o tamanho da imagem maior (exibida no lightbox). Ex.: 500.', 'class' => 'dica numero {validar:{required:true, messages:{required:\'Digite o tamanho grande.\'}}}', 'maxlength' => '10', 'size' => '15')) . br();
  echo li_close();

  echo ul_close();

  echo '</div>';
}

if ($this->usuario->nivel == 1) { // Somente SuperUsuário
  // Tab 04 - Twitter
  echo '<div id="tab_5">';

  echo ul(array('class' => 'zebra'));

  echo li();
  echo form_label('Usuário: ', 'tw_usuario');
  echo form_input(array('name' => 'tw_usuario', 'id' => 'tw_usuario', 'value' => set_value('tw_usuario', @$tw_usuario), 'title' => 'Digite o usuário do Twitter, para ser utilizado no site.', 'class' => 'dica', 'maxlength' => '10', 'size' => '15')) . br();
  echo li_close();

  echo ul_close();

  echo '</div>';
}

// Tab 05 - Configurações
echo '<div id="tab_6">';

echo ul(array('class' => 'zebra'));
/*
  echo li(array('class' => 'titulo'));
  echo heading('Notícias', 1, array('class' => 'titulo'));
  echo li_close();

  echo li();
  echo form_label('Utilizar Notícias Automáticas', 'noticia_auto');
  echo form_dropdown('noticia_auto', $this->config->item('simnao'), @$noticia_auto, 'class="dica" title="Deseja utilizar notícias automáticas no site? Obs.:  Não há controle sobre o conteúdo das notícias."');
  echo li_close();

  echo li();
  echo form_label('Insira o endereço das Notícias Automáticas', 'noticia_link');
  echo form_input(array('name' => 'noticia_link', 'id' => 'noticia_link', 'value' => set_value('noticia_link', @$noticia_link), 'title' => 'Digite a URL do Feed de notícias para exibir no site. Obs.: Não há controle sobre o conteúdo das notícias.', 'class' => 'dica {validar:{url:true, messages:{url:\'Digite o endereço das notícias automáticas. (com http://).\'}}}', 'maxlength' => '255', 'size' => '75')).br();
  echo li_close();
  echo msg('Quando a opção de Notícias Automáticas é habilitada, não há controle sobre o conteúdo de notícias que serão exibidas.', NULL, 'alerta corner', FALSE).br();
 */
echo li(array('class' => 'titulo'));
echo heading('Produto/Serviço', 1, array('class' => 'titulo'));
echo li_close();

echo li();
echo form_label('Sua empresa disponibiliza:', 'produto_servico');
echo form_dropdown('produto_servico', $this->config->item('pro_ser'), @$produto_servico, 'class="dica" title="Selecione o que sua empresa disponibiliza: Produto e Servico, somente Produto ou somente Serviço."');
echo li_close();
echo msg('Se a empresa possui produto e serviço, pode-se cadastrar cada um destes, utilizando a opção no cadastro do Produto.', NULL, 'ajuda corner', FALSE) . br();

echo li();
echo form_label('Mostrar Valor do Produto/Serviço', 'produto_valor');
echo form_dropdown('produto_valor', $this->config->item('simnao'), @$produto_valor, 'class="dica" title="Deseja exibir o valor do produto no site?"');
echo li_close();
echo msg('Se não for cadastrado o valor, aparecerá "Sob consulta".', NULL, 'ajuda corner', FALSE) . br();

echo li();
echo form_label('Exibir as categorias de:', 'categoria_menu');
echo form_dropdown('categoria_menu', $this->config->item('pro_ser'), @$categoria_menu, 'class="dica" title="Selecione as Categorias a serem exibidas ao lado esquerdo do banner."');
echo li_close();
echo msg('Ao lado esquerdo do banner são exibidas as categorias dos Produtos e Serviços, escolha entre exibir as categorias de  Produtos e Serviços, somente Produto ou somente Serviço.', NULL, 'ajuda corner', FALSE) . br();

echo li();
echo form_label('Exibir na página inicial:', 'exibir_inicial');
echo form_dropdown('exibir_inicial', $this->config->item('pro_ser'), @$exibir_inicial, 'class="dica" title="Selecione se na página Inicial devem ser exibidos Produtos ou Serviços."');
echo li_close();
echo msg('Na página inicial são exibidos Produtos ou Serviços, selecione qual deve ser exibido.', NULL, 'ajuda corner', FALSE) . br();

echo ul_close();

echo '</div>';

// Tab 07 - Administraçao
echo '<div id="tab_7">';
echo ul(array('class' => 'zebra'));

echo li();
echo form_label('Feed Inicial: ', 'admin_feed');
echo form_input(array('name' => 'admin_feed', 'id' => 'admin_feed', 'value' => set_value('admin_feed', @$admin_feed), 'title' => 'Endereço das notícias a serem exibidas na administração.', 'class' => 'dica {validar:{url:true, messages	:{url:\'Digite o endereço do feed inicial da administração. (com http://).\'}}}', 'maxlength' => '200', 'size' => '75')) . br();
echo li_close();

echo ul_close();
echo '</div>';


echo '</div>';

echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' => 'alterar', 'content' => 'Alterar ' . img('img/icons/pencil.png')));

echo form_close();

echo form_fieldset_close();
?>