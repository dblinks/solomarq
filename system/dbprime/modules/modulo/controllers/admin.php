<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Módulo - Controller
 *
 * @package 		Admin
 * @subpackage 	Módulo
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller{

	private $validation_rules = array(
		array(
			'field'   => 'modulo',
			'label'   => 'Módulo',
			'rules'   => 'required|alpha|trim'
		),
		array(
			'field'   => 'intro',
			'label'   => 'Introução',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'url',
			'label'   => 'URL',
			'rules'   => 'required|alpha|trim'
		),
		array(
			'field'   => 'modulo',
			'label'   => 'Seção',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'nivel',
			'label'   => 'Nível',
			'rules'   => 'required|trim'
		)
	);

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('modulo/modulo_m');
		$this->load->model('secao/secao_m');

		($this->usuario->nivel > 1)
			? $this->template->write('atalho', '', TRUE)
			: NULL;
	}

	/**
	 * Página principal - index
	 *
	 * @access	public
	 */
	public function index()
	{
		$this->template->write('conteudo', heading('Listando '.$this->modulo->alias, 1));
		$this->template->write_view('conteudo', 'admin/listar');

		$this->template->render();
	}

	/**
	 * Adiciona registro com validação do formulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function adicionar()
	{
		$this->template->write('conteudo', heading('Adicionar '.$this->modulo->alias));
		$this->template->write_view('conteudo', 'admin/form');

		// Dados recebidos
		if ($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				if($this->modulo_m->insert($this->input->post()))
				{
					$this->session->set_flashdata('msg', msg('Sucesso ao adicionar '.$this->modulo->alias, 'sucesso'));
					redirect(admin_url($this->modulo->url));
				}
				else
				{
					$this->template->write('msg', msg('Erro ao adicionar '.$this->modulo->alias, 'erro'));
				}
			}
			else
			{
				$this->template->write('msg', msg(validation_errors(), 'erro'));
			}
		}

		$this->template->render();
	}

	/**
	 * Altera registro com validação de fomulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function alterar()
	{
		$a = $this->modulo_m->get_id($this->uri->segment(4));
		$this->template->write('conteudo', heading('Alterar '.$this->modulo->alias.' : '.$a->{$this->modulo_m->_desc}));
		$this->template->write_view('conteudo', 'admin/form', $a);

		// Dados recebidos
		if ($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				if($this->modulo_m->update($this->input->post()))
				{
					$this->session->set_flashdata('msg', msg('Sucesso ao alterar '.$this->modulo->alias, 'sucesso'));
					redirect(admin_url($this->modulo->url));
				}
				else
				{
					$this->template->write('msg', msg('Erro ao alterar '.$this->modulo->alias, 'erro'));
				}
			}
			else
			{
				$this->template->write('msg', msg(validation_errors(), 'erro'));
			}
		}

		$this->template->render();
	}

	/**
	 * Exclui registro
	 *
	 * @access	public
	 * @return	redirect
	 */
	 /*
	public function excluir()
	{
		$this->modulo_m->delete($this->uri->segment(4))
			? $this->session->set_flashdata('msg', msg('Módulo excluído com sucesso!', 'sucesso'))
			: $this->session->set_flashdata('msg', msg('Erro ao excluir Módulo!', 'erro'));

		redirect(admin_url($this->modulo->url));
	}
	*/
	/**
	 * Ativa módulo
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function ativar()
	{
		$dado = array('id' => $this->uri->segment(4), 'ativo' => 1);
		$this->modulo_m->ativar($dado)
			? $this->session->set_flashdata('msg', msg('Sucesso ao ativar '.$this->modulo->alias, 'sucesso'))
			: $this->session->set_flashdata('msg', msg('Erro ao ativar '.$this->modulo->alias, 'erro'));
		redirect(admin_url($this->modulo->url));
	}

  /**
	 * Desativa módulo
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function desativar()
	{
		$dado = array('id' => $this->uri->segment(4), 'ativo' => 0);
		$this->modulo_m->desativar($dado)
			? $this->session->set_flashdata('msg', msg('Sucesso ao desativar '.$this->modulo->alias, 'sucesso'))
			: $this->session->set_flashdata('msg', msg('Sucesso ao desativar '.$this->modulo->alias, 'erro'));
		redirect(admin_url($this->modulo->url));
	}

  /**
	 * Seleciona ícone
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function icone()
	{
    $this->load->helper('directory');
    $this->template->set_template('basic');
		$a = $this->modulo_m->get_id($this->uri->segment(4));
		$this->template->write('conteudo', heading('Selecione o ícone'));
    $this->template->add_css('div.icone_modulo { width: 20px; height: 20px; line-height: 20px; float: left; vertical-align: middle; }', 'embed');
    //$this->template->add_js('', 'embed');
    $this->template->add_js('js/admin/icones.js');

		$this->template->write_view('conteudo', 'admin/icone', $a);

		$this->template->render();
  }

	/**
	 * Codifica registros em Json
	 *
	 * @access	public
	 * @return	string
	 */
	function json_listar()
	{
		######################## ALTERAR
		//Colunas de retorno
		$colunas = array($this->modulo_m->_id, $this->modulo_m->_desc, 'intro', 'versao', 'ativo', 'id_secao', 'nivel', 'opcoes');
		$secao = $this->secao_m->get_array();
		$nivel = $this->config->item('nivel');

		//Campo de ID
		$id = $colunas[0];

		// Paginação
		if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1')
		{
			$n = $_POST['iDisplayLength'];
			$offset = $_POST['iDisplayStart'];
		}
		else
		{
			if (isset($_POST['iDisplayLength']))
			{
				$n = ($_POST['iDisplayLength'] == '-1') ? NULL : $_POST['iDisplayLength'];
				$offset = 0;
			}
			else
			{
				$n = 25;
				$offset = 0;
			}
		}

		// Ordenação
		if(isset($_POST['iSortCol_0']))
		{
			$order = NULL;
			for($i=0;$i<intval($_POST['iSortingCols']);$i++)
			{
				if($_POST['bSortable_'.intval($_POST['iSortCol_'.$i])] == "true")
				{
					if(trim($colunas[intval($_POST['iSortCol_'.$i])]) == 'opcoes') { $order = NULL; break; }
					$order .= $colunas[intval($_POST['iSortCol_'.$i])]." ".$_POST['sSortDir_'.$i].", ";
				}
			}
			$order = rtrim($order, ' ,');
			if((trim($order) == 'ORDER BY') || (strlen($order) == 0)) {	$order = NULL; }
		}
		else
			$order = NULL;

		//Filtro
		$orlike = NULL;
		if(strlen($_POST['sSearch']) > 0)
		{
			$campo = array();
			$valor = array();
			for($i = 0; $i < (count($colunas)-1); $i++)
			{
				$campo[] = $colunas[$i];
				$valor[] = trim($_POST['sSearch']);
			}
			$orlike = array_combine($campo, $valor);
		}

		######################## ALTERAR
		//SQL Query
		$query = $this->modulo_m->get_all($n, $offset, NULL, NULL, $order, $orlike);

		// Resultado do filtro
		$query_filtro = $this->modulo_m->get_all(NULL, NULL, NULL, NULL, $order, $orlike);
		$filtro = $query_filtro->num_rows();

		// Total
		$query_total = $this->modulo_m->get_all();
		$total = $query_total->num_rows();

		//Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $total,
			"iTotalDisplayRecords" => $filtro,
			"aaData" => array()
		);

		######################## ALTERAR
		foreach($query->result() as $row)
		{
			$retorno = array();
			for($i=0;$i<count($colunas);$i++)
			{
				switch($colunas[$i])
				{
					case 'opcoes': // Ações
						$links = array();
						(!$row->ativo) ? $links[] = anchor(admin_url($this->modulo->url.'/ativar/'.$row->{$this->modulo_m->_id}), 'ativar', array('class' => 'ativar')) : NULL;
						($row->ativo) ? $links[] = anchor(admin_url($this->modulo->url.'/desativar/'.$row->{$this->modulo_m->_id}), 'desativar', array('class' => 'desativar')) : NULL;
						if(!$row->nativo)
						{
							(!$row->ativo) ? $links[] = anchor(admin_url($this->modulo->url.'/excluir/'.$row->{$this->modulo_m->_id}), 'excluir', array('desc' => $row->{$this->modulo_m->_desc}, 'class' => 'excluir', 'onclick' => 'excluir();')) : '';
							$links[] = anchor(admin_url($this->modulo->url.'/desinstalar/'.$row->{$this->modulo_m->_id}), 'desinstalar', array('desc' => $row->{$this->modulo_m->_desc}, 'class' => 'desinstalar', 'onclick' => 'excluir();'));
						}
						($this->usuario->nivel == 1) ? $links[] = anchor(admin_url($this->modulo->url.'/alterar/'.$row->{$this->modulo_m->_id}), 'alterar', array('class' => 'alterar')) : NULL;
						$retorno[] = implode(' | ', $links);
					break;
					case 'id_secao':
						$retorno[] = $secao[$row->id_secao];
					break;
					case 'desc_modulo':
            //$desc = (!is_null($row->icone)) ? img($row->icone).nbs() : NULL;
            //$desc .= ($row->menu && $row->ativo) ? anchor(admin_url($row->url), $row->{$this->modulo_m->_desc}) : $row->{$this->modulo_m->_desc};
						$retorno[] = ($row->menu && $row->ativo) ? anchor(admin_url($row->url), $row->{$this->modulo_m->_desc}) : $row->{$this->modulo_m->_desc};
					break;
					case 'ativo':
						$retorno[] = center(status_icon($row->ativo));
					break;
					case 'nativo':
						$retorno[] = center(status_icon($row->nativo));
					break;
					case 'versao':
						$retorno[] = center('v'.$row->versao);
					break;
					case 'nivel':
						$pos_nivel = (strlen($row->nivel) == 0) ? 0 : $row->nivel;
						$retorno[] = $nivel[$pos_nivel];
					break;
					default: // Outras colunas
						$retorno[] = $row->{$colunas[$i]};
				}
			}
			$output['aaData'][] = $retorno;
		}
		echo json_encode($output);
	}
}

/* End of file modulo.php */
/* Location: ./app/modules/modulo/controllers/modulo.php */
