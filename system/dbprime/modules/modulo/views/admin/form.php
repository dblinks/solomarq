<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => $id_modulo));

if($this->_acao == 'alterar')
{
  echo li();
  echo form_label('ID Módulo: ', 'id_modulo');
  echo form_input(array('name' => 'id_modulo', 'id' => 'id_modulo', 'value' => set_value('id_modulo', @$id_modulo), 'title' => 'Digite o novo ID do módulo.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite o ID módulo\'}}}', 'maxlength' => '50', 'size' => '25')).br();
  echo li_close();
}

echo li();
echo form_label('Módulo: ', 'modulo');
echo form_input(array('name' => 'modulo', 'id' => 'modulo', 'value' => set_value('modulo', @$desc_modulo), 'title' => 'Digite o nome do módulo (interno).', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite o módulo\'}}}', 'maxlength' => '50', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Alias: ', 'alias');
echo form_input(array('name' => 'alias', 'id' => 'alias', 'value' => set_value('alias', @$alias), 'title' => 'Digite o nome do módulo exibido no sistema.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite o módulo\'}}}', 'maxlength' => '50', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Introdução: ', 'intro');
echo form_input(array('name' => 'intro', 'id' => 'intro', 'value' => set_value('intro', @$intro), 'title' => 'Digite uma introdução rápida ao módulo, é exibida na barra superior.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite uma introdução ao módulo\'}}}', 'maxlength' => '250', 'size' => '50')).br();
echo li_close();
/*
echo li();
echo form_label('Ícone: ', 'icone');
echo form_input(array('name' => 'icone', 'id' => 'icone', 'value' => set_value('icone', @$icone), 'title' => 'Clique e selecione um ícone.', 'class' => 'dica', 'maxlength' => '50', 'size' => '25', 'rel' => 'colorbox'));
$icone = (isset($icone)) ? img($icone).nbs() : '';
echo anchor(admin_url('modulo/icone'), $icone.'Clique para selecionar', 'class="colorbox"').br();
echo li_close();
*/
echo li();
echo form_label('URL: ', 'url');
echo form_input(array('name' => 'url', 'id' => 'url', 'value' => set_value('url', @$url), 'title' => 'Digite a URI do módulo, deve ser única', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite a url\'}}}', 'maxlength' => '50', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Versão: ', 'versao');
echo form_input(array('name' => 'versao', 'id' => 'versao', 'value' => set_value('versao', @$versao), 'title' => 'Digite a versão do módulo.', 'class' => 'dica corner', 'maxlength' => '10', 'size' => '5')).br();
echo li_close();

echo li();
echo form_label('Nativo: ', 'nativo');
echo form_dropdown('nativo', $this->config->item('simnao'), (@$nativo == 0) ? 0 : 1, 'class="dica" title="Este módulo é nativo?"');
echo li_close();

echo li();
echo form_label('Exibir no Menu: ', 'menu');
echo form_dropdown('menu', $this->config->item('simnao'), (@$menu == 0) ? 0 : 1, 'class="dica" title="Este módulo deve ser exibido no menu lateral?"');
echo li_close();

echo li();
echo form_label('Ativo: ', 'ativo');
echo form_dropdown('ativo', $this->config->item('simnao'), (@$ativo == 0) ? 0 : 1, 'class="dica" title="Este módulo está ativo?"');
echo li_close();

echo li();
echo form_label('Seção: ', 'secao');
echo form_dropdown('secao', $this->secao_m->get_array(), @$id_secao, 'id="secao" title="Selecione a seção que o módulo será exibido no menu lateral." class="dica corner {validar:{required:true, messages:{required:\'Selecione uma seção\'}}}"').br();
echo li_close();

echo li();
echo form_label('Nível: ', 'nivel');
echo form_dropdown('nivel', $this->config->item('nivel'), (!isset($nivel)) ? 2 : $nivel , 'id="nivel" title="Selecione o nível de acesso do módulo." class="dica corner {validar:{required:true, messages:{required:\'Selecione o nível\'}}}"').br();
echo li_close();

echo li();
echo form_label('Midia: ', 'midia');
echo form_dropdown('midia', $this->config->item('simnao'), (@$midia == 0) ? 0 : 1, 'class="dica" title="Este módulo tem suporte á Mídia?"');
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>