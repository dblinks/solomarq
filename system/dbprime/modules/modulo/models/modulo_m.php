<?php

/**
 * Modulo_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Modulo
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Modulo_m extends DBModel {

	var $_tabela = 'modulo';
	var $_id = 'id_modulo';
	var $_desc = 'desc_modulo';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_modulo ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
	}

  /**
	 * Função executada antes da inserção
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function _before_insert($dado, $id = NULL)
	{
		//$this->db->where_not_in($this->_id, $id);
		$a = $this->get_by_url($dado['url']);
		if ($a->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Modulo já cadastrado com esta url', 'erro'));
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Função executada antes da alteração
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function _before_update($dado, $id = NULL)
	{
		$this->db->where_not_in($this->_id, $id);
		$a = $this->get_by_url($dado['url']);
		if ($a->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Modulo já cadastrado com esta url', 'erro'));
			return FALSE;
		}

		return TRUE;
	}

  /**
	 * Altera usuário
	 * com validação de url
	 *
	 * @access	public
	 * @return	boolean
	 */
	function insert($dado, $tabela = NULL)
	{
		if(!$this->_before_insert($dado, $dado['id'])) { return  FALSE; }

		return parent::insert(array(
		  'id_secao'	=> $dado['secao'],
			$this->_desc	=> $dado['modulo'],
			'alias'	=> $dado['alias'],
      'intro'	=> $dado['intro'],
			'url'	=> $dado['url'],
			'versao'	=> $dado['versao'],
			'ativo'	=> $dado['ativo'],
			'menu'	=> $dado['menu'],
			'nativo'	=> $dado['nativo'],
			'nivel'	=> (int)$dado['nivel'],
			'midia'	=> $dado['midia']
		));
	}

	/**
	 * Altera usuário
	 * com validação de url
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		if(!$this->_before_update($dado, $dado['id'])) { return  FALSE; }

    if($dado['id'] != $dado['id_modulo'])
      parent::update(array($this->_id	=> $dado['id_modulo']), $dado['id']);

		return parent::update(array(
		  'id_secao'	=> $dado['secao'],
			$this->_desc	=> $dado['modulo'],
			'alias'	=> $dado['alias'],
      'intro'	=> $dado['intro'],
			'url'	=> $dado['url'],
			'versao'	=> $dado['versao'],
			'ativo'	=> $dado['ativo'],
			'menu'	=> $dado['menu'],
			'nativo'	=> $dado['nativo'],
			'nivel'	=> (int)$dado['nivel'],
			'midia'	=> $dado['midia']
		), $dado['id']);
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function ativar($dado = array())
	{
		return parent::update(array(
			'ativo'	=> $dado['ativo']
		), $dado['id']);
	}

	/**
	 * Desativa módulo
	 *
	 * @access	public
	 * @return	boolean
	 */
	function desativar($dado = array())
	{
		return parent::update(array(
			'ativo'	=> $dado['ativo']
		), $dado['id']);
	}
}

/* End of file Modulo_m.php */
/* Location: ./app/modules/modulo/models/Modulo_m.php */
