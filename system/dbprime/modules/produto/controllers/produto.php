<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Produto extends Site_Controller
{

    var $titulo_pg = NULL;

    function __construct()
    {
        parent::__construct();
        $this->load->model('produto/produto_m');

        $this->load->library('pagination');

        $this->js_css();

        $this->titulo_pg = $this->menu_m->get_field('desc_menu', 'produto', 'slug');
    }

    function js_css()
    {
        // CSS
        $this->template->add_css($this->tema_url('css/produto.css'));

        //JS
        $this->template->add_js('js/jquery.tools.min.js');
        $this->template->add_js('js/galleria-1.2.5.min.js');
        $this->template->add_js('js/galleria/galleria.classic.min.js');
    }

    public function index()
    {
        // Título
        $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');

        $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($p->desc_menu), 'pagina' => $p->desc_menu));
        $this->_titulo[] = $p->desc_menu;

        // Total de Produtos
        $todos = $this->midia_m->get_midia_join('produto', NULL, NULL, NULL, TRUE, array('produto.status' => 1, 'produto.tipo' => 1));
        $total = is_null($todos) ? 0 : $todos->num_rows();

        // Produtos
        $produtos = $this->midia_m->get_midia_join('produto', NULL, $this->config->item('produto_por_pagina'), NULL, TRUE, array('produto.status' => 1, 'produto.tipo' => 1), 'produto.destaque DESC, produto.desc_produto DESC');

        $this->template->write_view('conteudo', 'index', array('produtos' => $produtos, 'total' => $total));
        $this->template->write_view('titulo', 'comum/title');
        $this->template->render();
    }

    public function busca_ajax()
    {
        $this->template->set_template('no');
        $this->load->helper('text');

        $produtos = $this->produto_m->get_all(NULL, NULL, NULL, array('desc_produto' => $this->input->post('busca')));
        $categorias = $this->categoria_m->get_array();

        $this->template->write_view('conteudo', 'busca_ajax', array('produtos' => $produtos, 'categorias' => $categorias));
        $this->template->render();
    }

    public function busca()
    {
        if ($this->input->post()) {
            redirect(site_url() . 'produto/busca/' . slug($this->input->post('busca')));
        }

        $todos = $this->midia_m->get_midia_join('produto', NULL, NULL, NULL, TRUE, array('produto.status' => 1), 'produto.destaque DESC, produto.desc_produto DESC', array('slug' => $this->uri->segment(3)));
        $total = is_null($todos) ? 0 : $todos->num_rows();

        $ate = ($this->uri->segment(6)) ? ($this->config->item('produto_por_pagina') * ($this->uri->segment(6) - 1)) : 0;
        $produtos = $this->midia_m->get_midia_join('produto', NULL, $this->config->item('produto_por_pagina'), $ate, TRUE, array('produto.status' => 1), 'produto.destaque DESC, produto.desc_produto DESC', array('slug' => $this->uri->segment(3)));

        $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($this->titulo_pg, 'Busca', $this->uri->segment(3)), 'pagina' => 'Busca'));
        $this->template->write_view('conteudo', 'busca', array('produtos' => $produtos, 'total' => $total));
        $this->_titulo[] = $this->titulo_pg;
        $this->_titulo[] = 'Busca';
        $this->_titulo[] = $this->uri->segment(3);
        $this->template->write_view('titulo', 'comum/title');
        $this->template->render();
    }

    public function categoria()
    {
        $c = $this->categoria_m->get_id($this->uri->segment(3));

        $this->template->write_view('conteudo', 'produto_categoria', array('titulo' => array($this->titulo_pg, 'Categoria', $c->desc_categoria), 'pagina' => $this->titulo_pg, 'categoria' => $c->desc_categoria));

        // Total de Produtos
        $todos = $this->midia_m->get_midia_join('produto', NULL, NULL, NULL, TRUE, array('produto.status' => 1, 'produto.tipo' => 1, 'produto.id_categoria' => $this->uri->segment(3)));
        $total = is_null($todos) ? 0 : $todos->num_rows();

        // Produtos
        $ate = ($this->uri->segment(6)) ? ($this->config->item('produto_por_pagina') * ($this->uri->segment(6) - 1)) : 0;
        $produtos = $this->midia_m->get_midia_join('produto', NULL, $this->config->item('produto_por_pagina'), $ate, TRUE, array('produto.status' => 1, 'produto.tipo' => 1, 'produto.id_categoria' => $this->uri->segment(3)), 'produto.destaque DESC, produto.desc_produto DESC');

        $this->_titulo[] = $this->titulo_pg;
        $this->_titulo[] = 'Categoria';
        $this->_titulo[] = $c->desc_categoria;
        $this->template->write_view('titulo', 'comum/title');
        $this->template->write_view('conteudo', 'categoria', array('produtos' => $produtos, 'total' => $total));

        $this->template->render();
    }

    public function indique()
    {
        $this->template->set_template('basic');

        //CSS
        $this->template->add_css($this->tema_url('css/produto.css'));
        $this->template->add_css($this->tema_url('css/contato.css'));

        //JS
        $this->template->add_js('js/jquery.meio.mask.js');
        $this->template->add_js('js/jquery.metadata.js');
        $this->template->add_js('js/jquery.validate.min.js');
        $this->template->add_js('js/jquery.sleep.js');
        $this->template->add_js('js/site/form.js');

        // Título
        $produto = $this->produto_m->get_id($this->uri->segment(3));

        // Enviando
        if ($this->input->post()) {
            unset($_POST['enviar_x']);
            unset($_POST['enviar_y']);

            $post = $this->input->post();

            $post['assunto'] = 'Um amigo seu recomendou um produto';

            // Envio do email
            $introducao = 'Olá <b>' . $post['amigo_nome'] . '</b>,' . br() . '<b>' . $post['seu_nome'] . '</b> lhe recomendou o produto ' . $produto->desc_produto;
            $mensagem = $post['mensagem'] . br() . anchor($post['url'], 'Clique aqui para ver o produto no site.') . br(2);
            $assunto = $post['assunto'];
            $de_email = $post['seu_email'];
            $de = $post['seu_nome'];
            $para = $post['amigo_email'];

            // Cadatro no banco de dados
            if (parent::envia_email($introducao, $mensagem, $assunto, $de_email, $de, $para)) {
                $this->template->write('conteudo', msg('Sua mensagem foi enviada com sucesso.', 'sucesso margem corner'));
            } else {
                $this->template->write('conteudo', msg('Infelizmente sua mensagem não pode ser enviada, tente novamente mais tarde.', 'alerta margem corner'));
            }
            $this->template->add_js('$(document).ready(function() { $.sleep(2, function() { window.parent.$.colorbox.close(); }); });', 'embed');
        } else {
            $this->template->write_view('conteudo', 'indique', array('p' => $produto));
        }

        $this->template->render();
    }

    public function interesse()
    {
        $this->template->set_template('basic');

        //CSS
        $this->template->add_css($this->tema_url('css/produto.css'));
        $this->template->add_css($this->tema_url('css/contato.css'));

        //JS
        $this->template->add_js('js/jquery.meio.mask.js');
        $this->template->add_js('js/jquery.metadata.js');
        $this->template->add_js('js/jquery.validate.min.js');
        $this->template->add_js('js/jquery.sleep.js');
        $this->template->add_js('js/site/form.js');

        // Produto
        $produto = $this->produto_m->get_id($this->uri->segment(3));

        // Enviando
        if ($this->input->post()) {
            unset($_POST['enviar_x']);
            unset($_POST['enviar_y']);

            $post = $this->input->post();

            $post['assunto'] = 'Interesse no produto: ' . $produto->desc_produto;

            // Envio do email
            $introducao = $post['assunto'];
            $mensagem = $post['mensagem'] . br(2) . $post['nome'] . br() . $post['email'] . br() . $post['telefone'];
            $assunto = $post['assunto'];
            $de_email = $post['email'];
            $de = $post['nome'];
            $para = $this->site->email_contato;

            // Cadatro no banco de dados
            if (parent::envia_email($introducao, $mensagem, $assunto, $de_email, $de, $para)) {
                $this->template->write('conteudo', msg('Sua mensagem foi enviada com sucesso.', 'sucesso margem corner'));
            } else {
                $this->template->write('conteudo', msg('Infelizmente sua mensagem não pode ser enviada, tente novamente mais tarde.', 'alerta margem corner'));
            }
            $this->template->add_js('$(document).ready(function() { $.sleep(2, function() { window.parent.$.colorbox.close(); }); });', 'embed');
        } else {
            $this->template->write_view('conteudo', 'interesse', array('p' => $produto));
        }

        $this->template->render();
    }

    public function pg()
    {
        // Título
        $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');
        $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($this->titulo_pg, 'Página ' . @$this->uri->segment(3)), 'pagina' => $p->desc_menu));

        // Total de Produtos
        $todos = $this->midia_m->get_midia_join('produto', NULL, NULL, NULL, TRUE, array('produto.status' => 1, 'produto.tipo' => 1), 'produto.destaque DESC');
        $total = is_null($todos) ? 0 : $todos->num_rows();

        // Produtos
        $ate = ($this->config->item('produto_por_pagina') * ($this->uri->segment(3) - 1));
        $produtos = $this->midia_m->get_midia_join('produto', NULL, $this->config->item('produto_por_pagina'), $ate, TRUE, array('produto.status' => 1, 'produto.tipo' => 1), 'produto.destaque DESC, produto.desc_produto ASC');

        $this->_titulo[] = $this->titulo_pg;
        $this->_titulo[] = 'Página';
        $this->_titulo[] = @$this->uri->segment(3);
        $this->template->write_view('titulo', 'comum/title');
        $this->template->write_view('conteudo', 'index', array('produtos' => $produtos, 'total' => $total));

        $this->template->render();
    }

    public function ver()
    {
        $p = $this->produto_m->get_id($this->uri->segment(3));
        $c = $this->categoria_m->get_id($p->id_categoria);

        // Título
        $this->template->write_view('conteudo', 'produto_categoria', array('titulo' => array($this->titulo_pg, 'Categoria', $p->desc_produto), 'pagina' => $this->titulo_pg, 'categoria' => $c->desc_categoria));

        $this->_titulo[] = $this->titulo_pg;
        $this->_titulo[] = 'Categoria';
        $this->_titulo[] = $p->desc_produto;
        $this->template->write_view('titulo', 'comum/title');

        // Produto
        $this->template->write_view('conteudo', 'ver', array('p' => $p));

        //print_r($produto);
        $this->template->render();
    }

}

/* End of file site.php */
/* Location: ./application/controllers/site.php */
