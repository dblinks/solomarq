<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Produto - Controller
 *
 * @package        Admin
 * @subpackage    Produto
 * @category        Módulo
 * @author            Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version        1.0 #20110921
 */
class Admin extends Admin_Controller
{

    private $validation_rules = array(
        array(
            'field' => 'produto',
            'label' => 'Produto',
            'rules' => 'required|trim'
        ),
        array(
            'field' => 'texto',
            'label' => 'Texto',
            'rules' => 'required|trim'
        ),
        array(
            'field' => 'categoria',
            'label' => 'Categoria',
            'rules' => 'required|trim'
        )
    );

    private $validation_rules_caracteristica = array(
        array(
            'field' => 'caracteristica',
            'label' => 'Característica',
            'rules' => 'required|trim'
        ),
        array(
            'field' => 'texto',
            'label' => 'Texto',
            'rules' => 'required|trim'
        )
    );

    /**
     * Inicializa a classe
     *
     * @access    public
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('produto/produto_m');
        $this->load->model('categoria/categoria_m');
        $this->load->model('caracteristica/caracteristica_m');
        $this->load->model('sitemap_m');
    }

    /**
     * Página principal - index
     *
     * @access    public
     */
    public function index()
    {
        $this->template->write('conteudo', heading('Listando ' . $this->modulo->alias, 1));
        $this->template->write_view('conteudo', 'admin/listar');

        $this->template->render();
    }

    /**
     * Adiciona registro com validação do formulário
     *
     * @access    public
     * @return    boolean | redirect
     */
    public function adicionar()
    {
        $this->template->write('conteudo', heading('Adicionar ' . $this->modulo->alias, 1));
        $this->template->write_view('conteudo', 'admin/form');

        // Dados recebidos
        if ($_POST) {
            $this->form_validation->set_rules($this->validation_rules);

            if ($this->form_validation->run()) {
                $dado = $this->input->post();
                $_POST['valor'] = ($dado['valor'] > 0) ? $dado['valor'] : 0;
                $_POST['parcela'] = ($dado['parcela'] > 0) ? $dado['parcela'] : 0;
                $_POST['parcela_valor'] = ($dado['parcela_valor'] > 0) ? $dado['parcela_valor'] : 0;
                if ($this->produto_m->insert($this->input->post())) {
                    $this->session->set_flashdata('msg', msg('Sucesso ao adicionar ' . $this->modulo->alias, 'sucesso'));
                    $this->sitemap_m->gerar();
                    redirect(admin_url($this->modulo->url));
                } else {
                    $this->template->write('msg', msg('Erro ao adicionar ' . $this->modulo->alias, 'erro'));
                }
            } else {
                $this->template->write('msg', msg(validation_errors(), 'erro'));
            }
        }

        $this->template->render();
    }

    /**
     * Altera registro com validação de fomulário
     *
     * @access    public
     * @return    boolean | redirect
     */
    public function alterar()
    {
        $a = $this->produto_m->get_id($this->uri->segment(4));
        $this->template->write('conteudo', heading('Alterar Produto : ' . $a->{$this->produto_m->_desc}));
        $this->template->write_view('conteudo', 'admin/form', $a);

        // Dados recebidos
        if ($_POST) {
            $this->form_validation->set_rules($this->validation_rules);

            if ($this->form_validation->run()) {
                $dado = $this->input->post();
                $_POST['valor'] = ($dado['valor'] > 0) ? $dado['valor'] : 0;
                $_POST['parcela'] = ($dado['parcela'] > 0) ? $dado['parcela'] : 0;
                $_POST['parcela_valor'] = ($dado['parcela_valor'] > 0) ? $dado['parcela_valor'] : 0;
                if ($this->produto_m->update($this->input->post())) {
                    $this->session->set_flashdata('msg', msg('Sucesso ao alterar ' . $this->modulo->alias, 'sucesso'));
                    $this->sitemap_m->gerar();
                    redirect(admin_url($this->modulo->url));
                } else {
                    $this->template->write('msg', msg('Erro ao alterar ' . $this->modulo->alias, 'erro'));
                }
            } else {
                $this->template->write('msg', msg(validation_errors(), 'erro'));
            }
        }

        $this->template->render();
    }

    /**
     * Exclui registro
     *
     * @access    public
     * @return    redirect
     */
    public function excluir()
    {
        $this->midia_m->delete_batch($this->produto_m->_tabela, $this->uri->segment(4));

        if ($this->produto_m->delete($this->uri->segment(4))) {
            $this->session->set_flashdata('msg', msg('Sucesso ao excluir ' . $this->modulo->alias, 'sucesso'));
            $this->sitemap_m->gerar();
        } else
            $this->session->set_flashdata('msg', msg('Erro ao excluir ' . $this->modulo->alias, 'erro'));

        redirect(admin_url($this->modulo->url));
    }

    /**
     * Característica do produto
     *
     * @access    public
     * @return    boolean | redirect
     */
    public function caracteristica()
    {
        // Excluir
        if ($this->uri->segment(4) == 'excluir') {
            if ($this->produto_m->caracteristica_delete($this->uri->segment(5), 'produto_caracteristica', 'id_produto_caracteristica')) {
                $this->session->set_flashdata('msg', msg('Sucesso ao excluir característica', 'sucesso'));
            } else {
                $this->session->set_flashdata('msg', msg('Erro ao excluir característica', 'erro'));
            }
            redirect($this->agent->referrer());
        }

        $p = $this->produto_m->get_id($this->uri->segment(4));
        $this->template->write('conteudo', heading('Detalhes do Produto : ' . $p->{$this->produto_m->_desc}));

        if ($this->uri->segment(5)) {
            $p = $this->produto_m->get_id($this->uri->segment(5), 'id_produto_caracteristica', 'produto_caracteristica');
            $ca = $this->caracteristica_m->get_id($p->id_caracteristica);

            $this->template->write('conteudo', heading('Alterar característica : ' . $ca->desc_caracteristica), 2);
        }

        if ($this->input->post()) {
            $this->form_validation->set_rules($this->validation_rules_caracteristica);

            if ($this->form_validation->run()) {
                //Alterar Detalhe
                if ($this->uri->segment(5)) {
                    if ($this->produto_m->caracteristica_update($this->input->post(), 'produto_caracteristica')) {
                        $this->template->write('msg', msg('Sucesso ao alterar característica', 'sucesso'));
                    } else {
                        $this->template->write('msg', msg('Erro ao alterar característica', 'erro'));
                    }
                } else // Adicionar Detalhe
                {
                    if ($this->produto_m->caracteristica_insert($this->input->post(), 'produto_caracteristica')) // Cadastrar
                    {
                        $this->template->write('msg', msg('Sucesso ao adicionar característica', 'sucesso'));
                    } else {
                        $this->template->write('msg', msg('Erro ao adicionar característica', 'erro'));
                    }
                }
            } else {
                $this->template->write('msg', msg(validation_errors(), 'erro'));
            }

        }

        //$p = $this->produto_m->get_id($this->uri->segment(4));

        $this->template->write_view('conteudo', 'admin/caracteristica', $p);

        $this->template->render();
    }

    /**
     * Detalhe de produto
     *
     * @access    public
     * @return    boolean | redirect
     */
    public function detalhe()
    {
        // Excluir
        if ($this->uri->segment(4) == 'excluir') {
            if ($this->produto_m->detalhe_delete($this->uri->segment(5), 'produto_detalhe', 'id_produto_detalhe')) {
                $this->session->set_flashdata('msg', msg('Sucesso ao excluir detalhe', 'sucesso'));
            } else {
                $this->session->set_flashdata('msg', msg('Erro ao excluir detalhe', 'erro'));
            }
            redirect($this->agent->referrer());
        }

        $a = $this->produto_m->get_id($this->uri->segment(4));
        $this->template->write('conteudo', heading('Detalhes do Produto : ' . $a->{$this->produto_m->_desc}));

        if ($this->uri->segment(5)) {
            $c = $this->produto_m->get_id($this->uri->segment(5), 'id_produto_detalhe', 'produto_detalhe');
            $this->template->write('conteudo', heading('Alterar detalhe : ' . $c->desc_produto_detalhe), 2);
        } else {
            $c = $a;
        }

        // Dados recebidos
        if ($_POST) {
            $this->form_validation->set_rules($this->validation_rules_detalhe);

            if ($this->form_validation->run()) {
                //Alterar Detalhe
                if ($this->uri->segment(5)) {
                    if ($this->produto_m->detalhe_update($this->input->post(), 'produto_detalhe')) {
                        $this->template->write('msg', msg('Sucesso ao alterar detalhe', 'sucesso'));
                    } else {
                        $this->template->write('msg', msg('Erro ao alterar detalhe', 'erro'));
                    }
                } else // Adicionar Detalhe
                {
                    if ($this->produto_m->detalhe_insert($this->input->post(), 'produto_detalhe')) // Cadastrar
                    {
                        $this->template->write('msg', msg('Sucesso ao adicionar detalhe', 'sucesso'));
                    } else {
                        $this->template->write('msg', msg('Erro ao adicionar detalhe', 'erro'));
                    }
                }
            } else {
                $this->template->write('msg', msg(validation_errors(), 'erro'));
            }
        }

        $this->template->write_view('conteudo', 'admin/detalhe', $c);

        $this->template->render();
    }

    /**
     * Codifica registros em Json
     *
     * @access    public
     * @return    string
     */
    function json_listar()
    {
        ######################## ALTERAR
        //Colunas de retorno
        $colunas = array($this->produto_m->_id, $this->produto_m->_desc, 'id_categoria', 'status', 'destaque', 'opcoes');
        $categoria = $this->categoria_m->get_array();
        $url_modulo_midia = $this->modulo_m->get_field('url', 'midia', 'url');

        //Campo de ID
        $id = $colunas[0];

        // Paginação
        if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1') {
            $n = $_POST['iDisplayLength'];
            $offset = $_POST['iDisplayStart'];
        } else {
            if (isset($_POST['iDisplayLength'])) {
                $n = ($_POST['iDisplayLength'] == '-1') ? NULL : $_POST['iDisplayLength'];
                $offset = 0;
            } else {
                $n = 25;
                $offset = 0;
            }
        }

        // Ordenação
        if (isset($_POST['iSortCol_0'])) {
            $order = NULL;
            for ($i = 0; $i < intval($_POST['iSortingCols']); $i++) {
                if ($_POST['bSortable_' . intval($_POST['iSortCol_' . $i])] == "true") {
                    if (trim($colunas[intval($_POST['iSortCol_' . $i])]) == 'opcoes') {
                        $order = NULL;
                        break;
                    }
                    $order .= $colunas[intval($_POST['iSortCol_' . $i])] . " " . $_POST['sSortDir_' . $i] . ", ";
                }
            }
            $order = rtrim($order, ' ,');
            if ((trim($order) == 'ORDER BY') || (strlen($order) == 0)) {
                $order = NULL;
            }
        } else
            $order = NULL;

        //Filtro
        $orlike = NULL;
        if (strlen($_POST['sSearch']) > 0) {
            $campo = array();
            $valor = array();
            for ($i = 0; $i < (count($colunas) - 1); $i++) {
                $campo[] = $colunas[$i];
                $valor[] = trim($_POST['sSearch']);
            }
            $orlike = array_combine($campo, $valor);
        }

        ######################## ALTERAR
        //SQL Query
        $query = $this->produto_m->get_all($n, $offset, NULL, NULL, $order, $orlike);

        // Resultado do filtro
        $query_filtro = $this->produto_m->get_all(NULL, NULL, NULL, NULL, $order, $orlike);
        $filtro = $query_filtro->num_rows();

        // Total
        $query_total = $this->produto_m->get_all();
        $total = $query_total->num_rows();

        //Output
        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $total,
            "iTotalDisplayRecords" => $filtro,
            "aaData" => array()
        );

        ######################## ALTERAR
        foreach ($query->result() as $row) {
            $retorno = array();
            for ($i = 0; $i < count($colunas); $i++) {
                switch ($colunas[$i]) {
                    case 'opcoes': // Ações
                        $links = array();
                        $links[] = anchor(admin_url($this->modulo->url . '/alterar/' . $row->{$this->produto_m->_id}), 'alterar', array('class' => 'alterar'));
                        $links[] = anchor(admin_url($this->modulo->url . '/excluir/' . $row->{$this->produto_m->_id}), 'excluir', array('desc' => $row->{$this->produto_m->_desc}, 'class' => 'excluir', 'onclick' => 'excluir();'));
                        (in_array($url_modulo_midia, $this->_permissao) && $this->modulo->midia) ? $links[] = anchor(admin_url('midia/adicionar/' . $this->modulo->url . '/' . $row->{$this->produto_m->_id}), 'mídia', array('class' => 'midia')) : NULL;
                        //$links[] = anchor(admin_url('midia/adicionar/'.$this->modulo->url.'/'.$row->{$this->produto_m->_id}), 'mídia', array('class' => 'midia'));
                        $links[] = anchor(admin_url($this->modulo->url . '/caracteristica/' . $row->{$this->produto_m->_id}), 'característica', array('class' => 'caracteristica'));
                        $retorno[] = implode(' | ', $links);
                        break;
                    case 'id_categoria':
                        $retorno[] = $categoria[$row->id_categoria];
                        break;
                    case 'status':
                        $retorno[] = center(status_icon($row->status), 'Sim', 'Não');
                        break;
                    case 'destaque':
                        $retorno[] = center(status_icon($row->destaque));
                        break;
                    default: // Outras colunas
                        $retorno[] = $row->{$colunas[$i]};
                }
            }
            $output['aaData'][] = $retorno;
        }
        echo json_encode($output);
    }
}

/* End of file produto.php */
/* Location: ./app/modules/produto/controllers/produto.php */
