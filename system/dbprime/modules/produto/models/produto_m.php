<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Produto_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Produto
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Produto_m extends DBModel {

	var $_tabela = 'produto';
	var $_id = 'id_produto';
	var $_desc = 'desc_produto';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_produto ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		return parent::insert(array(
			'id_categoria'	=> $dado['categoria'],
			$this->_desc	=> $dado['produto'],
			'texto'	=> $dado['texto'],
      'video'	=> $dado['video'],
			'destaque' => $dado['destaque'],
			'status'	=> $dado['status'],
			'tipo'	=> $dado['tipo'],
			'slug'	=> slug($dado['produto']),
      'valor' => v2p($dado['valor']),
      'parcela' => $dado['parcela'],
      'parcela_valor' => v2p($dado['parcela_valor'])
		));
	}

  /**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function caracteristica_insert(Array $dado, $tabela)
	{
		return parent::insert(array(
			'id_produto'	=> $dado['produto'],
			'id_caracteristica'	=> $dado['caracteristica'],
			'texto' => $dado['texto']
		), $tabela);
	}

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::update(array(
			'id_categoria'	=> $dado['categoria'],
			$this->_desc	=> $dado['produto'],
			'texto'	=> $dado['texto'],
      'video'	=> $dado['video'],
			'destaque' => $dado['destaque'],
			'status'	=> $dado['status'],
			'tipo'	=> $dado['tipo'],
			'slug'	=> slug($dado['produto']),
      'valor' => v2p($dado['valor']),
      'parcela' => $dado['parcela'],
      'parcela_valor' => v2p($dado['parcela_valor'])
		), $dado['id']);
	}

   /**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function caracteristica_update(Array $dado, $tabela)
	{
		return parent::update(array(
			'id_caracteristica'	=> $dado['caracteristica'],
			'texto' => $dado['texto']
		), $dado['id'], $tabela, 'id_produto_caracteristica');
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
    $qry = $this->produto_m->get_all(NULL, NULL, array($this->produto_m->_id => $id), NULL, 'texto ASC', NULL, NULL, NULL, NULL, NULL, 'produto_caracteristica');
    if($qry->num_rows() > 0)
    {
      foreach ($qry->result() as $d){
        $this->caracteristica_delete($d->id_produto_caracteristica, 'produto_caracteristica', 'id_produto_caracteristica');
      }
    }

    $this->midia_m->delete_batch($this->modulo->url, $id);

    return parent::delete($id);
	}

  /**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function caracteristica_delete($id, $tabela, $id_tabela)
	{
		return parent::delete($id, $tabela, $id_tabela);
	}

  /**
	 * Pega detalhes
	 *
	 * @access	public
	 * @return	boolean
	 */
	function get_caracteristica($id)
	{
    $this->db->select('c.desc_caracteristica AS caracteristica, pc.texto AS texto');
    $this->db->from('produto_caracteristica pc');
    $this->db->join('caracteristica c', 'pc.id_caracteristica = c.id_caracteristica');
    $this->db->where('pc.id_produto', $id);
    return $this->db->get();
	}
}

/* End of file Produto_m.php */
/* Location: ./app/modules/produto/models/Produto_m.php */
