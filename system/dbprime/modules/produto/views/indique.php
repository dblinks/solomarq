<?php
print heading($p->desc_produto, 2);
print '<div id="indique">';
print 'Basta preencher os dados abaixo e enviar pra quem quiser:' . br(2);

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'));
echo form_hidden('id', $p->id_produto);
echo form_hidden('url', str_replace('indique', 'ver', current_url()));

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>' . form_label('Seu nome: ', 'seu_nome');
echo '</div>';
echo '<div class="col_dir left">';
echo form_input(array('name' => 'seu_nome', 'id' => 'seu_nome', 'value' => set_value('seu_nome'), 'class' => '{validar:{required:true, messages:{required:\'Digite seu nome.\'}}}', 'maxlength' => '200', 'size' => '35'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>' . form_label('Seu email: ', 'seu_email');
echo '</div>';
echo '<div class="col_dir left">';
echo form_input(array('name' => 'seu_email', 'id' => 'seu_email', 'value' => set_value('seu_email'), 'class' => '{validar:{required:true,email:true, messages:{required:\'Digite seu email.\',email:\'Digite seu email válido.\'}}}', 'maxlength' => '50', 'size' => '35'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>' . form_label('Nome do amigo: ', 'amigo_nome');
echo '</div>';
echo '<div class="col_dir left">';
echo form_input(array('name' => 'amigo_nome', 'id' => 'amigo_nome', 'value' => set_value('amigo_nome'), 'class' => '{validar:{required:true, messages:{required:\'Digite o nome do amigo.\'}}}', 'maxlength' => '200', 'size' => '35'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>' . form_label('Email do amigo: ', 'amigo_email');
echo '</div>';
echo '<div class="col_dir left">';
echo form_input(array('name' => 'amigo_email', 'id' => 'amigo_email', 'value' => set_value('amigo_email'), 'class' => '{validar:{required:true,email:true, messages:{required:\'Digite o email do amigo.\',email:\'Digite o email válido do amigo.\'}}}', 'maxlength' => '50', 'size' => '35'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>' . form_label('Mensagem: ', 'mensagem');
echo '</div>';
echo '<div class="col_dir left">';
echo form_textarea(array('name' => 'mensagem', 'id' => 'mensagem', 'value' => 'Encontrei este produto em ' . $this->empresa->desc_empresa . ' e achei que você iria gostar. Um abraço!', 'title' => 'Digite sua mensagem.', 'class' => '{validar:{required:true, messages:{required:\'Digite sua mensagem.\'}}}', 'cols' => '45', 'rows' => '5'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">' . nbs() . '</div>';
echo '<div class="col_dir left">';
echo form_input(array('type' => 'image', 'src' => site_url('themes/' . $this->site->tema) . '/img/enviar.png', 'name' => 'enviar', 'class' => 'no'));
echo '</div>';
echo '</div>';

echo form_close();

echo form_fieldset_close();

print '</div>';
?>
