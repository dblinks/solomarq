<div id="esquerda" class="margem-esquerda">
  <?php
  $midias = $this->midia_m->get_midia('produto', $p->id_produto, NULL, 'principal DESC');
  if((is_null($midias) || ($midias->num_rows() == 0)) && (strlen($p->video) == 0))
  {
  ?>
  <br/>
  <?php
  }
  else
  {
  ?>
    <ul class="tabs">
      <?php
      echo (!is_null($midias) && ($midias->num_rows() > 0)) ? li(NULL, anchor('#', 'Fotos')) : NULL;
      echo (strlen($p->video) > 0) ? li(NULL, anchor('#', 'Vídeo')) : NULL;
      ?>
    </ul>
    <div class="panes">
      <?php
      // Fotos
      if(!is_null($midias) && ($midias->num_rows() > 0))
      {
        echo '<div><div id="galleria">';
        foreach($midias->result() as $midia)
        {
          echo anchor($this->config->item('dir_midia').$midia->img_gd, img(array('src' => 'image/get_image_c/'.$midia->img_th.':73:73')));
        }
        echo '</div>';

        echo '<div id="imagens">';
        foreach($midias->result() as $midia)
        {
          echo anchor($this->config->item('dir_midia').$midia->img_gd, '', 'class="galeria" rel="gal" title="'.$midia->desc_midia.'"');
        }
        echo '</div></div>';
      }

      // Vídeo
      if(strlen(trim($p->video)) > 0)
      {
        echo '<div id="video"><iframe width="328" height="246" src="http://www.youtube.com/embed/'.$p->video.'?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>';
      }
      ?>
  </div>
  <?php
  }
  ?>
  <div id="opcoes">
    <div class="interesse left fonte-google"><?php echo anchor(site_url($this->uri->slash_segment(1).'interesse/'.$p->id_produto.'/'.$p->slug), img('img/interesse.png').' Estou interessado', 'class="popup" title="Estou interessado"'); ?></div>
    <div class="indique left fonte-google"><?php echo anchor(site_url($this->uri->slash_segment(1).'indique/'.$p->id_produto.'/'.$p->slug), img('img/indique.png').' Indicar para um amigo', 'class="popup" title="Indicar para um amigo"'); ?></div>
  </div>
</div>
<div id="direita" class="margem-direita">
<?php
  echo heading($p->desc_produto, 2, 'class="fonte-google"');
  echo '<div id="texto">';
  echo $p->texto.br();

  $caracteristicas = $this->produto_m->get_caracteristica($p->id_produto);
  if($caracteristicas->num_rows() > 0)
  {
    echo heading('Características do produto', 3);
    echo ul('class="caracteristicas"');
    foreach($caracteristicas->result() as $c)
      echo li(NULL, $c->caracteristica.': <span class="negrito">'.$c->texto.'</span>');
    echo ul_close();
  }

  if($this->site->produto_valor)
  {
    echo '<div id="box_valor">';
    echo '<div class="esquerda">';
    echo 'Valor:';
    echo '</div>';
    echo '<div class="direita">';
    echo '<span class="valor">';
    echo ($p->valor > 0) ? 'R$ '.moeda($p->valor) : 'Sob consulta';
    echo '</span>'.br();
    echo (strlen($p->parcela > 0)) ? $p->parcela.'x de R$ '.moeda($p->parcela_valor) : NULL;
    echo '</div>';
    echo '</div>';
  }

  echo '</div>';
  ?>
</div>