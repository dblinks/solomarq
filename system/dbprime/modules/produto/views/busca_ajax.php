<?php
if ($produtos->num_rows() > 0) {
    foreach ($produtos->result() as $p) {
        ?>
        <a href="<?php echo site_url('produto/ver/' . $p->id_produto . '/' . $p->slug); ?>">
            <div class="item">
                <?php
                $midia = $this->midia_m->get_midia('produto', $p->id_produto, TRUE);
                $img = (isset($midia->img_me)) ? $midia->img_me : 'nao.jpg';
                echo anchor(site_url('produto/ver/' . $p->id_produto . '/' . $p->slug), img(array('src' => 'image/get_image_c/' . @$img . ':30:30', 'align' => 'left')));
                echo anchor(site_url('produto/ver/' . $p->id_produto . '/' . $p->slug), highlight_phrase($p->desc_produto, $this->input->post('busca')));
                ?>
            </div>
        </a>
        <?php
    }
}
?>
