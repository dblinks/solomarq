<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_produto));

echo li();
echo form_label('Produto: ', 'produto');
echo form_input(array('name' => 'produto', 'id' => 'produto', 'value' => set_value('produto', @$desc_produto), 'title' => 'Digite o nome do produto.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o produto\'}}}', 'maxlength' => '200', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Texto: ', 'texto');
echo form_textarea(array('name' => 'texto', 'id' => 'texto', 'value' => set_value('texto', @$texto), 'title' => 'Digite o texto da notícia.', 'class' => 'dica editor {validar:{required:true, messages:{required:\'Digite o texto\'}}}', 'cols' => '100', 'rows' => '7')).br();
echo li_close();

echo li();
echo form_label('Vídeo: ', 'video');
echo form_input(array('name' => 'video', 'id' => 'video', 'value' => set_value('video', @$video), 'title' => 'Digite somente o código após v=. Ex.: http://www.youtube.com/watch?v=-F_ke3rxopc', 'class' => 'dica', 'maxlength' => '15', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Mostrar: ', 'status');
echo form_dropdown('status', $this->config->item('simnao'), (isset($status) && @$status == 0 ) ? 0 : 1, 'class="dica" title="Deseja mostrar este produto?"');
echo li_close();

echo li();
echo form_label('Destaque: ', 'destaque');
echo form_dropdown('destaque', $this->config->item('simnao'), (@$destaque == 0) ? 0 : 1, 'class="dica" title="Deseja deixar este produto na página inicial?"');
echo li_close();

if($this->site->produto_valor)
{
  echo li();
  echo form_label('Valor: ', 'valor');
  echo form_input(array('name' => 'valor', 'id' => 'valor', 'value' => set_value('valor', @$valor), 'title' => 'Digite o valor do produto.', 'class' => 'dica moeda', 'maxlength' => '15', 'size' => '25')).br();
  echo li_close();

  echo li();
  echo form_label('Número de Parcelas: ', 'parcela');
  echo form_input(array('name' => 'parcela', 'id' => 'parcela', 'value' => set_value('parcela', @$parcela), 'title' => 'Digite o número de parcelas.', 'class' => 'dica numero', 'maxlength' => '15', 'size' => '25')).br();
  echo li_close();

  echo li();
  echo form_label('Valor da Parcela: ', 'parcela_valor');
  echo form_input(array('name' => 'parcela_valor', 'id' => 'parcela_valor', 'value' => set_value('parcela_valor', @$parcela_valor), 'title' => 'Digite o valor da parcela.', 'class' => 'dica moeda', 'maxlength' => '15', 'size' => '25')).br();
  echo li_close();
}
else
{
  echo form_hidden('valor', 0);
  echo form_hidden('parcela', 0);
  echo form_hidden('parcela_valor', 0);
}

if($this->site->produto_servico == 0)
{
  $pro_ser = $this->config->item('pro_ser');
  $pro_ser = array_merge(array(''=>''), $pro_ser);
  unset($pro_ser[0]);
  echo li();
  echo form_label('Produto/Serviço: ', 'tipo');
  echo form_dropdown('tipo', $pro_ser, @$tipo, 'title="Selecione se este é um Serviço ou Produto." class="dica carrega {validar:{required:true, messages:{required:\'Selecione o tipo\'}}}" destino="categoria" modulo="categoria" acao="option_tipo"');
  echo li_close();
}
else
  echo form_hidden('tipo', $this->site->produto_servico);

if($this->site->produto_servico == 0)
{
  $arr_categoria = ($this->_acao == 'alterar')
    ? $this->categoria_m->get_array(array('tipo' => $tipo))
    : array();
}
else
  $arr_categoria = $this->categoria_m->get_array(array('tipo' => $this->site->produto_servico));

echo li();
echo form_label('Categoria: ', 'categoria');
echo form_dropdown('categoria', $arr_categoria, @$id_categoria, 'id="categoria" class="dica {validar:{required:true, messages:{required:\'Selecione a categoria\'}}}" title="Selecione a categoria do produto."');
echo li_close();

echo br(2);

if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>