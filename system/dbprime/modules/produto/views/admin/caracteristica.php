<?php
echo ul(array('class' => 'zebra'));
echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('produto' => @$id_produto, 'id' => $this->uri->segment(5)));
echo li();
echo form_label('Característica: ', 'caracteristica');
echo form_dropdown('caracteristica', $caracteristica = $this->caracteristica_m->get_array(), @$id_caracteristica, 'id="caracteristica" class="dica {validar:{required:true, messages:{required:\'Selecione a característica\'}}}" title="Selecione a característica."');
echo li_close();

echo li();
echo form_label('Texto: ', 'texto');
$texto = ($this->uri->segment(5)) ? $texto : NULL;
echo form_input(array('name' => 'texto', 'id' => 'texto', 'value' => $texto, 'title' => 'Digite o texto da característica.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o texto da característica\'}}}', 'max_length' => '100', 'size' => '25')).br();
echo li_close();
echo br(2);

if($this->uri->segment(5))
{
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
  echo anchor($this->uri->slash_segment(1).$this->uri->slash_segment(2).$this->uri->slash_segment(3).$this->uri->slash_segment(4), img('img/icons/plus-circle.png').' Nova característica');
}
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();

$this->table->set_heading(array('ID', 'Detalhe', 'Texto', 'Opções'));

$qry = $this->caracteristica_m->get_all(NULL, NULL, array($this->produto_m->_id => $id_produto), NULL, 'id_produto', NULL, NULL, NULL, NULL, NULL, 'produto_caracteristica');
foreach($qry->result() as $row)
{
	$this->table->add_row(array(
		$row->id_produto_caracteristica,
		$caracteristica[$row->id_caracteristica],
		$row->texto,
		anchor(admin_url($this->modulo->url.'/caracteristica/'.$row->id_produto.'/'.$row->id_produto_caracteristica), 'alterar', array('class' => 'alterar')).' | '.
		anchor(admin_url($this->modulo->url.'/caracteristica/excluir/'.$row->id_produto_caracteristica), 'excluir', array('desc' => $row->texto, 'class' => 'excluir', 'onclick' => 'excluir();'))
	));
}

$this->table->set_template($this->config->item('tabela_full'));

echo $this->table->generate().br();

?>