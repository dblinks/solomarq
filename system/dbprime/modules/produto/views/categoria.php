<div id="produtos" class="inicial margem">
    <?php
    $quebra = 0;
    if (!is_null($produtos)) {
        foreach ($produtos->result() as $produto) {
            $img = (!isset($produto->img_me)) ? 'naodisponivel.jpg' : $produto->img_me;
            $quebra++;
            echo (($quebra % 5) == 0)
                ? '<div class="produto last">'
                : '<div class="produto">';
            //echo '<div class="produto">';
            echo anchor('produto/ver/' . $produto->id_produto . '/' . $produto->slug, img('image/get_image_c/' . $img . ':140:100'));
            echo anchor('produto/ver/' . $produto->id_produto . '/' . $produto->slug, heading($produto->desc_produto, 3));
            echo '<div class="detalhe">' . anchor('produto/ver/' . $produto->id_produto . '/' . $produto->slug, '+ detalhes') . '</div>';
            echo '</div>';
        }
    }
    ?>
</div>
<!-- #produtos -->
<div class="clear-both"></div>
<?php
$config['base_url'] = site_url($this->uri->slash_segment(1) . $this->uri->slash_segment(2) . $this->uri->slash_segment(3) . $this->uri->slash_segment(4) . 'pg');
$config['total_rows'] = $total;
$config['per_page'] = $this->config->item('produto_por_pagina');
$config['num_links'] = 5;
$config['uri_segment'] = 6;
$config['page_query_string'] = FALSE;

$config['full_tag_open'] = '<ul id="paginacao" class="margem">';
$config['full_tag_close'] = '</ul>';
$config['first_link'] = 'Primeira';
$config['first_tag_open'] = '<li class="primeira">';
$config['first_tag_close'] = '</li>';
$config['last_link'] = 'Última';
$config['last_tag_open'] = '<li class="ultima">';
$config['last_tag_close'] = '</li>';
$config['next_link'] = 'Próxima';
$config['next_tag_open'] = '<li class="proxima">';
$config['next_tag_close'] = '</li>';
$config['prev_link'] = 'Anterior';
$config['prev_tag_open'] = '<li class="anterior">';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="negrito destaque current">';
$config['cur_tag_close'] = '</li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';

$this->pagination->initialize($config);

echo $this->pagination->create_links();
?>
