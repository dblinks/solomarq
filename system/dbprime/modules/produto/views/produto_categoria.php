<div id="tit_produto" class="margem-esquerda left"><?php echo heading($pagina, 1); ?></div>
<div id="tit_categoria" class="left fonte-google"><?php echo heading($categoria, 2); ?></div>
<div class="clear-both"></div>
<div class="sublinhado-maior-titulo margem-esquerda">&nbsp;</div>
<div class="sublinhado-menor-titulo margem-direita">&nbsp;</div>
<div class="midias-sociais">Compartilhe:
<?php
$via = (strlen($this->site->tw_usuario > 0)) ? ' (via @'.$this->config->tw_usuario.')' : '';

echo anchor('http://www.facebook.com/sharer.php?t='.$this->empresa->desc_empresa.' - '.implode(' - ', $titulo).'&u='.current_url(), img('img/facebook.png'), 'target="_blank"');

echo anchor('http://migre.me/compartilhar?msg='.$this->empresa->desc_empresa.' - '.implode(' - ', $titulo).$via.'+'.current_url(), img('img/twitter.png'), 'target="_blank"');

echo anchor('http://promote.orkut.com/preview?nt=orkut.com&tt='.$this->empresa->desc_empresa.' - '.implode(' - ', $titulo).'&du='.current_url(), img('img/orkut.png'), 'target="_blank"');
?>
</div>
<div class="clear-both"></div>