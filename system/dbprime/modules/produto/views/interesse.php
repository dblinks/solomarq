<?php
print heading($p->desc_produto, 2);
print '<div id="indique">';

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'));
echo form_hidden('id', $p->id_produto);

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>'.form_label('Nome: ', 'nome');
echo '</div>';
echo '<div class="col_dir left">';
echo form_input(array('name' => 'nome', 'id' => 'nome', 'value' => set_value('nome'), 'class' => '{validar:{required:true, messages:{required:\'Digite seu nome.\'}}}', 'maxlength' => '200', 'size' => '35'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>'.form_label('Email: ', 'email');
echo '</div>';
echo '<div class="col_dir left">';
echo form_input(array('name' => 'email', 'id' => 'email', 'value' => set_value('email'), 'class' => '{validar:{required:true,email:true, messages:{required:\'Digite seu email.\',email:\'Digite seu email válido.\'}}}', 'maxlength' => '50', 'size' => '35'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>'.form_label('Telefone: ', 'telefone');
echo '</div>';
echo '<div class="col_dir left">';
echo form_input(array('name' => 'telefone', 'id' => 'telefone', 'value' => set_value('telefone'), 'class' => 'telefone {validar:{required:true, messages:{required:\'Digite seu telefone.\'}}}', 'maxlength' => '50', 'size' => '35'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">';
echo '<span class="obrigatorio">*</span>'.form_label('Mensagem: ', 'mensagem');
echo '</div>';
echo '<div class="col_dir left">';
echo form_textarea(array('name' => 'mensagem', 'id' => 'mensagem', 'value' => 'Estou interessado no produto '.$p->desc_produto.'. Obrigado!', 'title' => 'Digite sua mensagem.', 'class' => '{validar:{required:true, messages:{required:\'Digite sua mensagem.\'}}}', 'cols' => '45', 'rows' => '9'));
echo '</div>';
echo '</div>';

echo '<div class="linha">';
echo '<div class="col_esq left">'.nbs().'</div>';
echo '<div class="col_dir left">';
echo form_input(array('type' => 'image', 'src' => site_url('themes/'.$this->site->tema).'/img/enviar.png', 'name' => 'enviar', 'class' => 'no'));
echo '</div>';
echo '</div>';

echo form_close();

echo form_fieldset_close();

print '</div>';
?>
