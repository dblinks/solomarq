<?php
$ih = (!$this->_modulo) ? ' current' : '';
echo li().anchor(admin_url(),'Inicial', array('class' => 'link go no-submenu'.$ih)).li_close();

$ultimo = 0;
$class_last = '';
$secao_total = $this->db->distinct()->get('secao');

$this->db->select('s.desc_secao AS secao, s.url AS secao_url, m.alias AS modulo, m.url AS modulo_url');
$this->db->join('modulo m', 'm.id_secao = s.id_secao', 'LEFT');
$this->db->where_in('m.url', $this->_permissao);
$this->db->where(array('m.menu' => 1, 'm.ativo' => 1, 'm.nivel >=' => $this->usuario->nivel));
$this->db->order_by('s.ordem ASC, s.desc_secao ASC, m.desc_modulo ASC');
$secao = $this->db->get('secao s');

$secao_ = '';
$modulo_ = 0;
foreach($secao->result() as $s)
{
	if($secao_ != $s->secao)
	{
		$ultimo++;
		if($ultimo > 1)
		{
			echo '</ul>'."\n";
      echo li_close()."\n";
			$modulo_ = 0;
		}

		// Último  link
		$class_last = ($secao_total->num_rows() == $ultimo) ? ' last' : '';

		// Seções
		echo (strlen($s->secao_url) > 0)
			? li().anchor(admin_url($s->secao_url), $s->secao, array('class' => 'link go no-submenu'.$class_last))
			: li().anchor(admin_url($s->secao_url), $s->secao, array('class' => 'link'.$class_last));
	}

	if(strlen($s->secao_url) == 0)
	{
		if ($modulo_ == 0) { echo '<ul style="display: block;" class="mod">'; $modulo_ = 1; }
		$current = (($this->_modulo == $s->modulo_url) || (($this->_modulo == 'midia') && ($this->uri->segment(4) == $s->modulo_url))) ? array('class' => 'current') : NULL;
		echo li().anchor(admin_url($s->modulo_url), $s->modulo, $current).li_close();
	}

	$secao_ = $s->secao;
}
?>
