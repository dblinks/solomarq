<?php
echo form_fieldset('');

echo form_open(current_url());

echo form_label('Email: ', 'email');
echo form_input(array('name' => 'email', 'id' => 'email', 'value' => set_value('email', $this->session->userdata('email')), 'class' => 'input corner', 'maxlength' => '200', 'size' => '25'));
echo br();
echo form_button(array('name' => 'button', 'id' => 'entrar', 'class' => 'verde corner', 'type' => 'submit', 'value' => 'recuperar', 'content' => 'Enviar'.nbs().img('img/icons/mail--arrow.png')));
echo br(2);

echo form_close();

echo form_fieldset_close();
?>
<div id="link"><?=anchor(admin_url('login'), 'Efetuar Login')?></div>
