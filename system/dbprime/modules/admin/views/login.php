<?php
echo form_fieldset('');
echo form_open(current_url(), NULL, array('redir' => (!$this->agent->is_referral() || (in_array($this->uri->segment(2), array('login', 'recuperar_senha'))) || $this->uri->segment(1) != 'admin') ? admin_url('') : $this->agent->referrer()));

echo form_label('Login ou Email: ', 'login');
echo form_input(array('name' => 'login', 'id' => 'login', 'value' => set_value('login'), 'class' => 'input corner', 'maxlength' => '200', 'size' => '25'));

echo form_label('Senha: ', 'senha');
echo form_password(array('name' => 'senha', 'id' => 'senha', 'value' => set_value('senha'), 'class' => 'input corner', 'alt' => 'Digite sua senha', 'maxlength' => '20', 'size' => '25'));
echo br();

echo form_button(array('name' => 'button', 'id' => 'entrar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'entrar', 'content' => 'Entrar'.nbs().img('img/icons/door-open-in.png')));
echo br();

echo form_close();

echo form_fieldset_close();
?>
<div id="link"><?=anchor(admin_url('recuperar_senha'), 'Recuperar Senha')?></div>
