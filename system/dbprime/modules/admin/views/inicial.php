<div class="box_inicial left col25 ">
  <?php
  // Atalhos rápidos
  echo '<div class="cabecalho">' . heading('Atalhos rápidos', 1) . '</div>';
  echo ul(array('class' => 'list'));
  foreach ($atalhos->result() as $a)
    echo li(NULL, anchor(admin_url($a->url), $a->desc_modulo));
  echo ul_close();
  ?>
</div>
<?php
// Google Analytics
if ($this->site->ga_email && $this->site->ga_senha && $this->site->ga_analytics && $this->site->ga_perfil)
  {
?>
<div class="box_inicial left col50">
<?php
    echo '<div class="cabecalho">' . heading('Google Analytics', 1) . '</div>';

    $this->load->library('CiGAnalytics');
    $this->load->library('CiGChart');

    function dia ($data)
    {
      return substr($data, 6, 2) . '-' . substr($data, 4, 2);
    }

    try
    {
      // Tempo
      $dias = 7;
      $inicial = date("Y-m-d", $_SERVER['REQUEST_TIME'] - $dias * 60 * 60 * 24); // 7 dias, 60 seg, 60 min, 24 hr;
      $hoje = date("Y-m-d");

      $ga = new CiGAnalytics();
      $ga->login($this->site->ga_email, base64_decode($this->site->ga_senha));
      $ga->setProfile('ga:'.$this->site->ga_perfil);

      // Setting date range with pre-defined variables
      $ga->setDateRange($inicial, $hoje);

      // Generating report arrays for the date range, consisting of pageviews and visits
      $report = $ga->getReport(
        array('dimensions' => urlencode('ga:date'),
          'metrics' => urlencode('ga:pageviews,ga:visits,ga:newVisits'),
        )
      );
      // page views
      $pviews = array();
      foreach ($report as $dimension => $metrics)
      {
        array_push($pviews, $metrics['ga:pageviews']);
      }
      // visits
      $visits = array();
      foreach ($report as $dimension => $metrics)
      {
        array_push($visits, $metrics['ga:visits']);
      }
      // new visits
      $nvisits = array();
      foreach ($report as $dimension => $metrics)
      {
        array_push($nvisits, $metrics['ga:newVisits']);
      }

      // Extract various dates from the report array keys in order to use them as variables for x-axis labels
      $days = array_keys($report);
      $days = array_map("dia", $days);
      $days[(count($days) - 1)] = 'Hoje';
    }
    catch (Exception $e)
    {
      print 'Error: ' . $e->getMessage();
    }

    // Get the keys for max. values of page views and visits
    function max_key ($array)
    {
      foreach ($array as $key => $val)
      {
        if ($val == max($array))
          return $key;
      }
    }

    $array = $pviews;
    $precord = max_key($array);
    $array = $visits;
    $vrecord = max_key($array);
    $array = $nvisits;
    $nvrecord = max_key($array);

    // Always use max. value recorded in array for y-axis
    $ymax = 1 * (max($pviews));
    // Devide it by six and round up to nearest whole number to set appropriate y-axis ticks
    $ytick = ceil((max($pviews)) / 6);

    // Chart settings
    $traffic = new CiGChart();
    $traffic->type = 'lc';
    $traffic->SetImageSize(500, 210);
    $traffic->SetChartMargins(30, 30, 30, 30);
    $traffic->SetEncode('simple');
    $traffic->AddData($visits);
    $traffic->AddData($pviews);
    $traffic->AddData($nvisits);
    $traffic->AddChartColor('FF9900');
    $traffic->AddChartColor('0077CC');
    $traffic->AddChartColor('33CC33');
    $traffic->AddLineStyle(1);
    $traffic->AddLineStyle(1);
    $traffic->AddLineStyle(1);
    $traffic->AddDataLineStyle('0077CC', 1, '0:30', 1);
    $traffic->AddFillArea('a', 'FF99007F', 0);
    $traffic->AddFillArea('b', 'E6F2FA7F', 0, 1);
    $traffic->AddFillArea('c', '33CC337F', 0, 1);
    $traffic->AddAxis('y,x');
    $traffic->AddAxisRange(0, $ymax, $ytick);
    $traffic->AddAxisLabel($days, 1);
    $traffic->SetGrid(round(100 / $dias, 2), 0, 1, 3);
    $traffic->AddAxisStyle('0000DD', 1, 0);
    $traffic->AddAxisTickLength(-500, 0);
    $traffic->SetTitle('Acesso ao site nos últimos ' . $dias . ' dias.', 'FF0000');
    $traffic->AddLegend('Visitas');
    $traffic->AddLegend('Visualização de página');
    $traffic->AddLegend('Novos visitantes');
    $traffic->SetLegendPosition('b');
    $traffic->AddDataPointLabel('f', ($visits[$dias]), '000000', 0, 30, 12);
    $traffic->AddDataPointLabel('f', ($pviews[$dias]), '000000', 1, 30, 12);
    $traffic->AddDataPointLabel('f', ($nvisits[$dias]), '000000', 2, 30, 12);
    $traffic->AddDataPointLabel('t', (max($visits)) . ' max', '900C0C', 0, $vrecord, 12);
    $traffic->AddDataPointLabel('t', (max($pviews)) . ' max', '900C0C', 1, $precord, 12);
    $traffic->AddDataPointLabel('t', (max($nvisits)) . ' max', '900C0C', 2, $nvrecord, 12);
    // Generate chart URL
    echo $traffic->GetImg();
  ?>
</div>
<?php
}
// RSS
if($this->site->admin_feed)
{
?>
<div class="box_inicial left col25">
<div class="cabecalho"><h1>Notícias</h1></div>
<div id="rss" url="<?php echo $this->site->admin_feed; ?>"></div>
</div>
<?php
}
?>
