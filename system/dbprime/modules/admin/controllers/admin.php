<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('usuario/usuario_m');
	}

	public function index()
	{
    if (is_dir('./instalador'))
		{
      $this->template->write('msg', msg('Por favor, remova o diretório de instalação de seu servidor. Deixá-lo online significa permitir o controle do seu site para qualquer outra pessoa.', 'erro'));
		}

    // Feed Notícias
    if($this->site->admin_feed)
    {
      $this->template->add_css('css/admin/jquery.zrssfeed.css');
      $this->template->add_js('js/admin/jquery.zrssfeed.min.js');
    }

    $secao_conteudo = $this->secao_m->get_field('id_secao', 'conteudo', 'slug');
    $atalhos = $this->modulo_m->get_all(NULL, NULL, array('id_secao' => $secao_conteudo, 'menu' => 1));

    $this->template->write_view('conteudo', 'inicial', array('atalhos' => $atalhos));

		$this->template->render();
	}

	public function login()
	{
		$this->template->set_template('login');

		if($this->input->post())
		{
			$this->form_validation->set_rules('login', 'Login', 'required|trim|unique|min_length[6]|max_length[100]');
			$this->form_validation->set_rules('senha', 'Senha', 'required|trim|min_length[6]|max_length[20]');

			if($this->form_validation->run() == FALSE)
			{
				(stristr($this->input->post('login'), '@')) ? $this->session->set_userdata(array('email' => $this->input->post('login'))) : NULL;
				$this->template->write('msg', msg(validation_errors(), 'erro corner'));
			}
			else
			{
				$dado = array();
				(stristr($this->input->post('login'), '@')) ? $dado['email'] = $this->input->post('login') : $dado['login'] = $this->input->post('login');
        $dado['senha'] = $this->input->post('senha');

				if ($this->usuario_m->login($dado))
				{
					$this->template->write('msg', msg('Login efetuado com sucesso!', 'sucesso corner'));
					redirect($this->input->post('redir'));
				}
				else
				{
					$this->template->write('msg', msg('Erro ao efetuar Login!', 'erro corner'));
				}
			}
		}

		$this->template->write_view('form', 'admin/login');
		$this->template->render();
	}

	public function recuperar_senha()
	{
		//if($this->logado())
			//redirect('admin/login');

		$this->template->set_template('login');

		if($this->input->post('button'))
		{
			$this->form_validation->set_rules('email', 'Email', 'required|trim|email|unique|max_length[100]');
			if($this->form_validation->run() == FALSE)
			{
				$this->template->write('msg', msg(validation_errors(), 'erro corner'));
			}
			else
			{
				$u = new Usuario_m();
				$u->email = $this->input->post('email');

				if( $u->recuperar_senha() )
				{
					// @TODO - enviar senha por email
					$this->template->write('msg', msg('Foi enviado um email com a nova senha para: '.br().$this->input->post('email'), 'sucesso corner'));
				}
				else
				{
					$this->template->write('msg', msg($u->error->string, 'erro corner'));
				}
			}
		}

		$this->template->write_view('form', 'admin/recuperar_senha');
		$this->template->render();
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin/login');
	}

	public function habilitar_js()
	{
		$this->template->set_template('no');
		$this->template->write_view('conteudo', 'habilitar_js');
		$this->template->render();
	}

	public function ajax_slug()
	{
		$this->output->enable_profiler(FALSE);
		echo slug($this->input->post('string'));
	}

	public function logado()
	{
		return (($this->session->userdata('logado') == 1) && ($this->session->userdata('tipo') == 'admin')) ? TRUE : FALSE;
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */