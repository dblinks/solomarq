<?php
echo heading(img('img/dblinks16.png').nbs().$this->config->item('dev_desc'), 1);

echo ul(array('class'=> 'zebra'));

echo li(array('class' => 'titulo'));
echo heading('dbEmpresa', 1, array('class' => 'titulo'));
echo li_close();
echo li(NULL, 'Versão: '.DBE_VERSION);
echo li(NULL, 'Revisão: '.DBE_REV);
echo li(NULL, 'Estágio: '.DBE_STA);
echo li(NULL, 'CodeIgniter: '.CI_VERSION.' '.ver_versao(pegar_pagina('http://versions.ellislab.com/codeigniter_version.txt'), CI_VERSION));
//echo li(NULL, 'Desenvolvedor: '.$this->config->item('dev_dev'));
echo li(NULL, 'E-mail: '.$this->config->item('dev_email'));
echo li(NULL, 'Site: '.anchor($this->config->item('dev_url'), $this->config->item('dev_url'), array('target' => '_blank')));
echo li(NULL, 'Desenvolvido no Brasil '.img('img/icons_pais/br.png'));
echo br(2);

echo li(array('class' => 'titulo'));
echo heading(' Sua configuração', 1, array('class' => 'titulo'));
echo li_close();
echo li(NULL, 'Navegador: '.$this->agent->browser().nbs().'(Versão: '.$this->agent->version().')');
echo li(NULL, 'Sistema operacional: '.$this->agent->platform());
echo li(NULL, 'Resolução: <span id="resolucao"></span> (Área visível: <span id="area_visivel"></span>)');
echo li(NULL, 'Última url: '.$this->agent->referrer());
echo li(NULL, 'Navegador (Completo): '.$this->agent->agent_string());
echo br(2);

if($this->usuario->nivel == 1)
{
	echo li(array('class' => 'titulo'));
	echo heading('Servidor', 1, array('class' => 'titulo'));
	echo li_close();
	echo li(NULL, 'Banco de dados: '.ucfirst($this->db->platform()));
	echo li(NULL, 'Versão do banco de dados: '.$this->db->version());
	echo li(NULL, 'Versão do PHP: '.phpversion());
	echo li(NULL, 'Biblioteca GD: '.check_gd());
	echo br(2);
}

echo li(array('class' => 'titulo'));
echo heading(' Esta página', 1, array('class' => 'titulo'));
echo li_close();
echo li(NULL, 'Tempo de carregamento: {elapsed_time}s');
echo li(NULL, 'Uso de memória: {memory_usage}');
echo li(NULL, 'ID da Sessão: '.$this->session->userdata('session_id'));
echo li(NULL, 'Seu IP: '.$this->session->userdata('ip_address'));
echo li(NULL, 'Última atividade: '.$this->session->userdata('last_activity').' - '.standard_date('DATE_RFC822', $this->session->userdata('last_activity')));
$hv = ((bool)date('I')) ? ' (Horário de Verão)' : NULL;
echo li(NULL, 'Data/Hora: '.date('d  \d\e F \d\e Y').nbs().'/'.nbs().date('H:i').$hv);

echo br(2);

echo li(array('class' => 'titulo'));
echo heading(' Créditos', 1, array('class' => 'titulo'));
echo li_close();
echo li(NULL, 'Ícones:'.nbs().anchor('http://p.yusukekamiyamane.com/', 'Fugue Icons (Yusuke Kamiyamane)', 'target="_blank"'));

echo ul_close();
?>