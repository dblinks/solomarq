<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Informacao - Controller
 *
 * @package 		Admin
 * @subpackage 	Informação
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{
	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		//$this->load->model('config/config_m');
		//$this->form_validation->set_rules($this->validation_rules);
	}

	/**
	 * Página principal - index
	 *
	 * @access	public
	 */
	public function index()
	{
		$this->template->write_view('conteudo', 'admin/info');
		$this->template->add_js('js/admin/informacao.js');

		$this->template->render();
	}
}


/* End of file informacao.php */
/* Location: ./app/modules/informacao/controllers/informacao.php */