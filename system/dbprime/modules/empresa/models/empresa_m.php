<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Empresa_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Config
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Empresa_m extends DBModel {

	var $_tabela = 'empresa';
	var $_id = 'id_empresa';
	var $_desc = 'desc_empresa';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_empresa ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
	}

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		$upload['upload_path'] = $this->config->item('dir_midia');
		$upload['allowed_types'] = 'jpg|jpeg|png|gif';
		$upload['file_name'] = 'logotipo';
		$upload['overwrite'] = TRUE;
		$upload['remove_spaces'] = TRUE;
		$this->upload->initialize($upload);

		if($this->upload->do_upload('logotipo'))
		{
			$up = $this->upload->data();
			chmod($up['full_path'], 0755);
			$logotipo_atual = $this->empresa_m->get_field('logotipo', 1);
			($logotipo_atual != $upload['file_name'].$up['file_ext'] && file_exists($upload['upload_path'].$logotipo_atual)) ? unlink($this->config->item('dir_midia').$logotipo_atual) : NULL;
			$logotipo = $upload['file_name'].$up['file_ext'];
		}
		else
			$logotipo = $this->empresa_m->get_field('logotipo', 1);

		return parent::update(array(
			$this->_desc	=> $dado['empresa'],
			'id_municipio'	=> $dado['municipio'],
			'logotipo' => $logotipo,
			'slogan'	=> $dado['slogan'],
			'ramo'	=> $dado['ramo'],
			'palavras_chave'	=> $dado['palavras_chave'],
			'endereco'	=> $dado['endereco'],
			'complemento'	=> $dado['complemento'],
			'numero'	=> $dado['numero'],
			'bairro'	=> $dado['bairro'],
			'cep'	=> $dado['cep'],
			'gmaps'	=> @$dado['gmaps'],
			'email'	=> $dado['email'],
      'site'	=> $dado['site'],
      'telefone'	=> $dado['telefone'],
		), $dado['id']);
	}
}

/* End of file Empresa_m.php */
/* Location: ./app/modules/config/models/Empresa_m.php */
