<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends Site_Controller {

	function __construct()
	{
		parent::__construct();
    $this->load->model('pagina/pagina_m');

    //CSS
    $this->template->add_css($this->tema_url('css/noticia.css'));
	}

	public function index()
	{
    // Título
    $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');
    $this->_titulo[] = $p->desc_menu;

    $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($p->desc_menu), 'pagina' => $p->desc_menu));

    $pagina = $this->pagina_m->get_all(NULL, NULL, array('status' => 1), NULL, NULL, NULL, 'ordem ASC');
    if($pagina->num_rows() == 1)
    {
      foreach($pagina->result() as $p) {}
      $pg = $this->pagina_m->get_id($p->id_pagina);
      $this->template->write_view('conteudo', 'index', $pg);
    }
    else
    {
      $p = $pagina->row(0);
      //foreach($pagina->result() as $p) {}
      $pg = $this->pagina_m->get_id($p->id_pagina);
      $this->template->write_view('conteudo', 'index_esquerda', array('paginas' => $pagina));
      $this->template->write_view('conteudo', 'index_direita', $pg);
    }

    // Título
    $this->_titulo[] = $pg->{$this->pagina_m->_desc};
    $this->template->write_view('titulo', 'comum/title');

		$this->template->render();
  }

  public function ver()
  {
    // Título
    $p = $this->menu_m->get_id($this->uri->segment(1), 'slug');

    $this->template->write_view('conteudo', 'comum/titulo', array('titulo' => array($p->desc_menu), 'pagina' => $p->desc_menu));

    $paginas = $this->pagina_m->get_all(NULL, NULL, array('status' => 1), NULL, NULL, NULL, 'ordem ASC');

    $pg = $this->pagina_m->get_id($this->uri->segment(3));

    // Título
    $this->_titulo[] = $p->desc_menu;
    $this->_titulo[] = $pg->{$this->pagina_m->_desc};
    $this->template->write_view('titulo', 'comum/title');

    $this->template->write_view('conteudo', 'index_esquerda', array('paginas' => $paginas));
    $this->template->write_view('conteudo', 'index_direita', $pg);

    $this->template->render();
  }
}

/* End of file empresa.php */
/* Location: ./application/controllers/site.php */