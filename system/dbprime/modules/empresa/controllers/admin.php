<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Empresa - Controller
 *
 * @package 		Admin
 * @subpackage 	Empresa
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{

	private $validation_rules = array(
		array(
			'field'   => 'empresa',
			'label'   => 'empresa',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'ramo',
			'label'   => 'Ramo de atividade',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'palavras_chave',
			'label'   => 'Palavras-chave',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'endereco',
			'label'   => 'Endereço',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'numero',
			'label'   => 'Número',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'estado',
			'label'   => 'Estado',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'municipio',
			'label'   => 'Município',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'cep',
			'label'   => 'CEP',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'email',
			'label'   => 'Email',
			'rules'   => 'required|trim|valid_email'
		)
	);

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('empresa/empresa_m');
		$this->load->model('estado/estado_m');
		$this->load->model('municipio/municipio_m');
	}

	/**
	 * Página principal - index
	 *
	 * @access	public
	 */
	public function index()
	{
		$a = $this->empresa_m->get_id(1);
		$this->template->write('conteudo', heading($this->modulo->desc_modulo.' : '.$a->{$this->empresa_m->_desc}));
		$this->template->write_view('conteudo', 'admin/form', $a);

		// Dados recebidos
		if($_POST)
		{
			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				$this->empresa_m->update($this->input->post())
					? $this->session->set_flashdata('msg', msg($this->modulo->desc_modulo.' alterada com sucesso', 'sucesso'))
					: $this->session->set_flashdata('msg', msg('Erro ao alterar '.$this->modulo->desc_modulo, 'erro'));
					redirect($this->agent->referrer());
			}
			else
			{
				$this->session->set_flashdata('msg', msg(validation_errors(), 'erro'));
				redirect($this->agent->referrer());
			}

			redirect(admin_url($this->modulo->url));
		}

		$this->template->render();
	}
}

/* End of file empresa.php */
/* Location: ./app/modules/empresa/controllers/empresa.php */