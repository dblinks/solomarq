<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open_multipart(current_url(), array('class' => 'form validar'), array('id' => 1));

	echo li(array('class' => 'titulo'));
	echo heading('Dados da Empresa', 1, array('class' => 'titulo'));
	echo li_close();

	echo li();
	echo form_label('Empresa: ', 'empresa');
	echo form_input(array('name' => 'empresa', 'id' => 'empresa', 'value' => set_value('empresa', @$desc_empresa), 'title' => 'Digite o nome da empresa.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o nome da empresa.\'}}}', 'maxlength' => '200', 'size' => '75')).br();
	echo li_close();

	echo li();
	echo form_label('Logotipo: ', 'logotipo');
	echo form_upload(array('name' => 'logotipo', 'id' => 'logotipo', 'class' => 'dica', 'title' => 'Selecione o arquivo de logotipo (PNG, JPG, GIF).'));
	echo ($this->empresa->logotipo)
		? br().img($this->config->item('dir_midia').$this->empresa->logotipo)
		: msg('Envie o logotipo em versão horizontal, com largura máxima de 200px e fundo transparente (PNG).', 'corner nota ajuda', FALSE, FALSE, FALSE);
	echo li_close();

	echo li();
	echo form_label('Slogan: ', 'slogan');
	echo form_input(array('name' => 'slogan', 'id' => 'slogan', 'value' => set_value('slogan', @$slogan), 'class' => 'dica', 'title' => 'Digite o slogan.', 'maxlength' => '200', 'size' => '75')).br();
	echo li_close();

	echo li();
	echo form_label('Ramo de atividade: ', 'ramo');
	echo form_input(array('name' => 'ramo', 'id' => 'ramo', 'value' => set_value('ramo', @$ramo), 'title' => 'Digite o ramo de atividade.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o ramo de atividade da empresa.\'}}}', 'maxlength' => '200', 'size' => '75')).br();
	echo li_close();

	echo li();
	echo form_label('Palavras-chave: ', 'palavras_chave');
	echo form_textarea(array('name' => 'palavras_chave', 'id' => 'palavras_chave', 'value' => set_value('palavras_chave', @$palavras_chave), 'title' => 'Digite algumas palavras-chave para o site, no máximo 15, separadas por , (vírgula).', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite algumas Palavras-chave para seu site.\'}}}', 'cols' => '77', 'rows' => '2')).br();
	echo li_close();

	echo li(array('class' => 'titulo'));
	echo heading('Endereço', 1, array('class' => 'titulo'));
	echo li_close();

	echo li();
	echo form_label('Endereço: ', 'endereco');
	echo form_input(array('name' => 'endereco', 'id' => 'endereco', 'value' => set_value('endereco', @$endereco), 'title' => 'Digite o endereço, visualizado no site.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o endereço.\'}}}', 'maxlength' => '255', 'size' => '75')).br();
	echo li_close();

	echo li();
	echo form_label('Complemento: ', 'complemento');
	echo form_input(array('name' => 'complemento', 'id' => 'complemento', 'value' => set_value('complemento', @$complemento), 'title' => 'Digite o complemento, visualizado no site.', 'class' => 'dica', 'maxlength' => '255', 'size' => '75')).br();
	echo li_close();

	echo li();
	echo form_label('Número: ', 'numero');
	echo form_input(array('name' => 'numero', 'id' => 'numero', 'value' => set_value('numero', @$numero), 'title' => 'Digite o número, visualizado no site.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o número.\'}}}', 'maxlength' => '5', 'size' => '25')).br();
	echo li_close();

	echo li();
	echo form_label('Bairro: ', 'bairro');
	echo form_input(array('name' => 'bairro', 'id' => 'bairro', 'value' => set_value('bairro', @$bairro), 'title' => 'Digite o bairro, visualizado no site.', 'class' => 'dica', 'maxlength' => '255', 'size' => '50')).br();
	echo li_close();

	echo li();
	$m = $this->municipio_m->get_id($id_municipio);
	echo form_label('Estado: ', 'estado');
	echo form_dropdown('estado', $this->estado_m->get_array(), @$m->id_estado, 'class="carrega dica {validar:{required:true, messages:{required:\'Selecione o estado\'}}}" destino="municipio" modulo="municipio" acao="option" title="Selecione o estado"');
	echo li_close();

	echo li();
	$arr_mun = $this->municipio_m->get_array(array('id_estado' => $m->id_estado));
	echo form_label('Município: ', 'municipio');
	echo form_dropdown('municipio', $arr_mun, @$id_municipio, 'id="municipio" class="dica {validar:{required:true, messages:{required:\'Selecione o município\'}}}" title="Selecione o município"');
	echo li_close();

	echo li();
	echo form_label('CEP: ', 'cep');
	echo form_input(array('name' => 'cep', 'id' => 'cep', 'value' => set_value('cep', @$cep), 'title' => 'Digite o bairro, visualizado no site.', 'class' => 'cep dica {validar:{required:true, messages:{required:\'Digite o CEP.\'}}}', 'maxlength' => '9', 'size' => '10')).br();
	echo li_close();
/*
	echo li();
	echo form_label('URL Mapa Google Maps: ', 'gmaps');
	echo form_textarea(array('name' => 'gmaps', 'id' => 'gmaps', 'value' => set_value('g_maps', @$g_maps), 'class' => 'input', 'cols' => '77', 'rows' => '2')).br();
	echo li_close();
*/
	echo li(array('class' => 'titulo'));
	echo heading('Informações de contato', 1, array('class' => 'titulo'));
	echo li_close();

	echo li();
	echo form_label('Telefone: ', 'telefone');
	echo form_input(array('name' => 'telefone', 'id' => 'telefone', 'value' => set_value('telefone', @$telefone), 'title' => 'Digite o telefone, visualizado no site.', 'class' => 'dica telefone {validar:{required:true, messages:{required:\'Digite o Telefone.\'}}}', 'maxlength' => '14', 'size' => '20')).br();
	echo li_close();

  echo li();
	echo form_label('Email: ', 'email');
	echo form_input(array('name' => 'email', 'id' => 'email', 'value' => set_value('email', @$email), 'title' => 'Digite o email, visualizado no site e para ser enviado via formulário de contato no site.', 'class' => 'dica {validar:{required:true,email:true, messages:{required:\'Digite o Email.\',email:\'Digite um email válido.\'}}}', 'maxlength' => '50', 'size' => '50')).br();
	echo li_close();

	echo li();
	echo form_label('Site: ', 'site');
	echo form_input(array('name' => 'site', 'id' => 'site', 'value' => set_value('site', isset($site) ? $site : site_url()), 'title' => 'Digite a URL do site, visualizado no site.', 'class' => 'dica {validar:{required:true, messages:{required:\'Digite o Site.\'}}}', 'maxlength' => '50', 'size' => '50')).br();
	echo li_close();

echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>