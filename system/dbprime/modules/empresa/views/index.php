<div id="texto" class="margem">
<?php
  // Imagem principal
  $principal = $this->midia_m->get_midia('pagina', $id_pagina, TRUE);
  if(!is_null($principal))
  {
    echo '<div id="principal">';
    echo anchor($this->config->item('dir_midia').$principal->img_gd, img(array('src' => site_url('image/get_image_c/'.$principal->img_me.':270:185'), 'align' => 'left')), 'title="'.$principal->desc_midia.'" class="galeria" rel="gal"');
    echo '<div id="lupa">';
    echo anchor($this->config->item('dir_midia').$principal->img_gd, img('img/lupa.png'), 'title="'.$principal->desc_midia.'" class="galeria" rel="gal"');
    echo '</div>';
    if(isset($principal->desc_midia))
      echo '<div id="desc_midia">'.$principal->desc_midia.'</div>';
    echo '</div>';
  }

  echo $texto;

  // Imagens
  $imagens = $this->midia_m->get_midia('pagina', $id_pagina, FALSE);
  if(!is_null($imagens) && ($imagens->num_rows() > 0))
  {
    echo br(2).heading('Imagens', 1, 'class="imagens"');
    echo '<div class="imagens">';
    foreach($imagens->result() as $img)
    {
      echo anchor($this->config->item('dir_midia').$img->img_gd, img(array('src' => site_url('image/get_image_c/'.$img->img_me.':100:75'), 'align' => 'left')), 'title="'.$img->desc_midia.'" class="galeria" rel="gal"');
    }
    echo '</div>';
  }
?>
</div>
