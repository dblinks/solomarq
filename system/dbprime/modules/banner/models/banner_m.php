<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Banner_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Parceiro
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Banner_m extends DBModel {

	var $_tabela = 'banner';
	var $_id = 'id_banner';
	var $_desc = 'desc_banner';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_banner ASC';

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Salva registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		$dir = $this->config->item('dir_midia');//.$this->_tabela.'/';
		//if(!is_dir($dir)) { @mkdir($dir); @chmod($dir, 0777);  }

		$upload['upload_path'] = $dir;
		$upload['allowed_types'] = 'jpg|jpeg|png|gif';
		$upload['overwrite'] = TRUE;
		$upload['remove_spaces'] = TRUE;
		$this->upload->initialize($upload);

		if(!$this->upload->do_upload('imagem'))
		{
			$this->session->set_flashdata('msgaux', msg($this->upload->display_errors(), 'erro'));
			$imagem = NULL;
		}
		else
		{
			$up = $this->upload->data();
			chmod($up['full_path'], 0777);
			$imagem = '_'.$this->modulo->url.'_'.date('Ymd_His').'_'.time().$up['file_ext'];

			rename($dir.$up['file_name'], $dir.$imagem);
		}

		return parent::insert(array(
			$this->_desc	=> $dado['banner'],
			'imagem'	=> $imagem,
			'link'	=> prep_url($dado['link']),
      'legenda'	=> $dado['legenda'],
			'status'	=> $dado['status'],
			'slug'	=> slug($dado['banner'])
		));
	}

	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
    $dir = $this->config->item('dir_midia');//.$this->_tabela.'/';
		//if(!is_dir($dir)) { @mkdir($dir); @chmod($dir, 0777); }

		$upload['upload_path'] = $dir;
		$upload['allowed_types'] = 'jpg|jpeg|png|gif';
		$upload['overwrite'] = TRUE;
		$upload['remove_spaces'] = TRUE;
		$this->upload->initialize($upload);

		if(!$this->upload->do_upload('imagem'))
		{
			$imagem = $this->banner_m->get_field('imagem', $dado['id']);
		}
		else
		{
			$up = $this->upload->data();
			chmod($up['full_path'], 0755);
			$imagem = '_'.$this->modulo->url.'_'.date('Ymd_His').'_'.time().$up['file_ext'];

			rename($dir.$up['file_name'], $dir.$imagem);

			$imagem_atual = $this->banner_m->get_field('imagem', $dado['id']);
			@unlink($dir.$imagem_atual);
		}

		//@unlink($upload['upload_path'].$up['file_name']);

		return parent::update(array(
			$this->_desc	=> $dado['banner'],
			'imagem'	=> $imagem,
			'link'	=> prep_url($dado['link']),
      'legenda'	=> $dado['legenda'],
			'status'	=> $dado['status'],
			'slug'	=> slug($dado['banner'])
		), $dado['id']);
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		//$imagem_atual = $this->banner_m->get_field('imagem', $id);
		//@unlink($imagem_atual);
		return parent::delete($id);
	}
}

/* End of file Banner_m.php */
/* Location: ./app/modules/banner/models/banner.php */
