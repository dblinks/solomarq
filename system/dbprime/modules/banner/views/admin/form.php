<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open_multipart(current_url(), array('class' => 'form validar'), array('id' => @$id_banner));

echo li();
echo form_label('Descrição: ', 'banner');
echo form_input(array('name' => 'banner', 'id' => 'banner', 'value' => set_value('banner', @$desc_banner), 'title' => 'Digite a legenda do banner.', 'class' => 'dica', 'maxlength' => '200', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Imagem: ', 'imagem');
echo form_upload(array('name' => 'imagem', 'id' => 'imagem', 'title' => 'Selecione o arquivo de imagem.', 'class' => 'dica'));
echo (isset($imagem) > 0)
	? br().img(array('src' => $this->config->item('dir_midia').$imagem, 'width' => '300px')) : NULL;
echo msg('Envie uma imagem com o tamanho: 740px x 235px (largura x altura).', 'corner nota ajuda', FALSE, FALSE, FALSE);
echo li_close();

echo li();
echo form_label('Link: ', 'link');
echo form_input(array('name' => 'link', 'id' => 'link', 'value' => set_value('link', @$link), 'title' => 'Digite o link para redirecionar o usuário quando clicar no banner.', 'class' => 'dica', 'maxlength' => '200', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Legenda: ', 'legenda');
echo form_input(array('name' => 'legenda', 'id' => 'legenda', 'value' => set_value('legenda', @$legenda), 'title' => 'Digite uma legenda para o banner.', 'class' => 'dica', 'maxlength' => '255', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Mostrar: ', 'status');
echo form_dropdown('status', $this->config->item('simnao'), (isset($status) && @$status == 0 ) ? 0 : 1, 'title="Este banner deve ser exibido?" class="dica"');
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>