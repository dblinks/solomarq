<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_grupo));

echo li();
echo form_label('Grupo: ', 'grupo');
echo form_input(array('name' => 'grupo', 'id' => 'grupo', 'value' => set_value('grupo', @$desc_grupo), 'title' => 'Digite o nome do grupo.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite o grupo\'}}}', 'maxlength' => '50', 'size' => '25')).br();
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>