<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Usuário - Controller
 *
 * @package 		Admin
 * @subpackage 	Usuário
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */

class Admin extends Admin_Controller
{

	private $validation_rules = array(
		array(
			'field'   => 'nome',
			'label'   => 'Nome',
			'rules'   => 'required|trim|alpha'
		),
		array(
			'field'   => 'email',
			'label'   => 'Email',
			'rules'   => 'trim|email'
		),
		array(
			'field'   => 'login',
			'label'   => 'login',
			'rules'   => 'required|trim|min_lenght[6]'
		),
		array(
			'field'   => 'dica_senha',
			'label'   => 'Dica de Senha',
			'rules'   => 'required|trim'
		),
		array(
			'field'   => 'grupo',
			'label'   => 'Grupo',
			'rules'   => 'required'
		)
	);

	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('usuario/usuario_m');
		$this->load->model('grupo/grupo_m');
	}

	/**
	 * Página principal - index
	 *
	 * @access	public
	 */
	public function index()
	{
		$this->template->write('conteudo', heading('Listando '.$this->modulo->alias, 1));
		$this->template->write_view('conteudo', 'admin/listar');

		$this->template->render();
	}

	/**
	 * Adiciona registro com validação do formulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function adicionar()
	{
		$this->template->write('conteudo', heading('Adicionar '.$this->modulo->alias));
		$this->template->write_view('conteudo', 'admin/form');

		// Dados recebidos
		// Dados recebidos
		if ($_POST)
		{
      $_POST['alterar_senha'] = (isset($_POST['alterar_senha'])) ? $_POST['alterar_senha'] : 0;

      $this->validation_rules[] = array(
				'field'   => 'senha',
				'label'   => 'Senha',
				'rules'   => 'required|trim|min_lenght[6]'
			);

			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				if($this->usuario_m->insert($this->input->post()))
				{
					$this->session->set_flashdata('msg', msg('Sucesso ao adicionar '.$this->modulo->alias, 'sucesso'));
					$this->session->set_flashdata('msgaux', msg('Para o novo usuário acessar a Administração é necessário dar permissão aos módulos na opção "Permissão" na linha do Novo Usuário.', 'ajuda'));
					redirect(admin_url($this->modulo->url));
				}
				else
				{
					$this->template->write('msg', msg('Erro ao adicionar '.$this->modulo->alias, 'erro'));
				}
			}
			else
			{
				$this->template->write('msg', msg(validation_errors(), 'erro'));
			}
		}

		$this->template->render();
	}

	/**
	 * Altera registro com validação de fomulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function alterar()
	{
		$a = $this->usuario_m->get_id($this->uri->segment(4));
		$this->template->write('conteudo', heading('Alterar '.$this->modulo->alias.' : '.$a->desc_usuario));
		$this->template->write_view('conteudo', 'admin/form', $a);

		// Dados recebidos
		if($_POST)
		{
      $_POST['alterar_senha'] = (isset($_POST['alterar_senha'])) ? $_POST['alterar_senha'] : 0;
			if($this->input->post('alterar_senha') == 1)
			{
				$this->validation_rules[] = array(
					'field'   => 'senha',
					'label'   => 'Senha',
					'rules'   => 'required|trim|min_lenght[6]'
				);
				$this->validation_rules[] = array(
					'field'   => 'senhan',
					'label'   => 'Nova Senha',
					'rules'   => 'required|trim|min_lenght[6]'
				);
				$this->validation_rules[] = array(
					'field'   => 'csenhan',
					'label'   => 'Confirmação de Nova Senha',
					'rules'   => 'required|trim|min_lenght[6]|matches[senhan]'
				);
			}

			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				if($this->usuario_m->update($this->input->post()))
				{
					$this->session->set_flashdata('msg', msg('Sucesso ao alterar '.$this->modulo->alias, 'sucesso'));
					redirect(admin_url($this->modulo->url));
				}
				else
				{
					$this->template->write('msg', msg('Erro ao alterar '.$this->modulo->alias, 'erro'));
				}
			}
			else
			{
				$this->template->write('msg', msg(validation_errors(), 'erro'));
			}
		}

		$this->template->render();
	}

	/**
	 * Altera registro com validação de fomulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function perfil()
	{
		$a = $this->usuario_m->get_id($this->session->userdata('usuario'));
		$this->template->write('conteudo', heading('Perfil do Usuário : '.$a->desc_usuario));
		$this->template->write_view('conteudo', 'admin/perfil', $a);
		$this->template->write('atalho', '', TRUE);

		// Dados recebidos
		if($_POST)
		{
			array_pop($this->validation_rules);
			if($this->input->post('alterar_senha') == 1)
			{
				$this->validation_rules[] = array(
					'field'   => 'senha',
					'label'   => 'Senha',
					'rules'   => 'required|trim|min_lenght[6]'
				);
				$this->validation_rules[] = array(
					'field'   => 'senhan',
					'label'   => 'Nova Senha',
					'rules'   => 'required|trim|min_lenght[6]'
				);
				$this->validation_rules[] = array(
					'field'   => 'csenhan',
					'label'   => 'Confirmação de Nova Senha',
					'rules'   => 'required|trim|min_lenght[6]|matches[senhan]'
				);
			}

			$this->form_validation->set_rules($this->validation_rules);

			if($this->form_validation->run())
			{
				if($this->usuario_m->perfil($this->input->post()))
					$this->session->set_flashdata('msg', msg('Sucesso ao alterar Perfil do '.$this->modulo->alias, 'sucesso'));
				else
					$this->template->write('msg', msg('Erro ao alterar Perfil do '.$this->modulo->alias, 'erro'));
			}
			else
			{
				$this->template->write('msg', msg(validation_errors(), 'erro'));
			}
			redirect($this->agent->referrer());
		}

		$this->template->render();
	}

	/**
	 * Exclui registro
	 *
	 * @access	public
	 * @return	redirect
	 */
	public function excluir()
	{
    if(in_array($this->uri->segment(4), array(1,2)))
		{
			$this->session->set_flashdata('msg', msg('Este Usuário não pode ser excluído!', 'erro'));
		}
		else
		{
      $this->usuario_m->delete($this->uri->segment(4))
        ? $this->session->set_flashdata('msg', msg('Sucesso ao excluir '.$this->modulo->alias.'.', 'sucesso'))
        : $this->session->set_flashdata('msg', msg('Erro ao excluir '.$this->modulo->alias.'.', 'erro'));
    }
		redirect(admin_url($this->modulo->url));
	}

	/**
	 * Altera registro com validação de fomulário
	 *
	 * @access	public
	 * @return	boolean | redirect
	 */
	public function permissao()
	{
		$a = $this->usuario_m->get_id($this->uri->segment(4));
		$this->template->write('conteudo', heading('Permissões do '.$this->modulo->alias.' '.$a->desc_usuario));
		$this->template->write_view('conteudo', 'admin/permissao', $a);

		// Dados recebidos
		if($_POST)
		{
			unset($_POST['todos']);
			unset($_POST['button']);
			$id = array_shift($_POST);
			$permissao = (count($_POST) == 0) ? NULL : implode(':', $this->input->post());
			$this->usuario_m->permissao(array('id' => $id, 'permissao' => $permissao))
				? $this->session->set_flashdata('msg', msg('Permissões do '.$this->modulo->alias.' alteradas com sucesso', 'sucesso'))
				: $this->session->set_flashdata('msg', msg('Erro ao alterar Permissões do '.$this->modulo->alias, 'erro'));
				redirect($this->agent->referrer());
		}

		$this->template->render();
	}

	/**
	 * Codifica registros em Json
	 *
	 * @access	public
	 * @return	string
	 */
	function json_listar()
	{
		######################## ALTERAR
		//Colunas de retorno
		$colunas = array($this->usuario_m->_id, $this->usuario_m->_desc, 'email', 'id_grupo', 'nivel', 'ativo', 'opcoes');
		$grupo = $this->grupo_m->get_array();
		$nivel = $this->config->item('nivel');

		//Campo de ID
		$id = $colunas[0];

		// Paginação
		if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1')
		{
			$n = $_POST['iDisplayLength'];
			$offset = $_POST['iDisplayStart'];
		}
		else
		{
			if (isset($_POST['iDisplayLength']))
			{
				$n = ($_POST['iDisplayLength'] == '-1') ? NULL : $_POST['iDisplayLength'];
				$offset = 0;
			}
			else
			{
				$n = 25;
				$offset = 0;
			}
		}

		// Ordenação
		if(isset($_POST['iSortCol_0']))
		{
			$order = NULL;
			for($i=0;$i<intval($_POST['iSortingCols']);$i++)
			{
				if($_POST['bSortable_'.intval($_POST['iSortCol_'.$i])] == "true")
				{
					if(trim($colunas[intval($_POST['iSortCol_'.$i])]) == 'opcoes') { $order = NULL; break; }
					$order .= $colunas[intval($_POST['iSortCol_'.$i])]." ".$_POST['sSortDir_'.$i].", ";
				}
			}
			$order = rtrim($order, ' ,');
			if((trim($order) == 'ORDER BY') || (strlen($order) == 0)) {	$order = NULL; }
		}
		else
			$order = NULL;

		//Filtro
		$orlike = NULL;
		if(strlen($_POST['sSearch']) > 0)
		{
			$campo = array();
			$valor = array();
			for($i = 0; $i < (count($colunas)-1); $i++)
			{
				$campo[] = $colunas[$i];
				$valor[] = trim($_POST['sSearch']);
			}
			$orlike = array_combine($campo, $valor);
		}

		######################## ALTERAR
		//SQL Query
		$query = $this->usuario_m->get_all($n, $offset, NULL, NULL, $order, $orlike);

		// Resultado do filtro
		$query_filtro = $this->usuario_m->get_all(NULL, NULL, NULL, NULL, $order, $orlike);
		$filtro = $query_filtro->num_rows();

		// Total
		$query_total = $this->usuario_m->get_all();
		$total = $query_total->num_rows();

		//Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $total,
			"iTotalDisplayRecords" => $filtro,
			"aaData" => array()
		);

		######################## ALTERAR
		foreach($query->result() as $row)
		{
			$retorno = array();
			for($i=0;$i<count($colunas);$i++)
			{
				switch($colunas[$i])
				{
					case 'opcoes': // Ações
						$links = array();
						$links[] = anchor(admin_url($this->modulo->url.'/alterar/'.$row->{$this->usuario_m->_id}), 'alterar', array('class' => 'alterar'));
						($row->id_usuario == 1) ? NULL : $links[] = anchor(admin_url($this->modulo->url.'/excluir/'.$row->{$this->usuario_m->_id}), 'excluir', array('desc' => $row->{$this->usuario_m->_desc}, 'class' => 'excluir', 'onclick' => 'excluir();'));
						$links[] = anchor(admin_url($this->modulo->url.'/permissao/'.$row->{$this->usuario_m->_id}), 'permissão', array('class' => 'permissao'));
						$retorno[] = implode(' | ', $links);
					break;
					case 'ativo':
						$retorno[] = center(status_icon($row->ativo));
					break;
					case 'id_grupo':
						$retorno[] = $grupo[$row->id_grupo];
					break;
					case 'nivel':
						$retorno[] = $nivel[(int)$row->nivel];
					break;
					default: // Outras colunas
						$retorno[] = $row->{$colunas[$i]};
				}
			}
			$output['aaData'][] = $retorno;
		}
		echo json_encode($output);
	}
}

/* End of file usuario.php */
/* Location: ./app/modules/controllers/usuario.php */
