<div id="atalhos">
  <h6>Atalhos</h6>
  <ul>
    <li><?php echo anchor(admin_url($this->modulo->url.'/adicionar'), 'Adicionar '.$this->modulo->desc_modulo, array('class' => 'adicionar')); ?></li>
    <li><?php echo anchor(admin_url($this->modulo->url), 'Listar '.$this->modulo->desc_modulo, array('class' => 'listar')); ?></li>
    <li><?php echo anchor(admin_url('grupo/adicionar'), 'Adicionar Grupo', array('class' => 'adicionar')); ?></li>
    <li><?php echo anchor(admin_url('grupo'), 'Listar Grupo', array('class' => 'listar')); ?></li>  
  </ul>
  <br class="clear-both">
</div>