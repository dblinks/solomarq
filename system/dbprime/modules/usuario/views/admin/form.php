<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_usuario));

echo li();
echo form_label('Nome: ', 'nome');
echo form_input(array('name' => 'nome', 'id' => 'nome', 'value' => set_value('nome', @$desc_usuario), 'title' => 'Digite o nome do usuário.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite o nome\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Email: ', 'email');
echo form_input(array('name' => 'email', 'id' => 'email', 'value' => set_value('email', @$email), 'title' => 'Digite o email do usuário, deve ser único.', 'class' => 'dica corner {validar:{required:true,email:true, messages:{required:\'Digite o email\',email:\'Digite um email válido\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Login: ', 'login');
echo form_input(array('name' => 'login', 'id' => 'login', 'value' => set_value('login', @$login), 'title' => 'Digite o login do usuário, deve ser único.', 'class' => 'input corner {validar:{required:true,minlength:6, messages:{required:\'Digite o login\',minlength:\'Tamanho mínimo de {0} caracteres\'}}}', 'maxlength' => '20', 'size' => '25')).br();
echo li_close();

if($this->_acao == 'adicionar')
{
	echo li();
	echo form_label('Senha: ', 'senha');
	echo form_password(array('name' => 'senha', 'id' => 'senha', 'value' => set_value('senha', @$senha), 'title' => 'Digite a senha.', 'class' => 'dica corner {validar:{required:true,minlength:6, messages:{required:\'Digite a senha\',minlength:\'Tamanho mínimo de {0} caracteres\'}}}', 'maxlength' => '10', 'size' => '25')).br();
	echo li_close();
}
else
{
	echo li();
	echo form_label('Alterar Senha: ', 'alterar_senha');
	echo form_checkbox(array('name' => 'alterar_senha', 'id' => 'alterar_senha', 'value' => '1', 'checked' => FALSE, 'title' => 'Para alterar a senha marque este campo.', 'class' => 'dica'));
	echo li_close();
	
	echo '<div id="box_alterar_senha" style="display:none;">';
		
	echo li();
	echo form_label('Senha atual: ', 'senha');
	echo form_password(array('name' => 'senha', 'id' => 'senha', 'value' => set_value('senha'), 'title' => 'Digite a senha atual do usuário.', 'class' => 'dica corner {validar:{required:\'#alterar_senha:checked\',minlength:6, messages:{required:\'Digite a senha\',minlength:\'Tamanho mínimo de {0} caracteres\'}}}', 'maxlength' => '10', 'size' => '25')).br();
	echo li_close();	
	
	echo li();
	echo form_label('Nova Senha: ', 'senhan');
	echo form_password(array('name' => 'senhan', 'id' => 'senhan', 'value' => set_value('senhan'), 'title' => 'Digite a nova senha.', 'class' => 'dica corner {validar:{required:\'#alterar_senha:checked\',minlength:6, messages:{required:\'Digite a nova senha\',minlength:\'Tamanho mínimo de {0} caracteres\'}}}', 'maxlength' => '10', 'size' => '25')).br();
	echo li_close();
	
	echo li();
	echo form_label('Confirmação de Nova Senha: ', 'csenhan');
	echo form_password(array('name' => 'csenhan', 'id' => 'csenhan', 'value' => set_value('csenhan'), 'title' => 'Digite novamente a nova senha.', 'class' => 'dica corner {validar:{required:\'#alterar_senha:checked\',equalTo:\'#senhan\',minlength:6, messages:{required:\'Confirme a nova senha\',minlength:\'Tamanho mínimo de {0} caracteres\',equalTo:\'Digite igual o campo Nova Senha\'}}}', 'maxlength' => '10', 'size' => '25')).br();
	echo li_close();	
	
	echo '</div>';		
}
echo li();
echo form_label('Dica de Senha: ', 'dica_senha');
echo form_input(array('name' => 'dica_senha', 'id' => 'dica_senha', 'value' => set_value('dica_senha', @$dica_senha), 'title' => 'Digite uma dica para lembrar a senha.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite uma dica de senha\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Grupo: ', 'grupo');
echo form_dropdown('grupo', $this->grupo_m->get_array(), @$id_grupo, 'id="grupo" title="Selecione o grupo que o usuário fará parte." class="dica {validar:{required:true, messages:{required:\'Selecione um grupo\'}}}"').br();
echo li_close();

echo li();
echo form_label('Nível: ', 'nivel');
$nivel_dd = $this->config->item('nivel');
if($this->usuario->nivel == 1) { unset($nivel_dd[1]); };
if($this->usuario->nivel == 2) { unset($nivel_dd[1]); unset($nivel_dd[2]); };
echo form_dropdown('nivel', $nivel_dd, set_value(3, @$nivel), 'id="nivel" title="Selecione o nível de acesso aos módulos que o usuário terá. Lembre-se de adicionar permissões ao usuário após o cadastro." class="dica {validar:{required:true, messages:{required:\'Selecione um nível\'}}}"').br();
echo li_close();

echo li();
echo form_label('Ativo: ', 'ativo');
echo form_checkbox('ativo', '1', (bool)@$ativo, 'class="dica" title="Marque para ativar o usuário."');
echo li_close();

echo br(2);

if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>