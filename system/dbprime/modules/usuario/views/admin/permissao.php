<?php
$this->table->set_heading(array('M&oacute;dulo', 'Descrição', 'Acesso'));

$perm_usuario = $this->usuario_m->get_field('permissao', $id_usuario);
$perm = explode(':', $perm_usuario);

$qry = $this->modulo_m->get_all(NULL, NULL, array('nivel >=' => (int)$nivel, 'ativo' => 1));
$sel = 0;
foreach($qry->result() as $row)
{
	$check = (in_array($row->url, $perm)) ? TRUE : FALSE;
	$this->table->add_row(array(
		$row->desc_modulo,
		$row->intro,
		center(form_checkbox(array(
			'name' => 'm'.$row->id_modulo,
			'value' => $row->url,
			'class' => 'check',
			'checked' => $check)))
	));
	($check) ? $sel++ : NULL;
}
$todos = (($qry->num_rows() > 0) && ((int)$sel == (int)$qry->num_rows())) ? 1 : 0;
$this->table->set_template($this->config->item('tabela_full'));

echo form_fieldset('');
echo form_open(current_url(), array('name' => 'form', 'id' => 'form'), array('id' => $id_usuario));

echo '<span style="font-weight:bold">Selecionar todos</span> '.form_checkbox(array('name' => 'todos', 'id' => 'todos', 'value' => '1', 'checked' => $todos)).'</center>'.br(2);

echo $this->table->generate().br();

echo form_button(array('name' => 'button', 'id' => 'botao', 'class' => 'botao azul', 'type' => 'submit', 'content' => 'Alterar '.img('img/icons/pencil.png')));
echo form_close();
echo form_fieldset_close();
?>