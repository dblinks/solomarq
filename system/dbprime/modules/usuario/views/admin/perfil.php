<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_usuario));

echo li();
echo form_label('Imagem Gravatar: ', 'gravatar');
echo img(get_gravatar($email)).br();
echo li_close();

echo li();
echo form_label('Nome: ', 'nome');
echo form_input(array('name' => 'nome', 'id' => 'nome', 'value' => set_value('nome', @$desc_usuario), 'title' => 'Digite seu nome.', 'class' => 'dica corner igualf {validar:{required:true, messages:{required:\'Digite o nome\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Email: ', 'email');
echo form_input(array('name' => 'email', 'id' => 'email', 'value' => set_value('email', @$email), 'title' => 'Digite seu email, pode ser utilizado para acessar a administração.', 'class' => 'dica corner {validar:{required:true,email:true, messages:{required:\'Digite o email\',email:\'Digite um email válido\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Login: ', 'login');
echo form_input(array('name' => 'login', 'id' => 'login', 'value' => set_value('login', @$login), 'title' => 'Digite seu login.', 'class' => 'dica corner {validar:{required:true,minlength:6, messages:{required:\'Digite o login\',minlength:\'Tamanho mínimo de {0} caracteres\'}}}', 'maxlength' => '20', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Alterar Senha: ', 'alterar_senha');
echo form_checkbox(array('name' => 'alterar_senha', 'id' => 'alterar_senha', 'value' => '1', 'checked' => FALSE, 'class' => 'dica', 'title' => 'Deseja alterar a sua senha?'));
echo li_close();

echo '<div id="box_alterar_senha" style="display:none;">';

echo li();
echo form_label('Senha atual: ', 'senha');
echo form_password(array('name' => 'senha', 'id' => 'senha', 'value' => set_value('senha'), 'title' => 'Digite sua senha atual.', 'class' => 'dica corner {validar:{required:\'#alterar_senha:checked\',minlength:6, messages:{required:\'Digite a senha atual\',minlength:\'Tamanho mínimo de {0} caracteres\'}}}', 'maxlength' => '10', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Nova Senha: ', 'senhan');
echo form_password(array('name' => 'senhan', 'id' => 'senhan', 'value' => set_value('senhan'), 'title' => 'Digite a nova senha que deseja.', 'class' => 'dica corner {validar:{required:\'#alterar_senha:checked\',minlength:6, messages:{required:\'Digite a nova senha\',minlength:\'Tamanho mínimo de {0} caracteres\'}}}', 'maxlength' => '10', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Confirmação de Nova Senha: ', 'csenhan');
echo form_password(array('name' => 'csenhan', 'id' => 'csenhan', 'value' => set_value('csenhan'), 'title' => 'Digite novamente sua nova senha, para confirmação.', 'class' => 'dica corner {validar:{required:\'#alterar_senha:checked\',equalTo:\'#senhan\',minlength:6, messages:{required:\'Confirme a nova senha\',minlength:\'Tamanho mínimo de {0} caracteres\',equalTo:\'Digite igual o campo Nova Senha\'}}}', 'maxlength' => '10', 'size' => '25')).br();
echo li_close();

echo '</div>';

echo li();
echo form_label('Dica de Senha: ', 'dica_senha');
echo form_input(array('name' => 'dica_senha', 'id' => 'dica_senha', 'value' => set_value('dica_senha', @$dica_senha), 'title' => 'Digite uma dica para lembrar sua senha.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite uma dica de senha\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();
echo br(2);

echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Atualizar Perfil '.img('img/icons/user-green.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>