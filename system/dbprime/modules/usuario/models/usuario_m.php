<?php
/**
 * Usuario_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Usuario
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20110921
 */
class Usuario_m extends DBModel {

	var $_tabela = 'usuario';
	var $_id = 'id_usuario';
	var $_desc = 'desc_usuario';
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_usuario ASC';

	/**
	 * Função executada antes da inserção
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function _before_insert($dado)
	{
		$a = $this->get_by_email($dado['email']);
		if ($a->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Usuário já cadastrado com este email', 'erro'));
			return FALSE;
		}

		$b = $this->get_by_login($dado['login']);
		if ($b->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Usuário já cadastrado com este login', 'erro'));
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Função executada antes daalteração
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function _before_update($dado, $id = NULL)
	{
		$this->db->where_not_in($this->_id, $id);
		$a = $this->get_by_email($dado['email']);
		if ($a->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Usuário já cadastrado com este email', 'erro'));
			return FALSE;
		}

		$this->db->where_not_in($this->_id, $id);
		$b = $this->get_by_login($dado['login']);
		if ($b->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Usuário já cadastrado com este login', 'erro'));
			return FALSE;
		}

		if($dado['alterar_senha'])
		{
			$s = $this->get_all(1, NULL, array($this->_id => $dado['id'], 'senha' => sha1($dado['senha'])));
			if($s->num_rows() == 0)
			{
				$this->session->set_flashdata('msgaux', msg('A Senha Atual não confere com senha atual cadastrada!', 'erro'));
				return FALSE;
			}

			if($dado['senhan'] != $dado['csenhan'])
			{
				$this->session->set_flashdata('msgaux', msg('O campo Confirmação de Nova Senha deve ser igual ao Nova Senha!', 'erro'));
				return FALSE;
			}
		}

		return TRUE;
	}

	/**
	 * Efetua login
	 *
	 * @access	public
	 * @return boolean
	 */
	function login($dado = array())
	{
		$dado['senha'] = sha1($dado['senha']);

		$u = $this->usuario_m->get_all(NULL, NULL, $dado);

		if($u->num_rows() > 0)
		{
			$this->session->unset_userdata('email');
			foreach($u->result()  as $usuario)
				$this->session->set_userdata(array('logado' => 1, 'usuario' => $usuario->id_usuario, 'grupo' => $usuario->id_grupo, 'tipo' => 'admin'));
		}
		return $u->num_rows();
	}

	/**
	 * Efetua recuperação de senha
	 *
	 * @access	public
	 * @return boolean
	 */
	function recuperar_senha()
	{
		$u = new Usuario_m();
		$u->where('email', $this->email)->get();

		if(!empty($u->id_usuario))
		{
			return TRUE;
		}
		else
		{
			$this->error_message('email', 'Email inválido.');
			return FALSE;
		}
	}

	/**
	 * Salva usuário no banco de dados
	 * com verificação de email e login
	 *
	 * @access	public
	 * @return	boolean
	 */
	function insert($dado, $tabela = NULL)
	{
		if(!$this->_before_insert($dado)) { return  FALSE; }
		return parent::insert(array(
			'id_grupo' => $dado['grupo'],
			$this->_desc => $dado['nome'],
			'email'	=> $dado['email'],
			'login'	=> $dado['login'],
			'senha'	=> sha1($dado['senha']),
			'dica_senha'	=> $dado['dica_senha'],
			'ativo'	=> @(int)$dado['ativo'],
			'nivel'	=> @(int)$dado['nivel']
		));
	}

	/**
	 * Altera usuário no banco de dados
	 * com validação de email e login
	 *
	 * @access	public
	 * @return	boolean
	 */
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
    $post = $dado;
		if(!$this->_before_update($dado, $dado['id'])) { return  FALSE; }

    unset($dado);

		$dado = array(
			'id_grupo' => $post['grupo'],
			$this->_desc => $post['nome'],
			'email'	=> $post['email'],
			'login'	=> $post['login'],
			'dica_senha'	=> $post['dica_senha'],
			'ativo'	=> @$post['ativo'],
			'nivel'	=> @$post['nivel']
		);

		if($post['alterar_senha'])
		{
			$arr = array('senha' => sha1($post['senhan']));
			$dado = array_merge($dado, $arr);
		}

		return parent::update($dado, $post['id']);
	}

	/**
	 * Altera perfil do usuário
	 *
	 * @access	public
	 * @return	boolean
	 */
	function perfil($dado = array())
	{
		$post = $dado;
		if(!$this->_before_update($dado, $dado['id'])) { return  FALSE; }
		$id = $dado['id'];

		unset($dado);
		$dado = array(
			$this->_desc => $post['nome'],
			'email'	=> $post['email'],
			'login'	=> $post['login'],
			'dica_senha'	=> $post['dica_senha']
		);

		if($post['alterar_senha'])
		{
			$arr = array('senha' => sha1($post['senhan']));
			$dado = array_merge($dado, $arr);
		}

		return parent::update($dado, $post['id']);
	}

	/**
	 * Altera as permissões do usuário
	 *
	 * @access	public
	 * @return	boolean
	 */
	function permissao($dado = array())
	{
		return parent::update(array(
			'permissao' => $dado['permissao']
		), $dado['id']);
	}

	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}
}

/* End of file Usuario_m.php */
/* Location: ./app/modules/models/Usuario_m.php */
