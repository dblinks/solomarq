<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_pagina));

echo li();
echo form_label('Título: ', 'pagina');
echo form_input(array('name' => 'pagina', 'id' => 'pagina', 'value' => set_value('pagina', @$desc_pagina), 'title' => 'Digite o título da página, como irá aparecer no menu superior no site,', 'class' => 'dica titulo {validar:{required:true, messages:{required:\'Digite o título da página\'}}}', 'maxlength' => '200', 'size' => '50')).br();
echo li_close();

echo li();
echo form_label('Texto: ', 'texto');
echo form_textarea(array('name' => 'texto', 'id' => 'texto', 'value' => set_value('texto', @$texto), 'title' => 'Digite o texto da página.', 'class' => 'dica editor {validar:{required:true, messages:{required:\'Digite o texto\'}}}', 'cols' => '50', 'rows' => '10')).br();
echo li_close();

echo li();
echo form_label('Vídeo: ', 'video');
echo form_input(array('name' => 'video', 'id' => 'video', 'value' => set_value('video', @$video), 'title' => 'Digite somente o código após v=. Ex.: http://www.youtube.com/watch?v=-F_ke3rxopc', 'class' => 'dica', 'maxlength' => '15', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Mostrar: ', 'status');
echo form_dropdown('status', $this->config->item('simnao'), (isset($status) && @$status == 0) ? 0 : 1, 'class="dica" title="Esta página deve ser mostrada?"');
echo li_close();

if($this->_acao == 'adicionar')
{
  $ordem_max = $this->db->select_max('ordem')->get($this->pagina_m->_tabela);
  foreach($ordem_max->result() as $ordem_m ) { $ordem = $ordem_m->ordem + 1; }
}
echo li();
echo form_label('Ordem: ', 'ordem');
echo form_input(array('name' => 'ordem', 'id' => 'ordem', 'value' => set_value('ordem', @$ordem), 'class' => 'input numero dica', 'title' => 'Digite a ordem que deve aparecer no menu, pode ser alterada nas setas da listagem.', 'maxlength' => '2', 'size' => '5')).br();
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>