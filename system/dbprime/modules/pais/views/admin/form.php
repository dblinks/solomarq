<?php
echo ul(array('class' => 'zebra'));

echo form_fieldset('');

echo form_open(current_url(), array('class' => 'form validar'), array('id' => @$id_pais));

echo li();
echo form_label('País: ', 'pais');
echo form_input(array('name' => 'pais', 'id' => 'pais', 'value' => set_value('pais', @$desc_pais), 'title' => 'Digite o nome do país.', 'class' => 'dica corner {validar:{required:true, messages:{required:\'Digite o país\'}}}', 'maxlength' => '200', 'size' => '25')).br();
echo li_close();

echo li();
echo form_label('Código ISO (ISO 3166-2): ', 'iso');
echo form_input(array('name' => 'iso', 'id' => 'iso', 'value' => set_value('iso', @$iso), 'title' => 'Digite o código ISO  3166-2, 2 caracteres: ex.: BR', 'class' => 'dica corner {validar:{minlength:2, messages:{minlength:\'O tamanho é 2 caracteres\'}}}', 'alt' => 'Digite o código ISO (ISO 3166-2)', 'maxlength' => '2', 'size' => '5')).br();
echo li_close();

echo li();
echo form_label('Código ISO (ISO 3166-1): ', 'iso3');
echo form_input(array('name' => 'iso3', 'id' => 'iso3', 'value' => set_value('iso3', @$iso3), 'title' => 'Digite o código ISO  3166-3, 3 caracteres: ex.: BRA', 'class' => 'dica corner {validar:{minlength:3, messages:{minlength:\'O tamanho é 3 caracteres\'}}}', 'alt' => 'Digite o código ISO  (ISO 3166-1)', 'maxlength' => '3', 'size' => '5')).br();
echo li_close();

echo br(2);
if($this->_acao == 'alterar')
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'azul corner', 'type' => 'submit', 'value' =>  'alterar', 'content' => 'Alterar '.img('img/icons/pencil.png')));
else
	echo form_button(array('name' => 'button', 'id' => 'enviar', 'class' => 'verde corner', 'type' => 'submit', 'value' =>  'cadastrar', 'content' => 'Cadastrar '.img('img/icons/tick.png')));

echo form_close();

echo form_fieldset_close();

echo ul_close();
?>