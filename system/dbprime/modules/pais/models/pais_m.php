<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Pais_m - Model
 *
 * @package 		DataMapper
 * @subpackage 	Pais
 * @category 		Módulo
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link	
 * @version 		1.0 #20110921	
 */

class Pais_m extends DBModel {

	var $_tabela = 'pais';
	var $_id = 'id_pais';
	var $_desc = 'desc_pais';		
	var $_cri = 'data_cri';
	var $_alt = 'data_alt';
	var $_order = 'desc_pais ASC';
  
	/**
	 * Inicializa a classe
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Função executada antes da inserção
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function _before_insert($dado)
	{
		$a = $this->get_by_iso($dado['iso']);
		if ($a->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Código ISO (ISO 3166-2) já utilizado em outro país', 'erro'));
			return FALSE;
		}
		
		$b = $this->get_by_iso3($dado['iso3']);
		if ($b->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Código ISO (ISO 3166-3) já utilizado em outro país', 'erro'));
			return FALSE;
		}

		return TRUE;
	}
	
	/**
	 * Função executada antes daalteração
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function _before_update($dado, $id = NULL)
	{
		$this->db->where_not_in($this->_id, $id);
		$iso = $this->get_by_iso($dado['iso']);
		if ($iso->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Código ISO (ISO 3166-2) já utilizado em outro país', 'erro'));
			return FALSE;
		}
		
		$this->db->where_not_in($this->_id, $id);
		$iso3 = $this->get_by_iso3($dado['iso3']);
		if ($iso3->num_rows() > 0)
		{
			$this->session->set_flashdata('msgaux', msg('Código ISO (ISO 3166-3) já utilizado em outro país', 'erro'));
			return FALSE;
		}

		return TRUE;
	}	
	
	/**
	 * Insere registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function insert($dado, $tabela = NULL)
	{
		if(!$this->_before_insert($dado)) { return  FALSE; }
		return parent::insert(array(
			$this->_desc => $dado['pais'],
			'iso'	=> $dado['iso'],
			'iso3'	=> $dado['iso3'],
			'slug'	=> slug($dado['pais'])
		));
	}
		
	/**
	 * Altera registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */	
	function update($dado, $id = NULL, $tabela = NULL, $id_tabela = NULL)
	{
		if(!$this->_before_update($dado, $dado['id'])) { return  FALSE; }
		return parent::update(array(
			$this->_desc => $dado['pais'],
			'iso'	=> $dado['iso'],
			'iso3'	=> $dado['iso3'],
			'slug'	=> slug($dado['pais'])
		), $dado['id']);
	}		
		
	/**
	 * Exclui registro no banco de dados
	 *
	 * @access	public
	 * @return	boolean
	 */			
	function delete($id, $tabela = NULL, $id_tabela = NULL)
	{
		return parent::delete($id);
	}
}

/* End of file Pais_m.php */
/* Location: ./app/modules/pais/models/Pais_m.php */
