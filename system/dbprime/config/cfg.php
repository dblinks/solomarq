<?php
// Tabela listagem json - datatables
$config['tabela_json'] = array (
	'table_open'          => '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tabela_zebra json">',

	'heading_row_start'   => '<tr>',
	'heading_row_end'     => '</tr>',
	'heading_cell_start'  => '<th>',
	'heading_cell_end'    => '</th>',

	'row_start'           => '<tr>',
	'row_end'             => '</tr>',
	'cell_start'          => '<td>',
	'cell_end'            => '</td>',

	'table_close'         => '</table>'
);

// Tabela listagem zebra
$config['tabela_zebra'] = array (
	'table_open'          => '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tabela_zebra">',

	'heading_row_start'   => '<tr>',
	'heading_row_end'     => '</tr>',
	'heading_cell_start'  => '<th>',
	'heading_cell_end'    => '</th>',

	'row_start'           => '<tr>',
	'row_end'             => '</tr>',
	'cell_start'          => '<td>',
	'cell_end'            => '</td>',

	'table_close'         => '</table>'
);

//Tabela listagem completa
$config['tabela_full'] = array (
	'table_open'          => '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tabela_zebra full">',

	'heading_row_start'   => '<tr>',
	'heading_row_end'     => '</tr>',
	'heading_cell_start'  => '<th>',
	'heading_cell_end'    => '</th>',

	'row_start'           => '<tr>',
	'row_end'             => '</tr>',
	'cell_start'          => '<td>',
	'cell_end'            => '</td>',

	'table_close'         => '</table>'
);

// Nível dos Módulos
$config['nivel'] = array(1 => 'Super Usuário', 'Administrador', 'Usuário');

// Sim/Não
$config['simnao'] = array(0 => 'Não', 'Sim');

// status
$config['status'] = array(0 => 'Rascunho', 'Publicado');
$config['status_icon'] = array(0 => 'Rascunho', 'Publicado');

// parceiro
$config['parceiro'] = array(1 => 'Parceiro', 'Cliente');

// protocolo de envio de email
$config['email_protocolo'] = array('mail' => 'Mail (PHP)', 'sendmail' => 'Sendmail', 'smtp' => 'SMTP (Autenticado)');

// Informações do desenvolvedor
$config['dev_url'] = 'http://www.dblinks.com.br';
$config['dev_desc'] = 'dblinks - soluções inteligentes para web';
$config['dev_email'] = 'atendimento@dblinks.com.br';
$config['dev_dev'] = 'Israel Messias Júnior';

// Mes
$config['mes'] = array(1 => 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
$config['mes_abr'] = array(1 => 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');

// Semana
$config['semana'] = array(1 => 'Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado');
$config['semana_abr'] = array(1 => 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb');

// Link
$config['janela_link'] = array('_blank' => 'Nova janela', '_self' => 'Mesma janela');

// Diretório de mídias
$config['url_liberada'] = array('login', 'recuperar_senha', 'logout', 'ajax_slug');

// Diretório de mídias
$config['dir_midia'] = 'midia/';

// Diretório de temas
$config['dir_tema'] = 'themes/';

// Produto e Serviço
$config['pro_ser'] = array(0 => 'Produto e Serviço', 1 => 'Produto', 2 => 'Serviço');

// Tipo de mapa Google Maps
$config['gm_tipo'] = array('hybrid' => 'Híbrido', 'satellite' => 'Satélite', 'terrain' => 'Terreno', 'map' => 'Rua');

// Paginação
$config['produto_por_pagina'] = 10;
$config['noticia_por_pagina'] = 6;
$config['servico_por_pagina'] = 8;
?>