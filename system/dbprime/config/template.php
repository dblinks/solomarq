<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template
| group to make active.  By default there is only one group (the
| "default" group).
|
*/
$template['active_template'] = 'site';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land.
|   You may also include default markup, wrappers and attributes here
|   (though not recommended). Region keys must be translatable into variables
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/

/*
 * Site
 */
// Site
$template['site']['template'] = 'site_tpl';
$template['site']['regions'] = array('menu_lateral','conteudo', 'titulo');
$template['site']['parser'] = 'parser';
$template['site']['parser_method'] = 'parse';
$template['site']['parse_template'] = FALSE;

// Basic
$template['basic']['template'] = 'basic_tpl';
$template['basic']['regions'] = array('titulo', 'conteudo', 'msg');
$template['basic']['parser'] = 'parser';
$template['basic']['parser_method'] = 'parse';
$template['basic']['parse_template'] = FALSE;

/*
// RSS
$template['rss']['template'] = 'rss_tpl';
$template['rss']['regions'] = array('conteudo');
$template['rss']['parser'] = 'parser';
$template['rss']['parser_method'] = 'parse';
$template['rss']['parse_template'] = TRUE;

// XML
$template['xml']['template'] = 'xml_tpl';
$template['xml']['regions'] = array('conteudo');
$template['xml']['parser'] = 'parser';
$template['xml']['parser_method'] = 'parse';
$template['xml']['parse_template'] = TRUE;
*/
/*
 * Administração
 */
// Login / Recuperar Senha
$template['login']['template'] = 'admin/login_tpl';
$template['login']['regions'] = array('form','msg');
$template['login']['parser'] = 'parser';
$template['login']['parser_method'] = 'parse';
$template['login']['parse_template'] = FALSE;

// Admin
$template['admin']['template'] = 'admin/admin_tpl';
$template['admin']['regions'] = array('titulo', 'atalho', 'menu_lateral', 'btn_ajuda', 'ajuda', 'conteudo', 'msg');
$template['admin']['parser'] = 'parser';
$template['admin']['parser_method'] = 'parse';
$template['admin']['parse_template'] = FALSE;

//Layouts sem cabecalho
$template['no']['template'] = 'no_json_ajax_tpl';
$template['no']['regions'] = array('conteudo');
$template['no']['parser'] = 'parser';
$template['no']['parser_method'] = 'parse';
$template['no']['parse_template'] = FALSE;

$template['json']['template'] = 'no_json_ajax_tpl';
$template['json']['regions'] = array('conteudo');
$template['json']['parser'] = 'parser';
$template['json']['parser_method'] = 'parse';
$template['json']['parse_template'] = FALSE;

$template['ajax']['template'] = 'no_json_ajax_tpl';
$template['ajax']['regions'] = array('conteudo');
$template['ajax']['parser'] = 'parser';
$template['ajax']['parser_method'] = 'parse';
$template['ajax']['parse_template'] = FALSE;

/* End of file template.php */
/* Location: ./system/application/config/template.php */