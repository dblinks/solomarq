<?php
class MY_Config extends CI_Config {

/**
* CodeIgniter Config Extended Library
*
* This class extends the config to a database
*
* @package        CodeIgniter
* @subpackage    Extended Libraries
* @category    Extended Libraries
* @author        Tim Wood (aka codearachnid)
* @link        http://www.codearachnid.com/ci/db_fetch
* 
* you must have the following structure setup in order to use this class
* 
    CREATE TABLE IF NOT EXISTS `site_config` (
      `config_id` int(11) NOT NULL auto_increment,
      `last_updated` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP,
      `config_key` varchar(60) NOT NULL,
      `config_value` longtext NOT NULL,
      PRIMARY KEY  (`config_id`)
    ) ENGINE=MyISAM;
* 
* 
*/

	var $str = '::';

	function MY_Config()
	{
		parent::CI_Config();
	}
	
	function carregar_configuracao()
	{
		$CI =& get_instance();
		$query = $CI->db->get($this->item('table_config'));

		foreach ($query->result() as $row)
		{
				(substr_count($row->valor, $this->str)>0) ? $this->set_item($row->item, explode($this->str,$row->valor)) : $this->set_item($row->item, $row->valor);
				//$this->set_item($row->item, $row->valor);
		}
	}
}
/* End of file SAW_Config.php */
/* Location: ./system/libraries/SAW_Config.php */