<?php
class MY_Form_validation extends CI_Form_validation
{
  public function __construct()
  {
      parent::__construct();
  }
  
	// Alteração na função de verificação alfa para palavras com acento
  function alpha($str)
  {
    //return ( ! preg_match("/^([a-z])+$/i", $str)) ? FALSE : TRUE;
    //substitui por:
    return ( ! preg_match("/^([*A-Za-zá-úÁ=Ú.\s*])+$/i", $str)) ? FALSE : TRUE;
  }  
}
?>