<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Session extends CI_Session
{

    function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
     * Add or change flashdata, only available
     * until the next request
     *
     * @access    public
     * @param mixed
     * @param string
     * @return    void
     */
    function add_flashdata($newdata = array(), $newval = '')
    {
        if (is_string($newdata)) {
            $newdata = array($newdata => $newval);
        }

        if (count($newdata) > 0) {
            foreach ($newdata as $key => $val) {
                $flashdata_key = $this->flashdata_key . ':new:' . $key;
                $this->set_userdata($flashdata_key, $val);
            }
        }
    }
}
/* End of file SAW_Router.php */
/* Location: ./system/application/libraries/SAW_Router.php */
