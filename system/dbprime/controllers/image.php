<?php

class image extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('image_lib');
        $this->load->library('image_moo');
    }

    function index()
    {
        print '';
    }

    function get_image()
    {
        $img = explode(':', $this->uri->segment(3));

        $img[1] = (!isset($img[1])) ? 150 : $img[1];
        $img[2] = (!isset($img[2])) ? 150 : $img[2];

        $imagem = (file_exists(FCPATH . $this->config->item('dir_midia') . $img[0])) ? FCPATH . $this->config->item('dir_midia') . $img[0] : FCPATH . $this->config->item('dir_midia') . 'naodisponivel.jpg';

        $this->image_moo
            ->load($imagem)
            ->set_background_colour("#FFF")
            ->resize($img[1], $img[2])
            ->set_jpeg_quality(100)
            ->save_dynamic($imagem);
    }

    function get_image_c()
    {
        $img = explode(':', $this->uri->segment(3));

        $img[1] = (!isset($img[1])) ? 150 : $img[1];
        $img[2] = (!isset($img[2])) ? 150 : $img[2];

//        exit(var_dump($img));

        $imagem = (file_exists(FCPATH . $this->config->item('dir_midia') . $img[0])) ? FCPATH . $this->config->item('dir_midia') . $img[0] : FCPATH . $this->config->item('dir_midia') . 'naodisponivel.jpg';

        $this->image_moo
            ->load($imagem)
            ->set_background_colour("#FFF")
            ->resize_crop($img[1], $img[2])
            ->set_jpeg_quality(100)
            ->save_dynamic($imagem);
    }

}

?>
