<?php

/**
 * Sitemap_m - Model
 *
 * @package 		Site
 * @subpackage 	Sitemap
 * @category 		Sitemap
 * @author 			Júnior Messias <junior@dblinks.com.br>
 * @link
 * @version 		1.0 #20111206
 */

class Sitemap_m extends CI_Model
{
  public function gerar()
  {
    $this->load->library('Sitemap');
    $this->load->model('produto/produto_m');
    $this->load->model('pagina/pagina_m');
    $this->load->model('categoria/categoria_m');
    $this->load->model('noticia/noticia_m');
    $this->load->model('representante/representante_m');
    $this->load->model('menu/menu_m');

    // Menu
    $menus = $this->menu_m->get_all(NULL, NULL, array('status' => 1));
    if($menus->num_rows() > 0)
    {
      foreach($menus->result() as $m)
      {
        $item = array(
          'loc' => site_url($m->slug),
          'lastmod' => date('Y-m-d').'T'.date('H:i:sP'),
          'changefreq' => 'daily',
          'priority' => '0.5'
        );

        $this->sitemap->add_item($item);
        //echo implode(' > ', $item).br();
      }
    }

    // Páginas
    $paginas = $this->pagina_m->get_all(NULL, NULL, array('status' => 1));
    if($paginas->num_rows() > 0)
    {
      foreach($paginas->result() as $p)
      {
        $item = array(
          'loc' => site_url('empresa/ver/'.$p->id_pagina.'/'.$p->slug),
          'lastmod' => date('Y-m-d').'T'.date('H:i:sP'),
          'changefreq' => 'daily',
          'priority' => '0.5'
        );

        $this->sitemap->add_item($item);
        //echo implode(' > ', $item).br();
      }
    }

    // Categoria produto
    $categorias = $this->categoria_m->get_all(NULL, NULL, array('tipo' => 1));
    if($categorias->num_rows() > 0)
    {
      foreach($categorias->result() as $c)
      {
        $item = array(
          'loc' => site_url('produto/categoria/'.$c->id_categoria.'/'.$c->slug),
          'lastmod' => date('Y-m-d').'T'.date('H:i:sP'),
          'changefreq' => 'daily',
          'priority' => '0.5'
        );

        $this->sitemap->add_item($item);
        //echo implode(' > ', $item).br();
      }
    }

    // Produtos
    $produtos = $this->produto_m->get_all(NULL, NULL, array('status' => 1, 'tipo' => 1));
    if($produtos->num_rows() > 0)
    {
      foreach($produtos->result() as $p)
      {
        $item = array(
          'loc' => site_url('produto/ver/'.$p->id_produto.'/'.$p->slug),
          'lastmod' => date('Y-m-d').'T'.date('H:i:sP'),
          'changefreq' => 'daily',
          'priority' => '0.5'
        );

        $this->sitemap->add_item($item);
        //echo implode(' > ', $item).br();
      }
    }

    // Serviços
    $servicos = $this->produto_m->get_all(NULL, NULL, array('status' => 1, 'tipo' => 2));
    if($servicos->num_rows() > 0)
    {
      foreach($servicos->result() as $s)
      {
        $item = array(
          'loc' => site_url('servico/ver/'.$s->id_produto.'/'.$s->slug),
          'lastmod' => date('Y-m-d').'T'.date('H:i:sP'),
          'changefreq' => 'daily',
          'priority' => '0.5'
        );

        $this->sitemap->add_item($item);
        //echo implode(' > ', $item).br();
      }
    }

    // Representantes
    $representantes = $this->representante_m->get_all();
    if($representantes->num_rows() > 0)
    {
      foreach($representantes->result() as $r)
      {
        $item = array(
          'loc' => site_url('representante/ver/'.$r->uf),
          'lastmod' => date('Y-m-d').'T'.date('H:i:sP'),
          'changefreq' => 'daily',
          'priority' => '0.5'
        );

        $this->sitemap->add_item($item);
        //echo implode(' > ', $item).br();
      }
    }

    // Noticias
    $noticias = $this->noticia_m->get_all();
    if($representantes->num_rows() > 0)
    {
      foreach($noticias->result() as $n)
      {
        $item = array(
          'loc' => site_url('noticia/ver/'.$n->id_noticia.'/'.$n->slug),
          'lastmod' => date('Y-m-d').'T'.date('H:i:sP'),
          'changefreq' => 'daily',
          'priority' => '0.5'
        );

        $this->sitemap->add_item($item);
        //echo implode(' > ', $item).br();
      }
    }

    $file_name = $this->sitemap->build("sitemap.xml");

    $reponses = $this->sitemap->ping(site_url($file_name));
  }
}
?>
