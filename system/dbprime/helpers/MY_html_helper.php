<?php
/**
* Script
*
* Generates a script inclusion of a JavaScript file
* Based on the CodeIgniters original Link Tag.
*
* Author(s): Isern Palaus <ipalaus@ipalaus.es>, Viktor Rutberg <wishie@gmail.com>
*
* @access    public
* @param     mixed    javascript sources or an array
* @param     string    language
* @param     string    type
* @param     boolean    should index_page be added to the javascript path
* @return    string
*/

function gerar_atributos($atributos = NULL)
{
	$retorno = '';
	if(!is_null($atributos))
	{
		foreach ($atributos as $key => $val)
		{
			$retorno .= ' '.$key.'="'.$val.'"';
		}
	}
	return $retorno;
}


if ( ! function_exists('script_tag'))
{
    function script_tag($src = '', $language = 'javascript', $type = 'text/javascript', $index_page = FALSE)
    {
        $CI =& get_instance();

        $script = '<script';

        if(is_array($src))
        {
            foreach($src as $v)
            {
                if ($k == 'src' AND strpos($v, '://') === FALSE)
                {
                    if ($index_page === TRUE)
                    {
                        $script .= ' src="'.$CI->config->site_url($v).'"';
                    }
                    else
                    {
                        $script .= ' src="'.$CI->config->slash_item('base_url').$v.'"';
                    }
                }
                else
                {
                    $script .= "$k=\"$v\"";
                }
            }

            $script .= ">\n";
        }
        else
        {
            if ( strpos($src, '://') !== FALSE)
            {
                $script .= ' src="'.$src.'" ';
            }
            elseif ($index_page === TRUE)
            {
                $script .= ' src="'.$CI->config->site_url($src).'" ';
            }
            else
            {
                $script .= ' src="'.$CI->config->slash_item('base_url').$src.'" ';
            }

            $script .= 'language="'.$language.'" type="'.$type.'"';

            $script .= '>';
        }

        $script .= '</script>'."\n";

        return $script;
    }
}

if ( ! function_exists('link_tag'))
{
	function link_tag($href = '', $rel = 'stylesheet', $type = 'text/css', $title = '', $media = '', $index_page = FALSE)
	{
		$CI =& get_instance();

		$link = '<link';

		if (is_array($href))
		{
			foreach ($href as $k=>$v)
			{
				if ($k == 'href' AND strpos($v, '://') === FALSE)
				{
					if ($index_page === TRUE)
					{
						$link .= ' href="'.$CI->config->site_url($v).'" ';
					}
					else
					{
						$link .= ' href="'.$CI->config->slash_item('base_url').$v.'" ';
					}
				}
				else
				{
					$link .= "$k=\"$v\" ";
				}
			}

			$link .= "/>";
		}
		else
		{
			if ( strpos($href, '://') !== FALSE)
			{
				$link .= ' href="'.$href.'" ';
			}
			elseif ($index_page === TRUE)
			{
				$link .= ' href="'.$CI->config->site_url($href).'" ';
			}
			else
			{
				$link .= ' href="'.$CI->config->slash_item('base_url').$href.'" ';
			}

			$link .= 'rel="'.$rel.'" type="'.$type.'" ';

			if ($media	!= '')
			{
				$link .= 'media="'.$media.'" ';
			}

			if ($title	!= '')
			{
				$link .= 'title="'.$title.'" ';
			}

			$link .= '/>'."\n";
		}


		return $link;
	}
}

if ( ! function_exists('heading'))
{
	function heading($data = '', $h = '1', $attributes = NULL)
	{
		$atts = '';
    if(is_array($attributes))
    {
      foreach ($attributes as $key => $val)
      {
        $atts .= ' '.$key.'="'.$val.'"';
      }
    }
    elseif(!is_null($attributes))
      $atts = ' '.$attributes;

		return "<h".$h.$atts.">".$data."</h".$h.">";
	}
}

if(!function_exists('limpa_html'))
{
	function limpa_html($string, $pode = '<p><i><b><strong>')
	{
		return strip_tags(utf8_encode(html_entity_decode($string)), $pode);
	}
}

/*
* tipo: erro | alerta | sucesso | informacao | nota | ajuda | vermelho | amarelo | verde | azul | cinza
*/
function msg($msg, $tipo, $class = NULL, $fechar = TRUE, $fade = TRUE)
{
	(is_array($class)) ? $atts = gerar_atributos($class) : $atts = $class;

	$atts = ($fade) ? $atts.' mensagem-fade' : $atts;
	$atts = ($fechar) ? $atts.' fecha' : $atts;
	return '<div class="mensagem '. $tipo . $atts . '">'. $msg . '</div>';
}

function ul($atributos = NULL)
{
  $atributos = (is_array($atributos)) ? gerar_atributos($atributos) : ' '.$atributos;
	return '<ul'.$atributos.'>';
}

function ul_close()
{
	return '</ul>'."\n";
}

function li($atributos = NULL, $valor = NULL)
{
	$atributos = gerar_atributos($atributos);
	if(!is_null($valor))
		return '<li'.$atributos.'>'.$valor.'</li>';
	else
		return '<li'.$atributos.'>';
}

function li_close()
{
	return '</li>'."\n";
}

function center($str)
{
	return '<center>'.$str.'</center>';
}

function status_icon($status, $verdadeiro = '', $falso = '')
{
	if($status)
		return img('img/icons/status.png').$verdadeiro;
	else
		return img('img/icons/status-offline.png').$falso;
}

?>
