<?php
// Array para objeto
function array2object($array)
{
	$object = new stdClass();
	if (is_array($array) && count($array) > 0)
	{
		foreach ($array as $name=>$value)
		{
			$name = strtolower(trim($name));
			if (!empty($name)) {
				$object->$name = $value;
			}
		}
	}
	return $object;
}

// Objeto para array
function object2array($object)
{
	$array = array();
	if (is_object($object))
	{
	$array = get_object_vars($object);
	}
	return $array;
}

// Ultimo elemento do array
function last_key($array)
{
	return $array[count($array)-1];
}
?>
