<?php
/*
Caractere de formato - Descrição	- Exemplo de valores retornados
Day	---	---
d	» Dia do mês, 2 digitos com preenchimento de zero	- 01 até 31
D	» Uma representação textual de um dia, três letras	- Mon até Sun
j	» Dia do mês sem preenchimento de zero	- 1 até 31
l » ('L' minúsculo)	A representação textual completa do dia da semana	- Sunday até Saturday
N	» Representação numérica ISO-8601 do dia da semana (adicionado no PHP 5.1.0) - 1 (para Segunda) até 7 (para Domingo)
S	» Sufixo ordinal inglês para o dia do mês, 2 caracteres	st, nd, rd ou th.	- Funciona bem com j
w	» Representação numérica do dia da semana	- 0 (para domingo) até 6 (para sábado)
z	» O dia do ano (começando do 0)	- 0 até 365

Semana	---	---
W	» Número do ano da semana ISO-8601, semanas começam na Segunda (adicionado no PHP 4.1.0)	- 42 (semana 42 do ano)

Mês	---	---
F	» Um representação completa de um mês - January até December
m	» Representação numérica de um mês, com leading zeros	- 01 a 12
M	» Uma representação textual curta de um mês, três letras	- Jan a Dec
n	» Representação numérica de um mês, sem leading zeros	- 1 a 12
t	» Número de dias de um dado mês	- 28 até 31

Ano	---	---
L	» Se está em um ano bissexto - 1 se está em ano bissexto, 0 caso contrário.
o	» Número do ano ISO-8601. Este tem o mesmo valor como Y, exceto que se o número da semana ISO (W) pertence ao anterior ou próximo ano, o ano é usado ao invés. (adicionado no PHP 5.1.0)	Exemplos: 1999 ou 2003
Y	» Uma representação de ano completa, 4 dígitos - 1999 ou 2003
y	» Uma representação do ano com dois dígitos	- 99 ou 03

Tempo	---	---
a	» Antes/Depois de meio-dia em minúsculo - am or pm
A	» Antes/Depois de meio-dia em maiúsculo - AM or PM
B	» Internet time - 000 até 999
g	» Formato 12-horas de uma hora sem preenchimento de zero - 1 até 12
G	» Formato 24-horas de uma hora sem preenchimento de zero - 0 até 23
h	» Formato 12-horas de uma hora com zero preenchendo à esquerda - 01 até 12
H	» Formato 24-horas de uma hora com zero preenchendo à esquerda - 00 até 23
i	» Minutos com zero preenchendo à esquerda	- 00 até 59
s	» Segundos, com zero preenchendo à esquerda	- 00 até 59
u	» Milisegundos (adicionado no PHP 5.2.2) - 54321

Timezone	---	---
e	» Identificador de Timezone (adicionado no PHP 5.1.0) - UTC, GMT, Atlantic/Azores
I » ('I' maiúsculo)	Se a data está ou não no horário de verão	1 se horário de verão, 0 caso contrário.
O	» Diferença para Greenwich time (GMT) em horas - +0200
P	» Diferença para Greenwich time (GMT) com dois pontos entre horas e minutos (adicionado no PHP 5.1.3) - +02:00
T	» Abreviação de Timezone - EST, MDT ...
Z	» Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive. - -43200 até 50400

Full Date/Time	---	---
c	» ISO 8601 date (adicionado no PHP 5)	2004-02-12T15:19:21+00:00
r	» RFC 2822 formatted date	Exemplo: Thu, 21 Dec 2000 16:01:07 +0200
U	» Segundos desde a Época Unix (January 1 1970 00:00:00 GMT)	Veja também time()
*/
// Data e Hora atual - timestamp
function data_atual($tipo = NULL, $mascara = NULL)
{
	$tipo = strtolower($tipo);
	switch($tipo)
	{
		case 'h':
			return is_null($mascara) ? date('H:i:s') : date($mascara);
		break;
		case 'd':
			return is_null($mascara) ? date('d/m/Y') : date($mascara);
		break;
		default:
			return is_null($mascara) ? date('Y-m-d H:i:s') : date($mascara);
	}
}

function data_hora_extenso($datahora, $hora = TRUE)
{
	// Dia;
	$arrDia = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
	// Mês;
	$arrMes = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
	// Dia da semana
	$intDiaDaSemana = date('w',strtotime($datahora));
	// Dia do mês
	$intDiaDoMes = date('d',strtotime($datahora));
	// Mês
	$intMesDoAno = date('n',strtotime($datahora));
	// Ano
	$intAno = date('Y',strtotime($datahora));
	//Hora
	$intHora = substr($datahora,10,20);
	// Formato
	if($hora)
		return $arrDia[$intDiaDaSemana] . ', ' . $intDiaDoMes . ' de ' . $arrMes[$intMesDoAno] . ' de ' . $intAno . ' às '.$intHora;
	else
		return $arrDia[$intDiaDaSemana] . ', ' . $intDiaDoMes . ' de ' . $arrMes[$intMesDoAno] . ' de ' . $intAno;
}

function timestamp()
{
	return mdate('%Y-%m-%d %H:%i:%s');
}

function bd2br($data)
{
	return (empty($data)) ? NULL : implode('/',array_reverse(explode('-', $data)));
}

function br2bd($data)
{
	return (empty($data)) ? NULL : implode('-',array_reverse(explode('/', $data)));
}

function calcula_data($data, $dias, $separador = NULL, $operacao = '+', $formato = 'bd')
{
	(is_null($separador) && $formato == 'bd') ? $separador = '-' : NULL;
	(is_null($separador) && $formato != 'bd') ? $separador = '/' : NULL;
	if($formato == 'bd')
	{
		 $ano = substr($data, 0, 4);
		 $mes = substr($data, 5, 2);
		 $dia = substr($data, 8, 2);
	}
	else
	{
		 $ano = substr($data, 6, 4);
		 $mes = substr($data, 3, 2);
		 $dia = substr($data, 0, 2);
	}
	($operacao == '+') ? $data = mktime(0, 0, 0, $mes, $dia + $dias, $ano) :  $data = mktime(0, 0, 0, $mes, $dia - $dias, $ano);
	return ($formato == 'bd') ? strftime('%Y'.$separador.'%m'.$separador.'%d', $data) : strftime('%d'.$separador.'%m'.$separador.'%Y', $data);
}

function data_hora($datahora = NULL)
{
	$datahora = (is_null($datahora)) ? timestamp() : $datahora;
	return str_replace(':','', str_replace(' ','', str_replace('-','', $datahora)));
}
?>
