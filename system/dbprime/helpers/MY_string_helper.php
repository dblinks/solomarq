<?php
function remove_especiais($string)
{
	$string = trim($string);

	$array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç" ,
									"Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" );
	$array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c" ,
									"A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" );

	$string = str_replace($array1, $array2, $string);

	$string = str_replace('/', '-', str_replace(' ', '  ', $string));

	$array3 = array('\'', '\"', '!', '@', '#', '$', '%', '¨', '&', '*', '(', ')', '-', '=', '+', '´', '`', '[', ']', '{', '}', '˜', '~', 'ˆ', '^', ',', '<', '.', '>', ';', ':', '?', '\\', '|', '¹', '²', '³', '£', '¢', '¬', '§', 'ª', 'º', '°');
	$array4 = '';

	$string = str_replace($array3, $array4, $string);

	return $string;
}

function remove_acentos($string)
{
	$string = html_entity_decode(trim($string), ENT_QUOTES, 'ISO-8859-1');

	$array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç" ,
									"Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" );
	$array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c" ,
									"A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" );

	$string = str_replace($array1, $array2, $string);

	$string = str_replace('/', '-', str_replace(' ', '  ', $string));

	$array3 = array('\'', '\"', '!', '@', '#', '$', '%', '¨', '&', '*', '(', ')', '-', '=', '+', '´', '`', '[', ']', '{', '}', '~', '^', ',', '<', '>', ';', ':', '?', '\\', '|', '¹', '²', '³', '£', '¢', '¬', '§', 'ª', 'º', '°');
	$array4 = '';

	$string = str_replace($array3, $array4, $string);

	return $string;
}

function alias($string, $sinal = '-')
{
	$str = str_replace(' ', $sinal,(trim(strtolower(remove_especiais($string)))));
	$str = str_replace($sinal.$sinal, $sinal, $str);
	return $str;
}

function slug($str, $tamanho = 100, $sinal = '-')
{
	$result = strtolower(remove_acentos($str));
	$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
	$result = trim(preg_replace("/[\s-]+/", " ", $result));
	$result = trim(substr($result, 0, $tamanho));
	$result = preg_replace("/\s/", $sinal, $result);

	return $result;
}

function v2p($string)
{
	return str_replace(',','.',str_replace('.','',$string));
}

function p2v($string)
{
	return str_replace('.',',',str_replace(',','',$string));
}

function moeda($numero)
{
	return number_format($numero, 2, ',', '.');
}

function senha($tamanho = 6)
{
	$str = "ABCDEFGHIJLMNOPQRSTUVXZYWKabcdefghijlmnopqrstuvxzywk0123456789@#$%^&*+=-";
	$cod = "";
	for($a = 0;$a < $tamanho; $a++)
	{
		$rand = rand(0, strlen($str));
		$cod .= substr($str, $rand, 1);
	}
	return $cod;
}

function letra_aleatoria($tamanho = 6)
{
	$str = "abcdefghijklmnopqrstuvwxyz";
	$cod = "";
	for($a = 0;$a < $tamanho; $a++)
	{
		$rand = rand(0, strlen($str));
		$cod .= substr($str, $rand, 1);
	}
	return $cod;
}

function corte($string, $tamanho, $caractere = ''){
	return (strlen($string) >= $tamanho) ? substr($string,0,$tamanho).$caractere : $string;
}

function addzero($string)
{
	return ((int)$string<10) ? '0'.$string : $string;
}

function br2nl($string)
{
    return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}
?>
