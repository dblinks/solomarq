<?php
// ------------------------------------------------------------------------
/**
 * SAW String
 *
 * Returns the SAW Url.
 *
 * @access	public
 * @return	string
 */

if (!function_exists('admin_url'))
{
	function admin_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->site_url('admin/'.$uri);
	}
}

/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $size Size in pixels, defaults to 80px [ 1 - 512 ]
 * @param string $default Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string $rating Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atributos Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source http://gravatar.com/site/implement/images/php/
 */
function get_gravatar($email, $size = 32, $default = 'mm', $rating = 'g', $img = false, $atributos = array())
{
	$url = 'http://www.gravatar.com/avatar/';
	$url .= md5(strtolower(trim($email)));
	$url .= "?s=$size&d=$default&r=$rating";
	if($img)
	{
		$url = '<img src="'.$url.'"';
		foreach($atributos as $key => $val)
			$url .= ' '.$key.'="'.$val.'"';
		$url .= ' />';
	}

	return $url;
}

function pegar_pagina($url, $referer = 'http://www.google.com.br', $timeout = 15, $header = FALSE)
{
	$curl = curl_init();
	if(strstr($referer,"://"))
	{
		curl_setopt ($curl, CURLOPT_REFERER, $referer);
	}
	curl_setopt ($curl, CURLOPT_URL, $url);
	curl_setopt ($curl, CURLOPT_TIMEOUT, $timeout);
	curl_setopt ($curl, CURLOPT_USERAGENT, sprintf("Mozilla/%d.0",rand(4,5)));
	curl_setopt ($curl, CURLOPT_HEADER, (int)$header);
	curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
	$html = curl_exec ($curl);
	curl_close ($curl);
	return $html;
}

function ver_versao($nova, $atual)
{
	$versao = version_compare($nova, $atual);
	switch($versao)
	{
		case '-1':
			return '(alpha)';
		break;
		case '1':
			return '(Nova vers&atilde;o '.$nova.')';
		break;
		default:
			return '(Vers&atilde;o atual)';
	}
}

function ci_version()
{
	$versao = 'ND';
	@$url_versao = fopen('http://versions.ellislab.com/codeigniter_version.txt', 'r');
	if (false !== $url_versao) {
		@$versao = stream_get_contents($url_versao);
		fclose(@$url_versao);
	}
	return $versao;
}

function check_gd()
{
	if ( ! extension_loaded('gd'))
	{
		if ( ! dl('gd.so'))
		{
			return 'Biblioteca GD parece não estar carregada.';
		}
	}

	if (function_exists('gd_info'))
	{
		$gdarray = @gd_info();
		$versiontxt = @ereg_replace('[[:alpha:][:space:]()]+', '', $gdarray['GD Version']);
		$versionparts = explode('.',$versiontxt);
		if ($versionparts[0]=="2")
		{
			return 'Versão 2.';
		}
		else
		{
			return 'É recomendado utilizar a versão 2 da biblioteca GD. Sua versão atual é '.$versiontxt;
		}
	}
	else
	{
		return 'Não foi possível verificar a versão da biblioteca GD';
	}
}


/* End of file SAW_url_helper.php */
/* Location: ./helpers/SAW_url_helper.php */
?>
