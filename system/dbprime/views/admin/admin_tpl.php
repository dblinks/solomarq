<?=doctype('xhtml1-trans')?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
echo '<title>'.$this->empresa->desc_empresa.' - '.$this->empresa->slogan.' | Administração</title>';
	// META
	echo meta(array('name' => 'title', 'content' => $this->empresa->desc_empresa.' - '.$this->empresa->slogan));
	echo meta(array('name' => 'robots', 'content' => 'NOINDEX, NOFOLLOW'));
	echo meta(array('name' => 'description', 'content' => $this->empresa->slogan));
	echo meta(array('name' => 'keywords', 'content' => $this->empresa->palavras_chave));
	echo meta(array('name' => 'autor', 'content' => $this->config->item('dev_desc').', '.$this->config->item('dev_email').', '.$this->config->item('dev_url')));
	echo meta(array('name' => 'copyright', 'content' => '© 2011 '.$this->config->item('dev_desc').', '.$this->config->item('dev_email').', '.$this->config->item('dev_url')));
	// Mobile Viewport Fix
	echo meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));
	echo meta(array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'));
	echo meta(array('name' => 'expires', 'content' => standard_date('DATE_RFC822', time()+14400), 'type' => 'equiv'));
	//Always force latest IE rendering engine & Chrome Frame
	echo meta(array('name' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1', 'type' => 'equiv'));
	echo link_tag('dblinks.ico', 'shortcut icon', 'image/ico');
	// CSS
	echo link_tag('css/reset.css');
	echo link_tag('css/jquery-ui.css');
	echo link_tag('css/uniform.default.css');
  echo link_tag('css/colorbox.css');
  echo link_tag('css/admin/admin.css');
  echo link_tag('css/admin/datatables.css');
	echo link_tag('css/admin/jquery.jgrowl.css');
	echo link_tag('css/admin/jquery.qtip.min.css');
	echo $_styles;
	// JS
  echo script_tag('js/html5.js');
  echo script_tag('js/jquery.min.js');
	echo script_tag('js/jquery-ui.min.js');
	echo script_tag('js/jquery.uniform.min.js');
	echo script_tag('js/jquery.metadata.js');
	echo script_tag('js/jquery.validate.min.js');
  echo script_tag('js/jquery.validate.xtra.js');
  echo script_tag('js/jquery.colorbox-min.js');
  echo script_tag('js/jquery.meio.mask.js');
  echo script_tag('js/admin/jquery.ba-throttle-debounce.min.js');
  echo script_tag('js/admin/jquery.livequery.js');
	echo script_tag('js/admin/jquery.dataTables.min.js');
	echo script_tag('js/admin/jquery.jgrowl_google.js');
	echo script_tag('js/admin/jquery.qtip.min.js');
  echo script_tag('js/tiny_mce/tiny_mce.js');
	echo script_tag('js/admin/admin.js');
	echo $_scripts;
?>
</head>
<!-- <body	oncontextmenu="return false" onselectstart="return false">-->
<body>
<noscript>
	<span class="noscript">Este sistema requer o Javascript habilitado para funcionar corretamente. <?php echo anchor(admin_url('habilitar_js'), 'Clique aqui'); ?> e execute o procedimento para habilitar o Javascript no seu navegador.</span>
</noscript>
<div id="tudo">
	<div id="box_lateral">
  	<div id="logotipo"></div>
    <h1 class="negrito cinza1"><?php echo $this->empresa->desc_empresa; ?></h1>
    <div id="info_usuario">
    	<div id="links" class="fonte_7">Olá <?php echo $this->usuario->desc_usuario; ?> | <?php echo anchor(admin_url('usuario/perfil'), 'Editar Perfil').br().anchor(site_url(), 'Ver Site')?> | <?=anchor(admin_url('logout'), 'Sair', array('class' => 'logout'))?></div>
    </div>
    <ul id="menu_lateral">
			<?=$menu_lateral?>
    </ul>
    <br /><br /><br /><br /><br />
    <hr />
    <div id="rodape_lateral" class="fonte_7 cinza2">
      <?php echo img('img/dblinks.png').br(); ?>
      Copyright © 2011 - dblinks<br />
      Versão <?php echo DBE_VERSION.nbs().DBE_STA.nbs().'rev'.DBE_REV ?> (CI <?php echo CI_VERSION; ?>)<br />
      Carregado em {elapsed_time}s<br />
      {memory_usage} de memória usados
		</div>
    <!-- box_lateral -fim-->
  </div>
  <div id="box_conteudo">
  	<div id="cabecalho">
      <?php
      if (($this->modulo) && !is_null($this->modulo->desc_modulo))
      {
        //echo (strlen($this->modulo->icone) > 0) ? img($this->modulo->icone).nbs() : NULL;
        echo heading($this->modulo->desc_modulo, 1);
      }
      else
        echo img('img/icons/home.png').nbs().heading('Página Inicial', 1);
      ?>
      <p><?php echo @$this->modulo->intro; ?></p>
      <p id="cabecalho-ajuda"><?php echo @$btn_ajuda; ?></p>
  	</div>
    <!-- cabecalho -fim-->
    <?php echo $atalho; ?>
    <!-- atalhos -fim-->
    <?php echo $msg; ?>
    <br class="clear" />
    <div id="conteudo">
    <?php
			echo $conteudo;
		?>
    </div>
  </div>
  <!-- box_conteudo -fim-->
</div>
</body>
</html>
