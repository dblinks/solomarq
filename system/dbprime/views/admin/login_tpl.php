<?=doctype('xhtml1-trans')?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
echo '<title>'.$this->empresa->desc_empresa.' - '.$this->empresa->slogan.' | Administração</title>';
	// META
	echo meta(array('name' => 'title', 'content' => $this->empresa->desc_empresa.' - '.$this->empresa->slogan));
	echo meta(array('name' => 'robots', 'content' => 'NOINDEX, NOFOLLOW'));
	echo meta(array('name' => 'description', 'content' => $this->empresa->slogan));
	echo meta(array('name' => 'keywords', 'content' => $this->empresa->palavras_chave));
	echo meta(array('name' => 'autor', 'content' => $this->config->item('dev_desc').', '.$this->config->item('dev_email').', '.$this->config->item('dev_url')));
	echo meta(array('name' => 'copyright', 'content' => '© 2011 '.$this->config->item('dev_desc').', '.$this->config->item('dev_email').', '.$this->config->item('dev_url')));
	// Mobile Viewport Fix
	echo meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));
	echo meta(array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'));
	echo meta(array('name' => 'expires', 'content' => standard_date('DATE_RFC822', time()+14400), 'type' => 'equiv'));
	//Always force latest IE rendering engine & Chrome Frame
	echo meta(array('name' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1', 'type' => 'equiv'));
	echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');

  // CSS
	echo link_tag('css/reset.css');
	echo link_tag('css/login/login.css');
	echo $_styles;
	// JS
	echo $_scripts;
?>
</head>
<body>
<noscript>
	<span class="noscript">Este sistema requer o Javascript habilitado para funcionar corretamente. <a href="habilitar_javascript.php">Clique aqui</a> e execute o procedimento para habilitar o Javascript no seu navegador.</span>
</noscript>
<div id="tudo">
  <div id="centro">
  <div class="logotipo"><?php echo img(array('src' => 'img/dbprime.png', 'class' => 'logotipo', 'align' => 'center')); ?></div>
  	<?php
echo (strlen($msg) > 0) ? $msg : NULL;
    echo $form;
		echo anchor('http://www.dblinks.com.br', img(array('src' => 'img/dblinks.png', 'id' => 'logodblinks')), array('target' => '_blank'));
		?>
  </div>
</div>
</body>
</html>
