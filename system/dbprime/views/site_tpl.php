<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
  echo '<title>'.$this->empresa->desc_empresa.' - '.$this->empresa->slogan.$titulo.'</title>'."\n";
	// META
	echo meta(array('name' => 'title', 'content' => $this->empresa->desc_empresa.' - '.$this->empresa->slogan.$titulo));
	echo meta(array('name' => 'robots', 'content' => 'INDEX, FOLLOW'));
	echo meta(array('name' => 'description', 'content' => $this->empresa->slogan.$titulo));
	echo meta(array('name' => 'keywords', 'content' => $this->empresa->palavras_chave));
  echo meta(array('name' => 'revisit-after', 'content' => '2 days'));
	echo meta(array('name' => 'autor', 'content' => $this->config->item('dev_desc').', '.$this->config->item('dev_email').', '.$this->config->item('dev_url')));
	echo meta(array('name' => 'copyright', 'content' => '© 2011 '.$this->config->item('dev_desc').', '.$this->config->item('dev_email').', '.$this->config->item('dev_url')));
	// Mobile Viewport Fix
	echo meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));
	echo meta(array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'));
	echo meta(array('name' => 'expires', 'content' => standard_date('DATE_RFC822', time()+14400), 'type' => 'equiv'));
	//Always force latest IE rendering engine & Chrome Frame
	echo meta(array('name' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1', 'type' => 'equiv'));
  // Facebook
  echo '<meta property="og:title" content="'.$this->empresa->desc_empresa.' - '.$this->empresa->slogan.$titulo.'" />'."\n";
  echo '<meta property="og:description" content="'.$this->empresa->desc_empresa.' - '.$this->empresa->slogan.$titulo.'" />'."\n";
  echo '<meta property="og:site_name" content="'.$this->empresa->desc_empresa.' - '.$this->empresa->slogan.$titulo.'" />'."\n";
  echo '<meta property="og:image" content="'.site_url($this->config->item('dir_midia').$this->empresa->logotipo).'" />'."\n";
  echo '<base href="'.site_url().'" />'."\n";
  echo '<link href="'.site_url().'" rel="canonical" />'."\n";
  echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
	// CSS
	echo link_tag('css/reset.css');
	echo $_styles;
	// JS
  //echo script_tag('http://sawpf.com/1.0.js');
  echo script_tag('js/jquery.min.js');
	echo $_scripts;
  echo script_tag('js/site/site.js');
  if($this->site->ga_analytics)
  {
    echo "<script type=\"text/javascript\">\n";
    echo "var _gaq = _gaq || [];\n";
    echo "_gaq.push(['_setAccount', '".$this->site->ga_analytics."']);\n";
    echo "_gaq.push(['_trackPageview']);";

    echo "(function() {\n";
    echo "  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n";
    echo "  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n";
    echo "  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n";
    echo "})();\n";
    echo "</script>";
  }
?>
</head>
<body>
<div id="tudo">
	<div id="cabecalho">
    <div id="logotipo" class="margem-esquerda">
      <?php
        if(($this->empresa->logotipo) && (file_exists($this->config->item('dir_midia').$this->empresa->logotipo)))
        {
          list($largura, $altura) = getimagesize($this->config->item('dir_midia').$this->empresa->logotipo);
          echo anchor(site_url(), img(array('src' => $this->config->item('dir_midia').$this->empresa->logotipo, 'style'=> 'margin-top:'.round((125-$altura)/2).'px;margin-bottom:'.round((125-$altura)/2).'px;', 'alt' => $this->empresa->desc_empresa, 'title'=> $this->empresa->desc_empresa)));
        }
      ?>
    </div>
    <div id="contato" class="margem-direita">
      <?php
        echo heading(str_replace(' ', '<span class="hidden">_</span> ', $this->empresa->telefone).img(array('src' => site_url('img/icone_telefone.png'), 'alt' => 'Tel', 'title' => 'Telefone')), 3);
        echo heading(str_replace('@', '@<span class="hidden">#</span>', $this->empresa->email).img(array('src' => site_url('img/icone_email.png'), 'alt' => 'Email', 'title' => 'Email')), 3);
      ?>
    </div>
    <div id="menu" class="margem-direita">
      <ul id="menu_lava">
      <?php
        $menu = $this->menu_m->get_all(NULL, NULL, array('status' => 1), NULL, 'ordem ASC');
        foreach($menu->result() as $item)
        {
          $selectedLava = ($this->uri->segment(1) == $item->slug) ? 'selectedLava' : '';
          echo li(array('class' => 'fonte-google '.$selectedLava), anchor($item->slug, $item->desc_menu));
        }
      ?>
      </ul>
    </div>
  </div>
  <div id="busca-barra" class="fonte-google right">
  <?php
    echo form_open(site_url('produto/busca/')).'BUSCA'.nbs(2);
    echo form_input(array('name' => 'busca', 'id' => 'busca', 'value' => set_value('busca'), 'maxlength' => '200', 'size' => '90', 'class' => 'corner'));
    echo form_input(array('type' => 'image', 'src' => 'img/busca.png', 'name' => 'enviar', 'class' => 'corner'));
    echo form_close();
    echo '<div id="box-busca" class="right"></div>';
  ?>
  </div>
  <div id="box">
    <div id="margem25">&nbsp;</div>
    <div id="box-menu-lateral" class="margem-esquerda">
      <?php echo $menu_lateral; ?>
    </div>
    <div id="box-banner" class="margem-direita">
      <div id="slider">
      <?php
        $banners = $this->banner_m->get_all(NULL, NULL, array('status' => 1));
        foreach($banners->result() as $banner)
        {
          if(strlen($banner->link) > 0)
            echo anchor($banner->link, img(array('src'=> 'midia/'.$banner->imagem, 'title' => $banner->legenda, 'alt' => $banner->legenda)), 'target="_blank" title="'.$banner->legenda.'" alt="'.$banner->legenda.'"');
          else
            echo img(array('src'=> 'midia/'.$banner->imagem, 'title' => $banner->legenda, 'alt' => $banner->legenda));
        }
      ?>
      </div>
    </div>
    <div id="margem40">&nbsp;</div>
    <div class="clear-both"></div>
    <!-- #conteudo -->
    <?php
      echo $conteudo;
    ?>
    <!-- #conteudo -->
    <div class="clear-both"></div>
    <div id="rodape">
      <div id="copyright" class="margem-esquerda">&copy; <?php echo date('Y').' | <span class="upper negrito">'.$this->empresa->desc_empresa.'</span> - '.$this->empresa->endereco.', '.$this->empresa->numero.' - '.$this->empresa->bairro.' - '.$this->empresa->municipio.' - '.$this->empresa->uf.' | '.str_replace('@', '@<span class="hidden">#</span>', $this->empresa->email).' | '.str_replace(' ', '<span class="hidden">_</span> ', $this->empresa->telefone) ?></div>
      <div id="info" class="margem-esquerda">
        <div id="acesso"><?php echo img(array('src' => 'img/ipad.png', 'align' => 'left')); ?>Este site também pode ser acessado via iPad/Tablet. Mobilidade para você fazer bons negócios.</div>
        <div id="mapa"><?php echo anchor(site_url('site/mapa'), 'Mapa do Site', 'class="popup"'); ?></div>
        <div id="credito">Créditos: <?php echo anchor($this->config->item('dev_url'), img('img/dblinks.png'), 'alt="dblinks" title="dblinks - www.dblinks.com.br" target="_blank"'); ?></div>
      </div>
    </div>
    <div class="clear-both"></div>
  </div>
  <!-- #box -->
</div>
<!-- #tudo -->
</body>
</html>