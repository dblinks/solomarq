<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- Always force latest IE rendering engine & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Layout Navegador JS</title>
	<!-- Mobile Viewport Fix -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<!-- Grab Google CDNs jQuery, fall back if necessary -->
  
	<link href="<?php echo site_url('css/reset.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo site_url('css/navegadorjs.css'); ?>	" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="tudo">
  <div id="conteudo">
    <div id="ative">
    <?php
    	echo img('img/navegador/icon_browser.png', array('alt' => 'Ative o Javascript	'));
			echo heading('Para navegar em nosso site, ative o Javascript do seu navegador.', 1);
		?>
    </div>
    <div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="win">
     	<caption>Como habilitar JavaScript nos navegadores Mozilla Firefox, Google Chrome, Microsoft Internet Explorer, Opera e Safari no Windows</caption>
      <thead>
      	<tr>
          <th width="15%">Sistema Operacional</th>
          <th width="15%">Navegador</th>
          <th width="70%">Procedimento</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th rowspan="5"><?php echo img('img/navegador/icon_win.png').br(); ?>Windows</td>
          <td class="navegador"><?php echo img('img/navegador/icon_firefox.png').br(); ?>Firefox</td>
          <td>
          <ol>
            <li>Clique no menu <span class="negrito">Ferramentas</span>.</li>
            <li>Selecione <span class="negrito">Opções</span>.</li>
            <li>Clique na guia <span class="negrito">Conteúdo</span>.</li>
            <li>Marque a opção <span class="negrito">Ativar JavaScript</span>.</li>
            <li>Clique no botão <span class="negrito">OK</span>.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Firefox ou recarregar as páginas.</li>
          </ol>
					</td>
        </tr>
        <tr>
          <td class="navegador"><?php echo img('img/navegador/icon_chrome.png').br(); ?>Chrome</td>
          <td>
          <ol>
          	<li>No canto superior direito do navegador clique com imagem de uma <span class="negrito">Chave de boca</span></li>
            <li>Clique no menu <span class="negrito">Preferências</span>.</li>
            <li>Clique na Aba/Guia <span class="negrito">Configurações avançadas</span>.</li>
            <li>Clique no botão <span class="negrito">Configurações de Conteúdo</span>.</li>
            <li>À direita marque a opção <span class="negrito">Permitir que todos os sites executem JavaScript</span>.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Chrome ou recarregar as páginas.</li>         
          </ol>
          </td>
        </tr>
        <tr>
          <td class="navegador"><?php echo img('img/navegador/icon_ie.png').br(); ?>Internet Explorer</td>
          <td>
          <ol>
          	<li>Clique no menu <span class="negrito">Ferramentas</span>.</li>
            <li>Selecione <span class="negrito">Opções de Internet</span>.</li>
            <li>Clique na guia <span class="negrito">Segurança</span>.</li>
            <li>Clique no botão <span class="negrito">Nível Personalizado</span>.</li>
            <li>Role para baixo até ver a seção <span class="negrito">Scripts</span>. Selecione o botão de opção <span class="negrito">Ativar</span> para <span class="negrito">Scripts Ativos</span>.</li>
            <li>Clique no botão <span class="negrito">OK</span>.</li>
            <li>Clique no botão <span class="negrito">Sim</span> na janela de confirmação.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Internet Explorer ou recarregar as páginas.</li>
					</ol>
          </td>
        </tr>
        <tr>
          <td class="navegador"><?php echo img('img/navegador/icon_opera.png').br(); ?>Opera</td>
          <td>
          <ol>
          	<li>Clique em <span class="negrito">Menu</span>, após escolha <span class="negrito">Configurações</span> e então clique em <span class="negrito">Preferências</span>.</li>
          	<li>Clique na aba <span class="negrito">Avançado</span>.</li>
            <li>No menu à esquerda a opção <span class="negrito">Conteúdo</span>.</li>
            <li>Marque a caixa de seleção <span class="negrito">Habilitar JavaScript</span>.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Opera ou recarregar as páginas.</li>
          </ol>
          </td>
        </tr>
        <tr>
          <td class="navegador"><?php echo img('img/navegador/icon_safari.png').br(); ?>Safari</td>
          <td>
          <ol>
          	<li>No canto superior direito do navegador clique com imagem de uma <span class="negrito">Engrenagem</span>.</li>
            <li>Clique no item <span class="negrito">Preferências</span>.</li>
            <li>Clique na guia <span class="negrito">Segurança</span>.</li>
            <li>Marque a caixa de seleção <span class="negrito">Ativar JavaScript</span>.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Safari ou recarregar as páginas.</li>
          </ol>
          </td>
        </tr>
      </tbody>
		</table>
		<br />
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mac">
     	<caption>Como habilitar JavaScript nos navegadores Mozilla Firefox, Google Chrome, Opera e Safari no Mac OS</caption>
      <thead>
      	<tr>
          <th width="15%">Sistema Operacional</th>
          <th width="15%">Navegador</th>
          <th width="70%">Procedimento</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th rowspan="4"><?php echo img('img/navegador/icon_mac.png').br(); ?>Mac OS</td>        
          <td class="navegador"><?php echo img('img/navegador/icon_safari.png').br(); ?>Safari</td>
          <td>
          <ol>
	          <li>Clique no menu <span class="negrito">Safari</span>.</li>
            <li>Clique no item <span class="negrito">Preferências</span>.</li>
            <li>Clique na guia <span class="negrito">Segurança</span>.</li>
            <li>Marque a caixa de seleção <span class="negrito">Ativar JavaScript</span>.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Safari ou recarregar as páginas.</li>
          </ol>
          </td>
        </tr>      
        <tr>
          <td class="navegador"><?php echo img('img/navegador/icon_firefox.png').br(); ?>Firefox</td>
          <td>
          <ol>
            <li>Clique no menu <span class="negrito">Firefox</span>.</li>
            <li>Clique no item <span class="negrito">Preferências</span>.</li>
            <li>Clique na guia <span class="negrito">Conteúdo</span>.</li>
            <li>Marque a caixa de seleção <span class="negrito">Ativar JavaScript</span>.</li>
            <li>Feche a janela.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Firefox ou recarregar as páginas.</li>
          </ol>
					</td>
        </tr>
        <tr>
          <td class="navegador"><?php echo img('img/navegador/icon_chrome.png').br(); ?>Chrome</td>
          <td>
          <ol>
            <li>Clique no menu <span class="negrito">Chrome</span>.</li>
            <li>Clique no item <span class="negrito">Preferências</span>.</li>
            <li>Na janela que abrir clique na Aba/guia <span class="negrito">Configurações avançadas</span>.</li>
            <li>Clique no botão <span class="negrito">Configurações de Conteúdo</span>.</li>
            <li>Na janela que abrir clique em <span class="negrito">Permitir que todos os sites executem JavaScript</span>.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Chrome ou recarregar as páginas.</li>         
          </ol>
          </td>
        </tr>
        <tr>
          <td class="navegador"><?php echo img('img/navegador/icon_opera.png').br(); ?>Opera</td>
          <td>
          <ol>
          <li>Clique no menu <span class="negrito">Opera</span>.</li>
            <li>Clique no item <span class="negrito">Preferências</span>.</li>
          	<li>Clique na aba <span class="negrito">Avançado</span>.</li>
            <li>No menu à esquerda a opção <span class="negrito">Conteúdo</span>.</li>
            <li>Marque a caixa de seleção <span class="negrito">Habilitar JavaScript</span>.</li>
            <li>Em seguida fechar as janelas, e reiniciar o Opera ou recarregar as páginas.</li>
          </ol>
          </td>
        </tr>
      </tbody>
		</table>    
    </div>
    <div id="use">
      <center>
        <table width="50%" border="0" cellspacing="0" cellpadding="0" class="versao" align="center">
        <caption>Use qualquer navegador, mas use a última versão.</caption>
          <tr>
            <td><?php echo anchor('http://www.google.com/chrome?hl=pt-BR', img('img/navegador/icon_chrome.png', array('alt' => 'Chrome', 'title' => 'Chrome')), array('target' => '_blank')); ?></td>
            <td><?php echo anchor('http://www.mozilla.com/pt-BR/firefox/', img('img/navegador/icon_firefox.png', array('alt' => 'Firefox', 'title' => 'Firefox')), array('target' => '_blank')); ?></td>
            <td><?php echo anchor('http://www.opera.com/download/get.pl', img('img/navegador/icon_opera.png', array('alt' => 'Opera', 'title' => 'Opera')), array('target' => '_blank')); ?></td>
            <td><?php echo anchor('http://www.apple.com/br/safari/download/', img('img/navegador/icon_safari.png', array('alt' => 'Safari', 'title' => 'Safari')), array('target' => '_blank')); ?></td>
            <td><?php echo anchor('http://windows.microsoft.com/pt-BR/internet-explorer/products/ie/home', img('img/navegador/icon_ie.png', array('alt' => 'Internet Explorer', 'title' => 'Internet Explorer')), array('target' => '_blank')); ?></td>
          </tr>
        </table>
      </center>
    </div>    
    <div id="atualizar">
      <?php echo anchor(site_url(), img(array('src' => 'img/navegador/atualizar.png', 'alt' => 'Ative o Javascript'))); ?>
      <h1>Clique aqui para voltar ao site.</h1>
    </div>    
  </div>
</div>
</body>
</html>
