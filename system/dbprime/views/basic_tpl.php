<?php echo doctype('xhtml1-trans'); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $this->empresa->desc_empresa.' - '.$this->empresa->slogan; ?></title>
<?php
// META
	echo meta(array('name' => 'title', 'content' => $this->empresa->desc_empresa.' - '.$this->empresa->slogan));
	echo meta(array('name' => 'robots', 'content' => 'nocache, nofollow'));
	echo meta(array('name' => 'description', 'content' => $this->empresa->slogan));
	echo meta(array('name' => 'keywords', 'content' => $this->empresa->palavras_chave));
	echo meta(array('name' => 'autor', 'content' => $this->config->item('dev_desc').', '.$this->config->item('dev_email').', '.$this->config->item('dev_url')));
	echo meta(array('name' => 'copyright', 'content' => '© 2011 '.$this->config->item('dev_desc').', '.$this->config->item('dev_email').', '.$this->config->item('dev_url')));
	// Mobile Viewport Fix
	echo meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));
	echo meta(array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'));
	echo meta(array('name' => 'expires', 'content' => standard_date('DATE_RFC822', time()+14400), 'type' => 'equiv'));
	//Always force latest IE rendering engine & Chrome Frame
	echo meta(array('name' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1', 'type' => 'equiv'));
	echo link_tag('favicon.ico', 'shortcut icon', 'image/ico');
	// CSS
	echo link_tag('css/reset.css');
  echo link_tag('css/basic.css');
	echo $_styles;
	// JS
	echo script_tag('js/jquery.min.js');
	echo $_scripts;
?>
</head>
<body	oncontextmenu="return false" onselectstart="return false">
<noscript>
	<span class="noscript">Este sistema requer o Javascript habilitado para funcionar corretamente. <?php echo anchor(admin_url('habilitar_js'), 'Clique aqui'); ?> e execute o procedimento para habilitar o Javascript no seu navegador.</span>
</noscript>
<div id="tudo">
<?php
  echo $conteudo;
?>
</div>
<!-- tudo -fim-->
</body>
</html>
