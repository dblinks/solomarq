$(document).ready(function()
{
    //Padding, mouseover
    var padLeft = '5px';

    //Default Padding
    var defpadLeft = $('#menu_lateral li a').css('paddingLeft');

    //Animate the LI on mouse over, mouse out
    $('ul#menu_lateral li').click(function () {
      //Make LI clickable
      window.location = $(this).find('a').attr('href');

    }).mouseover(function (){

      //mouse over LI and look for A element for transition
      $(this).find('a')
      .animate( {
        paddingLeft: padLeft
      }, {
        queue:false,
        duration:50
      } );

    }).mouseout(function () {

      //mouse oout LI and look for A element and discard the mouse over transition
      $(this).find('a')
      .animate( {
        paddingLeft: defpadLeft
      }, {
        queue:false,
        duration:50
      } );
    });

    //Scroll the menu on mouse move above the #sidebar layer
    $('#box-menu-lateral').mousemove(function(e) {

      //Sidebar Offset, Top value
      var s_top = parseInt($('#box-menu-lateral').offset().top);

      //Sidebar Offset, Bottom value
      var s_bottom = parseInt($('#box-menu-lateral').height() + s_top);

      //Roughly calculate the height of the menu by multiply height of a single LI with the total of LIs
      //var mheight = parseInt($('ul#menu_categoria li').height() * $('ul#menu_categoria li').length);
      var mheight = parseInt($('ul#menu_lateral li').height() + 5);// * ($('ul#menu_lateral li').length / 2));

      //I used this coordinate and offset values for debuggin
      //console.log("X Axis : " + e.pageX + " | Y Axis " + e.pageY);
      //console.log(Math.round(((s_top - e.pageY)/100) * mheight / 2));

      //Calculate the top value
      //This equation is not the perfect, but it 's very close
      var top_value = Math.round(( (s_top - e.pageY) /100) * mheight / 2) + 5;

      //Animate the #menu by chaging the top value
      $('ul#menu_lateral').animate({
        top: top_value
      }, {
        queue:false,
        duration:2000
      });
    });
});