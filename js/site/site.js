$(document).ready(function() {
  $('#slider').orbit({
    directionalNav: false,
    timer: true,
    animation: 'fade',
    captions: true,
    captionAnimation: 'fade',
    bullets: true,
    bulletThumbs: false,
    animationSpeed: 1000,
    advanceSpeed: 6000
  });

  $('#menu_lava').lavaLamp();

  $("#busca").keyup(function() {
    var searchbox = $(this).val();
    var dataString = 'busca='+ searchbox;

    if(searchbox=='')
    {
    }
    else
    {
      $.ajax({
        type: 'POST',
        url: SITE_URL+'produto/busca_ajax',
        data: dataString,
        cache: false,
        success: function(html) {
          $("#box-busca").html(html).show();
        }
      });
    }
    return false;
  });
  $("#box-busca").mouseleave(function() { $(this).hide(); });


  $("#busca").Watermark("Busca");

  if($('a.galeria')[0])
  {
    $('a.galeria').colorbox({
      rel:'gal',
      transition:'fade',
      slideshow:true,
      speed: 500,
      slideshowSpeed:	5000
    });
  }

  if($('a.mapa')[0])
  {
    $('a.mapa').colorbox({
      iframe:true,
      width:"700px",
      height:"500px"
    });
  }

  if($('a.popup')[0])
  {
    $('a.popup').colorbox({
      iframe:true,
      width:"600px",
      height:"400px"
    });
  }

  if($('ul.tabs')[0])
  {
    $('ul.tabs').tabs('div.panes > div');
  }

  if($('#galleria')[0])
  {
    $('#galleria').galleria({
      width:348,
      height:350,
      showCounter: false,
      extend: function() {
        this.bind(Galleria.IMAGE, function(e) {
          $(e.imageTarget).css('cursor','pointer');
          $(e.imageTarget).click(this.proxy(function() {
            targ = $(e.imageTarget).attr('src');
            $('a[href="'+targ+'"]').click();
          }));
        });
      }
    });
  }
});