$(document).ready(function() {
  $('form.validar').validate({ meta: "validar" });
  $('.moeda').setMask({ mask: '99,999.999.99', type: 'reverse', autoTab: false });
  $('.data').setMask({ mask: '39/19/9999', autoTab: false });
  $('.ano').setMask({ mask: '9999', type: 'reverse', autoTab: false });
  $('.numero').setMask({ mask: '9999999999', type: 'reverse', autoTab: false });
  $('.cep').setMask({ mask: '99999-999', autoTab: false });
  $('.uf').setMask({ mask: 'ZZ', defaultValue: '', autoTab: false });
  $('.telefone').setMask({ mask: '(99) 9999 9999', autoTab: false });
  $('.cpf').setMask({ mask : '999.999.999-99', autoTab: false });
  $('.cnpj').setMask({ mask : '99.999.999/9999-99', autoTab: false });
});