// JavaScript Document
$(document).ready(function() {
	// Campo legenda
  $('span.campo_legenda').live('click', function() {
    $(this).editable($(this).attr('url')+'/legenda_alterar', {
			maxlength : 50,
      size: 15,
			name : 'descricao',
      indicator : "<img src='"+SITE_URL+"/img/loading.gif'>",
      tooltip   : "Clique para alterar...",
      style  : "inherit",
			submit    : 'OK',
			submitdata : function(value, settings) {
      	return { id: $(this).attr('idmidia')};
   		}
    })
  });

	//upload multiplo
	'use strict';

	// Initialize the jQuery File Upload widget:
	$('#fileupload').fileupload({
    dropZone: $('#arraste-arquivos'),
		sequentialUploads: true,
		limitConcurrentUploads: 1,
		maxFileSize: 5242880, // Size in Bytes - 5 MB
		minFileSize: 1, // Size in Bytes - 1 KB
		previewMaxWidth: 80,
		stop: function (){ location.reload(); }
	});

	// Load existing files:
	$.getJSON($('#fileupload form').prop('action'), function (files)
	{
    if(files !== null)
    {
      var fu = $('#fileupload').data('fileupload');
      fu._adjustMaxNumberOfFiles(-files.length);
      fu._renderDownload(files)
      .appendTo($('#fileupload .midia'))
      .fadeIn(function ()
      {
        // Fix for IE7 and lower:
        $(this).show();
      });
    }
	});

	// Open download dialogs via iframes,
	// to prevent aborting current uploads:
	$('#fileupload .midia a:not([target^=_blank])').live('click', function (e)
	{
		e.preventDefault();
		$('<iframe style="display:none;"></iframe>')
		.prop('src', this.href)
		.appendTo('body');
	});
});

/*
// JavaScript Document
$(document).ready(function() {
  // Campo legenda
  $('span.campo_legenda').live('click', function() {
    $(this).editable($(this).attr('url')+'/legenda_alterar', {
			maxlength : 50,
      size: 15,
			name : 'legenda',
      indicator : "<img src='"+SITE_URL+"/img/loading.gif'>",
      tooltip   : "Clique para alterar...",
      style  : "inherit",
			//submit    : 'OK',
			submitdata : function(value, settings) {
      	return { id: $(this).attr('idmidia')};
   		}
    })
  });

	//upload multiplo
	'use strict';

	// Initialize the jQuery File Upload widget:
	$('#fileupload').fileupload({
		dropZone: $('#arraste-arquivos'),
		//maxNumberOfFiles: parseInt($('#fileupload').attr('nmax')),
		sequentialUploads: true,
		limitConcurrentUploads: 1,
		//acceptFileTypes: /(png)|(jpe?g)|(gif)$/i,
		maxFileSize: 5242880, // Size in Bytes - 5 MB
		minFileSize: 1, // Size in Bytes - 1 KB
		previewMaxWidth: 80,
		stop: function (){ location.reload(); }
	});
  //$('#fileupload').bind('fileuploadsend', function (e, data) { alert(console.log(3)); })

	// Load existing files:
	$.getJSON($('#fileupload form').prop('action'), function (files)
	{
    if(files !== null)
    {
      var fu = $('#fileupload').data('fileupload');
      fu._adjustMaxNumberOfFiles(-files.length);
      fu._renderDownload(files)
      .appendTo($('#fileupload .midia'))
      .fadeIn(function ()
      {
        // Fix for IE7 and lower:
        $(this).show();
      });
    }
	});

	// Open download dialogs via iframes,
	// to prevent aborting current uploads:
	$('#fileupload .midia a:not([target^=_blank])').live('click', function (e)
	{
		e.preventDefault();
		$('<iframe style="display:none;"></iframe>')
		.prop('src', this.href)
		.appendTo('body');
	});
});
*/