$(document).ready(function() {
	var DOMINIO=window.location.href.match(/:\/\/(.[^/]+)/)[1];
	var URL_ATUAL=window.location.href;
	const URL_ARRAY = window.location.href.split('/').filter(e => { return e != '';});
	URL_ARRAY.shift();
  var MODULO = URL_ARRAY[2];

	$('a.cancelar').button();

	// Aplicando tabs
	$(".box-tab").tabs();

	// Adicionando fechar
	$(".fecha").append('<a href="#" class="fechar">fechar</a>');

	// Combobox dinâmico
	$("select.carrega").change(function(){
		var modulo = $(this).attr('modulo');
		var destino = $(this).attr('destino');
		var acao = $(this).attr('acao');
    $.jGrowl('Selecionando.', { header: 'Seleção', sticky: false });

    $.get(
      ADMIN_URL+'/'+modulo+'/ajax_'+acao+'/'+$(this).val(),
      null,
      function(data, err){
        $('#'+destino).html('');
        $('#'+destino).parent().children('span').html('');
        $('#'+destino).append(data);
      }, "html"
    );
	});

	// Fechar mensagens
	$("a.fechar").click(function () {
		$(this).fadeTo(200, 0); // Hack IE
		$(this).parent().fadeTo(200, 0);
		$(this).parent().slideUp(400);
		return false;
	});

	// Fade in nas mensagens
	$('.mensagem-fade').delay(300).fadeIn('slow');

	// usuario - alterar senha
	$("input#alterar_senha").click(function(){
		if($(this).is(':checked'))
			$('#box_alterar_senha').show("fast");
		else
			$('#box_alterar_senha').hide("fast");
	});

	// Slug
	$('input.titulo').keyup($.debounce(350, function(e){
		$.post(ADMIN_URL + '/ajax_slug', { string : $(this).val() }, function(slug){
			$('input#slug').val(slug);
		});
	}));

	// Igual
	$('input.igualf').keyup($.debounce(50, function(e) {
		if($('input.igualt').val().length <= 3) {
			primeiro = $('input.igualf').val().split(' ');
			$('input.igualt').val(primeiro[0]);
		}
	}));

  // Menu lateral
	$("ul#menu_lateral li ul").hide();

	$("ul#menu_lateral li a.current").parent().parent().parent().children('ul.mod').toggle();
	$("ul#menu_lateral li a.current").parent().parent().parent().children('a.link').addClass('current');

	$("ul#menu_lateral li a.link").click(function () {
		if($(this).hasClass("go"))
		{
			return true;
		}

		if($(this).hasClass("no-submenu"))
			return false;
		$(this).parent().siblings().find("ul").slideUp("normal");
		$(this).parent().siblings().find("a").removeClass("bottom-border");
		$(this).next().slideToggle("normal");
		$(this).toggleClass("bottom-border");

		return false;
	});

	$("ul#menu_lateral li ul li a").click(function() {
		$("ul#menu_lateral li ul li a").removeClass("current");
		$(this).toggleClass("current");
	});
	/* Menu lateral -fim */

	// usuario - permissão
	$("input#todos").click(function(){
		if($(this).is(':checked'))
			$("input.check").attr('checked', true);
    else
			$("input.check").attr('checked', false);
	});

	$("input.check").click(function(){
		if($("input#todos").is(':checked'))
			$("input#todos").attr('checked', false);
	});

	// Uniform - forms
	$("select, textarea, input, input[type=text], input[type=file], input[type=submit]").livequery(function(){
		$(this).not('.no-uniform, input[type=checkbox]').uniform();
	});

	// Qtip
	$("input.dica, select.dica, textarea.dica").each(function() {
	//$('select, textarea, input, input[type=text], input[type=file]').qtip({
		$(this).qtip({
			content: {
				text: function(api) {
					 // Retrieve content from custom attribute of the $('.selector') elements.
					 return $(this).attr('title');
				}
			},
			position: {
				my: 'bottom left',
      	at: 'top right'
  	 	},
			style: {
				tip: {
					corner: true,
					offset: 5,
					border: 2
				}
			},
			show: {
				delay: 0,
				event: "focus"
			},
			hide: {
				event: "blur"
			}
		});
	});

	$('fieldset, select, legend, button, .input, .submit, #carregando, .formato-arquivo').addClass("corner");

  // Formulário
	$("input:text:visible:first").focus();
	$('form.validar').validate({meta: "validar"});
	$('.moeda').setMask({mask: '99,999.999.99', type: 'reverse', autoTab: false});
	$('.data').setMask({mask: '39/19/9999', autoTab: false });
	$('.ano').setMask({mask: '9999', type: 'reverse', autoTab: false });
	$('.numero').setMask({mask: '9999999999', type: 'reverse', autoTab: false });
	$('.cep').setMask({mask: '99999-999', autoTab: false });
	$('.uf').setMask({mask: 'ZZ', defaultValue: '', autoTab: false });
	$('.telefone').setMask({ mask: '(99) 9999 9999', autoTab: false });
  $('.cpf').setMask({ mask : '999.999.999-99', autoTab: false });
  $('.cnpj').setMask({ mask : '99.999.999/9999-99', autoTab: false });
  $('.analytics').setMask({ mask : 'ZZ-99999999-9', autoTab: false });

	$('.calendario-mes').datepicker({
		closeText: 'Fechar',
		prevText: '&#x3c;Anterior',
		nextText: 'Pr&oacute;ximo&#x3e;',
		currentText: 'Hoje',
		monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
		'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
		'Jul','Ago','Set','Out','Nov','Dez'],
		dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
		dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	});

  tinyMCE.init({
      mode : "specific_textareas",
			editor_selector : 'editor',
			theme : "advanced",
			width : "50%",
			heigth : "120",
			plugins : "preview",

			theme_advanced_buttons1 : "bold,italic,|,undo,redo,|,removeformat,|,indent,outdent,|,bullist,numlist,|,link,unlink,|,preview,code",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_location : "bottom",
			theme_advanced_toolbar_align : "center",

			theme_advanced_resizing : true
	});

  // Link Excluir
	$(".excluir").live('click', function(){
		if(confirm('Tem certeza que deseja excluir "'+$(this).attr('desc')+'"?\nIsto não pode ser desfeito.'))
			return true;
		else
			return false;
	});

  if($('#rss')[0])
  {
    $('#rss').rssfeed($('#rss').attr('url'), {
      limit: 5,
      media: false,
      linktarget: '_blank',
      content: false
    });
  }
	// Datatables - Tabela dinâmica
	$('table.json').dataTable({
		bJQueryUI: true,
		bAutoWidth: false,
		sPaginationType: "full_numbers",
		aLengthMenu: [[10, 25, 50, 100, 150, 200, -1], [10, 25, 50, 100, 150, 200, "Todos"]],
		iDisplayLength: 25,
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: ADMIN_URL+'/'+MODULO+'/json_listar',
		fnServerParams: function (aoData) {
      aoData.push( { "name": "modulo", "value": URL_ARRAY[4] } );
			aoData.push( { "name": "item", "value": URL_ARRAY[5] } );
    },
		fnServerData: function(sSource, aoData, fnCallback) {
			$.ajax({
				dataType: 'text json',
				type: 'POST',
				url: sSource,
				data: aoData,
				error: function(r, status, error){
					$.jGrowl('Erro ao processar informações<br>Erro:'+r.responseText, { header: 'Erro', sticky: true });
				},
				success: fnCallback
			})
		},
		oLanguage: {
			sProcessing:   "Processando...",
			sLengthMenu:   "Exibindo _MENU_ registro(s).",
			sZeroRecords:  "Desculpe, nada encontrado.",
			sInfo:         "Exibindo _START_ a _END_ de _TOTAL_ registro(s).",
			sInfoEmpty:    "Exibindo 0 a 0 de 0 registro(s).",
			sInfoFiltered: "(filtro aplicado em _MAX_ registro(s)).",
			sSearch:       "Busca:",
			oPaginate: {
				sFirst:    "Primeira",
				sPrevious: "Anterior",
				sNext:     "Próxima",
				sLast:     "Última"
			}
		}
	});

	// Datatables - Tabela completa
	$('table.full').dataTable({
		//sDom: '<"H"lif>t<"F"p>',
		bJQueryUI: true,
		bAutoWidth: false,
		sPaginationType: "full_numbers",
		aLengthMenu: [[10, 25, 50, 100, 150, 200, -1], [10, 25, 50, 100, 150, 200, "Todos"]],
		iDisplayLength: 25,
		oLanguage: {
			sProcessing:   "Processando...",
			sLengthMenu:   "Exibindo _MENU_ registro(s).",
			sZeroRecords:  "Desculpe, nada encontrado.",
			sInfo:         "Exibindo _START_ a _END_ de _TOTAL_ registro(s).",
			sInfoEmpty:    "Exibindo 0 a 0 de 0 registro(s).",
			sInfoFiltered: "(filtro aplicado em _MAX_ registro(s)).",
			sSearch:       "Busca:",
			oPaginate: {
				sFirst:    "Primeira",
				sPrevious: "Anterior",
				sNext:     "Próxima",
				sLast:     "Última"
			}
		}
	});

  $('.colorbox').colorbox({iframe:true, innerWidth:"90%", innerHeight:"90%"});
});

function setaIcone(icone)
{
  $('#icone').val(icone);
}
